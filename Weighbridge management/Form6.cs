﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.IO.Ports;
using System.Threading;


namespace Weighbridge_management
{
    public partial class Form6 : Form
    {
        dbconnection con = new dbconnection();
        MySql.Data.MySqlClient.MySqlConnection conn;
        static string host = "localhost";
        static string database = "wms";
        static string userDB = "root";
        static string password = "Techbrain@123";
        public static string strProvider = "server=" + host + ";Database=" + database + ";User ID=" + userDB + ";Password=" + password;
        int ID = 0;
        int ID1 = 0;
        int ID2 = 0;
        int ID3 = 0;
        int ID4 = 0;
        string ID5 = "";

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtname.Text != "" && txtadd1.Text != "" && txtadd2.Text != "" && txtcity.Text != "" && txtstate.Text != "" )
                {
                    con.Open();
                    string query = "select  * from tbl_vendormst WHERE name ='" + txtname.Text + "'";
                    MySqlDataReader row;
                    row = con.ExecuteReader(query);
                    if (row.HasRows)
                    {
                        MessageBox.Show("Vendor already Exsist...", "Information");
                    }
                    else
                    {
                        if (txtpass.Text == txtcnfrmpass.Text)
                        {
                            con.Close();
                            con.Open();
                            
                            string query3 = "INSERT INTO `wms`.`tbl_vendormst`(`name`,`address1`,`address2`,`city`,`state`,`pincode`)VALUES('" + txtname.Text + "','" + txtadd1.Text + "','" + txtadd2.Text + "','" + txtcity.Text + "','" + txtstate.Text + "','"+txtpin.Text+"')";
                            MySqlDataReader row3;
                            row3 = con.ExecuteReader(query3);
                            if (row3.RecordsAffected == 1)
                            {
                                //this.Close();
                                //Form2 frm2 = new Form2();
                                //frm2.Show();
                                MessageBox.Show("Vendor added successfully...", "Information");
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Please Enter Detail", "Information");
                }
            }
            catch
            {
                MessageBox.Show("Connection Error", "Information");
            }
        }

        public Form6()
        {
            InitializeComponent();
            
        }

       

        private void button3_Click(object sender, EventArgs e)
        {
                this.Close();
                 
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtzone.Text != "")
                {
                    con.Open();
                    string query = "select  * from tbl_zone WHERE name ='" + txtzone.Text + "'";
                    MySqlDataReader row;
                    row = con.ExecuteReader(query);
                    if (row.HasRows)
                    {
                        MessageBox.Show("Zone already Exsist...", "Information");
                    }
                    else
                    {
                          con.Close();
                            con.Open();
                            string query3 = "INSERT INTO `wms`.`tbl_zone`(`name`)VALUES('" + txtzone.Text + "')";
                            MySqlDataReader row3;
                            row3 = con.ExecuteReader(query3);
                            if (row3.RecordsAffected == 1)
                            {
                                //this.Close();
                                //Form2 frm2 = new Form2();
                                //frm2.Show();
                                MessageBox.Show("Zone added successfully...", "Information");
                            }
                       
                    }
                }
                else
                {
                    MessageBox.Show("Please Enter Detail", "Information");
                }
            }
            catch
            {
                MessageBox.Show("Connection Error", "Information");
            }
        }

        private void txtzone_TextChanged(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void Form6_Load(object sender, EventArgs e)
        {
            this.sp_vehicleTableAdapter.Fill(this.wmsDataSet18.sp_vehicle);
            this.sp_driverlistTableAdapter1.Fill(this.wmsDataSet17.sp_driverlist);
            this.sp_driverlistTableAdapter.Fill(this.wmsDataSet16.sp_driverlist);
            this.sp_wardlistTableAdapter.Fill(this.wmsDataSet15.sp_wardlist);
            this.sp_zonelistTableAdapter.Fill(this.wmsDataSet14.sp_zonelist);
            this.sp_vendorlistTableAdapter.Fill(this.wmsDataSet13.sp_vendorlist);
            this.sp_userlistTableAdapter.Fill(this.wmsDataSet12.sp_userlist);

            this.tbl_zoneTableAdapter.Fill(this.wmsDataSet4.tbl_zone);
           
            con.Open();
            string query = "SELECT * FROM wms.tbl_vendormst order by name";
            MySqlDataReader row;
            row = con.ExecuteReader(query);
            if (row.HasRows)
            {

                while (row.Read())
                {
                    cbvendername.Items.Add(row["name"].ToString());
                }
            }

            con.Close();
            con.Open();
            string que = "SELECT * FROM wms.tbl_zone order by name";
            MySqlDataReader row5;
            row5 = con.ExecuteReader(que);
            if (row5.HasRows)
            {
                while (row5.Read())
                {
                    comboBox1.Items.Add(row5["name"].ToString());
                }
            }

            con.Close();
            con.Open();
            string que2 = "SELECT * FROM wms.tbl_drivermst order by fname";
            MySqlDataReader row7;
            row7 = con.ExecuteReader(que2);
            if (row7.HasRows)
            {
                while (row7.Read())
                {
                    cbdrvrname.Items.Add(row7["fname"].ToString());
                }
            }


           
        }

        private void fillByToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.tbl_zoneTableAdapter.FillBy(this.wmsDataSet4.tbl_zone);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtward.Text != "")
                {
                    con.Open();
                    string query = "select  * from tbl_ward WHERE name ='" + txtward.Text + "'";
                    MySqlDataReader row;
                    row = con.ExecuteReader(query);
                    if (row.HasRows)
                    {
                        MessageBox.Show("Ward already Exsist...", "Information");
                    }
                    else
                    {
                        con.Close();
                        con.Open();
                        string query3 = "INSERT INTO `wms`.`tbl_ward`(`name`,`zoneid`)VALUES('" + txtward.Text + "',"+ cbzonename.SelectedValue + ")";
                        MySqlDataReader row3;
                        row3 = con.ExecuteReader(query3);
                        if (row3.RecordsAffected == 1)
                        {
                            //this.Close();
                            //Form2 frm2 = new Form2();
                            //frm2.Show();
                            MessageBox.Show("Ward added successfully...", "Information");
                        }

                    }
                }
                else
                {
                    MessageBox.Show("Please Enter Detail", "Information");
                }
            }
            catch
            {
                MessageBox.Show("Connection Error", "Information");
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtdrivfname.Text != "" && txtdrivelname.Text!="" && txtdrivmo.Text!="" && txtdob.Text!="" && txtdriveadd1.Text!="" && txtdrivadd2.Text!="" && txtdrivcity.Text!="" && txtdrivestate.Text!="" && txtdrivelice.Text!="")
                {
                    con.Open();
                    string query = "select  * from tbl_drivermst WHERE fname ='" + txtdrivfname.Text + "' and lname='"+txtdrivelname+ "' and licenceid='"+txtdrivelice.Text+"'";
                    MySqlDataReader row;
                    row = con.ExecuteReader(query);
                    if (row.HasRows)
                    {
                        MessageBox.Show("Driver details already Exsist...", "Information");
                    }
                    else
                    {
                        con.Close();
                        con.Open();
                        string query3 = "INSERT INTO `wms`.`tbl_drivermst`(`fname`,`lname`,`mobile`,`dob`,`addressline1`,`addressline2`,`state`,`city`,`licenceid`)VALUES('" + txtdrivfname.Text + "','" + txtdrivelname.Text + "','"+txtdrivmo.Text+"','"+txtdob.Text+"','"+txtdriveadd1.Text+"','"+txtdrivadd2.Text+"','"+ txtdrivestate .Text+ "','"+txtdrivcity.Text+"','"+txtdrivelice.Text+"')";
                        MySqlDataReader row3;
                        row3 = con.ExecuteReader(query3);
                        if (row3.RecordsAffected == 1)
                        {
                            //this.Close();
                            //Form2 frm2 = new Form2();
                            //frm2.Show();
                            MessageBox.Show("Driver detail added successfully...", "Information");
                        }

                    }
                }
                else
                {
                    MessageBox.Show("Please Enter Detail", "Information");
                }
            }
            catch
            {
                MessageBox.Show("Connection Error", "Information");
            }
        }

        private void btnnewrecord_Click(object sender, EventArgs e)
        {
            if (txtvehicleno.Text != "" && cbvendername.Text != "" && comboBox1.Text != "" && cbwardname.Text != "" && cbdrvrname.Text != "" && txttyp.Text != "")
            {
               
                try
                {
                    string vend, zone, ward, driver, vehtyp;
                    ID5 = txtvehicleno.Text;
                    vend = cbvendername.Text;
                    zone = comboBox1.Text;
                    ward = cbwardname.Text;
                    driver = cbdrvrname.Text;
                    vehtyp = txttyp.Text;
                   


                    con.Open();
                    string query = "select  * from tbl_mstvehicle WHERE number='" + ID5 + "'";
                    MySqlDataReader row;
                    row = con.ExecuteReader(query);
                    if (row.HasRows)
                    {

                        MessageBox.Show("Vehicle number already exsist...");
                    }
                    else
                    {


                        strProvider = "server=" + host + ";Database=" + database + ";User ID=" + userDB + ";Password=" + password;
                        conn = new MySqlConnection(strProvider);
                        conn.Open();
                        string q1 = "sp_insertnewvehicle";
                        MySqlCommand sd = new MySqlCommand(q1, conn);
                        sd.CommandType = CommandType.StoredProcedure;
                        sd.Parameters.Add("@veh", MySqlDbType.VarChar, 225).Value = ID5;
                        sd.Parameters.Add("@typ", MySqlDbType.VarChar, 225).Value = vehtyp;
                        sd.Parameters.Add("@zone", MySqlDbType.VarChar, 225).Value = zone;
                        sd.Parameters.Add("@ward", MySqlDbType.VarChar, 225).Value = ward;
                        sd.Parameters.Add("@driver", MySqlDbType.VarChar, 225).Value = driver;
                        sd.Parameters.Add("@vend", MySqlDbType.VarChar, 225).Value = vend;

                        try
                        {

                            sd.ExecuteNonQuery();
                            MessageBox.Show("Vehicle detail added Successfully...", "Information");
                            DisplayData();

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                        finally
                        {
                            sd.Connection.Close();

                        }
                    }
                }
                catch
                {
                    MessageBox.Show("Vehicle detail erro");
                }
               
            }
            else
            {
                MessageBox.Show("Please fill all the details...");
            }
        }

        

        private void dataGridView2_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ID = Convert.ToInt32(dataGridView2.Rows[e.RowIndex].Cells[0].Value.ToString());
            txtfname.Text = dataGridView2.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtlname.Text = dataGridView2.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtmobi.Text = dataGridView2.Rows[e.RowIndex].Cells[3].Value.ToString();
            txtloginid.Text = dataGridView2.Rows[e.RowIndex].Cells[4].Value.ToString();
            txtpass.Text = dataGridView2.Rows[e.RowIndex].Cells[5].Value.ToString();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (txtfname.Text != "" && txtlname.Text != "" && txtmobi.Text != "" && txtpass.Text != "" && txtcnfrmpass.Text != "" && txtloginid.Text != "")
                {
                    con.Open();
                    string query = "select  * from tblmstusers WHERE loginId ='" + txtloginid.Text + "' and mobilenumber='" + txtmobi.Text + "'";
                    MySqlDataReader row;
                    row = con.ExecuteReader(query);
                    if (row.HasRows)
                    {
                        MessageBox.Show("Mobile number or LoginId already Exsist...", "Information");
                    }
                    else
                    {
                        if (txtpass.Text == txtcnfrmpass.Text)
                        {
                            con.Close();
                            con.Open();
                            string query3 = "INSERT INTO `wms`.`tblmstusers`(`firstname`,`lastname`,`mobilenumber`,`loginId`,`password`)VALUES('" + txtfname.Text + "','" + txtlname.Text + "','" + txtmobi.Text + "','" + txtloginid.Text + "','" + txtpass.Text + "')";
                            MySqlDataReader row3;
                            row3 = con.ExecuteReader(query3);
                            if (row3.RecordsAffected == 1)
                            {
                                //this.Close();
                                //Form1 frm2 = new Form1();
                                //frm2.Show();
                                MessageBox.Show("User added successfully...", "Information");
                                DisplayData();
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Please Enter Detail", "Information");
                }
            }
            catch
            {
                MessageBox.Show("Connection Error", "Information");
            }
        }
        private void DisplayData()
        {

            this.sp_vehicleTableAdapter.Fill(this.wmsDataSet18.sp_vehicle);
            this.sp_driverlistTableAdapter1.Fill(this.wmsDataSet17.sp_driverlist);
            this.sp_driverlistTableAdapter.Fill(this.wmsDataSet16.sp_driverlist);
            this.sp_wardlistTableAdapter.Fill(this.wmsDataSet15.sp_wardlist);
            this.sp_zonelistTableAdapter.Fill(this.wmsDataSet14.sp_zonelist);
            this.sp_vendorlistTableAdapter.Fill(this.wmsDataSet13.sp_vendorlist);
            this.sp_userlistTableAdapter.Fill(this.wmsDataSet12.sp_userlist);

            this.tbl_zoneTableAdapter.Fill(this.wmsDataSet4.tbl_zone);

           
        }
        //Clear Data  
        private void ClearData()
        {
            txtfname.Text = null; 
            txtlname.Text = null;
            txtmobi.Text = null;
            txtpass.Text = null;
            txtcnfrmpass.Text = null;
            txtloginid.Text = null;
            ID = 0;
            txtname.Text = null;
            txtadd1.Text = null;
            txtadd2.Text = null;
            txtcity.Text = null;
            txtstate.Text = null;
            ID1 = 0;
            txtzone.Text = null;
            ID2 = 0;
            txtward.Text = null;
            ID3 = 0;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtfname.Text != "" && txtlname.Text != "" && txtmobi.Text != "" && txtpass.Text != "" && txtloginid.Text != "")
                {
                    con.Open();
                    string query = "select  * from tblmstusers WHERE  userid=" + ID + "";
                    MySqlDataReader row;
                    row = con.ExecuteReader(query);
                    if (row.HasRows)
                    {                      
                            con.Close();
                            con.Open();
                            string query3 = "UPDATE `wms`.`tblmstusers` SET `firstname`='" + txtfname.Text + "',`lastname`='" + txtlname.Text + "',`mobilenumber`='" + txtmobi.Text + "',`loginId`='" + txtloginid.Text + "',`password`= '" + txtpass.Text + "' WHERE userid="+ID+"";
                            MySqlDataReader row3;
                            row3 = con.ExecuteReader(query3);
                            if (row3.RecordsAffected == 1)
                            {
                                //this.Close();
                                //Form1 frm2 = new Form1();
                                //frm2.Show();
                                MessageBox.Show("User detail updated successfully...", "Information");
                                DisplayData();
                                ClearData();
                            }                        
                    }
                }
                else
                {
                    MessageBox.Show("Please Select Record to Update", "Information");
                }
            }
            catch
            {
                MessageBox.Show("Connection Error", "Information");
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtfname.Text != "" && txtlname.Text != "" && txtmobi.Text != "" && txtpass.Text != ""  && txtloginid.Text != "")
                {
                    //con.Close();
                    con.Open();
                    string query3 = "delete from `wms`.`tblmstusers`  WHERE userid=" + ID + "";
                    MySqlDataReader row3;
                    row3 = con.ExecuteReader(query3);
                    if (row3.RecordsAffected == 1)
                    {
                        //this.Close();
                        //Form1 frm2 = new Form1();
                        //frm2.Show();
                        MessageBox.Show("User detail deleted successfully...", "Information");
                        DisplayData();
                        ClearData();
                    }
                }
                else
                {
                    MessageBox.Show("Please Select Record to Delete", "Information");
                }
            }
            catch
            {
                MessageBox.Show("Connection Error", "Information");
            }
        }

        private void dataGridView1_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ID1 = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
            txtname.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtadd1.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtadd2.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
            txtcity.Text = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
            txtstate.Text = dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString();
            txtpin.Text = dataGridView1.Rows[e.RowIndex].Cells[6].Value.ToString();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtname.Text != "")
                {
                    con.Open();
                    string query = "select  * from tbl_vendormst WHERE name ='" + txtname.Text + "'";
                    MySqlDataReader row;
                    row = con.ExecuteReader(query);
                    if (row.HasRows)
                    {                      
                            con.Close();
                            con.Open();

                            string query3 = "update `wms`.`tbl_vendormst`set `name`='" + txtname.Text + "',`address1`='" + txtadd1.Text + "',`address2`='" + txtadd2.Text + "',`city`='" + txtcity.Text + "',`state`='" + txtstate.Text + "',`pincode`= '" + txtpin.Text + "' where id="+ID1+"";
                            MySqlDataReader row3;
                            row3 = con.ExecuteReader(query3);
                            if (row3.RecordsAffected == 1)
                            {
                                
                                MessageBox.Show("Vendor updated successfully...", "Information");
                            DisplayData();
                            ClearData();
                        }

                    }
                }
                else
                {
                    MessageBox.Show("Please Select Record to Update", "Information");
                }
            }
            catch
            {
                MessageBox.Show("Connection Error", "Information");
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtname.Text != "")
                {
                    con.Open();
                    string query = "select  * from tbl_vendormst WHERE name ='" + txtname.Text + "'";
                    MySqlDataReader row;
                    row = con.ExecuteReader(query);
                    if (row.HasRows)
                    {
                        con.Close();
                        con.Open();

                        string query3 = "delete from  `wms`.`tbl_vendormst` where id=" + ID1 + "";
                        MySqlDataReader row3;
                        row3 = con.ExecuteReader(query3);
                        if (row3.RecordsAffected == 1)
                        {

                            MessageBox.Show("Vendor deleted successfully...", "Information");
                            DisplayData();
                            ClearData();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Please Select Record to Delete", "Information");
                }
            }
            catch
            {
                MessageBox.Show("Connection Error", "Information");
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtzone.Text != "")
                {
                    con.Open();
                    string query = "select  * from tbl_zone WHERE name ='" + txtzone.Text + "'";
                    MySqlDataReader row;
                    row = con.ExecuteReader(query);
                    if (row.HasRows)
                    {
                                           
                        con.Close();
                        con.Open();
                        string query3 = "update `wms`.`tbl_zone`set `name`='" + txtzone.Text + "' where id="+ID2+"";
                        MySqlDataReader row3;
                        row3 = con.ExecuteReader(query3);
                        if (row3.RecordsAffected == 1)
                        {
                            //this.Close();
                            //Form2 frm2 = new Form2();
                            //frm2.Show();
                            MessageBox.Show("Zone updated successfully...", "Information");
                            DisplayData();
                            ClearData();
                        }

                    }
                }
                else
                {
                    MessageBox.Show("Please Select Record to Update", "Information");
                }
            }
            catch
            {
                MessageBox.Show("Connection Error", "Information");
            }
        }

        private void dataGridView3_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ID2 = Convert.ToInt32(dataGridView3.Rows[e.RowIndex].Cells[0].Value.ToString());
            txtzone.Text = dataGridView3.Rows[e.RowIndex].Cells[1].Value.ToString();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtzone.Text != "")
                {
                    con.Open();
                    string query = "select  * from tbl_zone WHERE name ='" + txtzone.Text + "'";
                    MySqlDataReader row;
                    row = con.ExecuteReader(query);
                    if (row.HasRows)
                    {
                       

                        con.Close();
                        con.Open();
                        string query3 = "delete from  `wms`.`tbl_zone` where id=" + ID2 + "";
                        MySqlDataReader row3;
                        row3 = con.ExecuteReader(query3);
                        if (row3.RecordsAffected == 1)
                        {
                            con.Close();
                            con.Open();
                            string query4 = "delete from  `wms`.`tbl_ward` where zoneid=" + ID2 + "";
                            MySqlDataReader row4;
                            row4 = con.ExecuteReader(query4);

                            MessageBox.Show("Zone deleted successfully...", "Information");
                            DisplayData();
                            ClearData();
                        }

                    }
                }
                else
                {
                    MessageBox.Show("Please Select Record to Delete", "Information");
                }
            }
            catch
            {
                MessageBox.Show("Connection Error", "Information");
            }
        }

        private void dataGridView4_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ID3 = Convert.ToInt32(dataGridView4.Rows[e.RowIndex].Cells[0].Value.ToString());
            txtward.Text = dataGridView4.Rows[e.RowIndex].Cells[1].Value.ToString();
            cbzonename.Text = dataGridView4.Rows[e.RowIndex].Cells[2].Value.ToString();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtward.Text != "")
                {
                    con.Open();
                    string query = "select  * from tbl_ward WHERE name ='" + txtward.Text + "'";
                    MySqlDataReader row;
                    row = con.ExecuteReader(query);
                    if (row.HasRows)
                    {
                       
                        con.Close();
                        con.Open();
                        string query3 = "update  `wms`.`tbl_ward` set `name`='" + txtward.Text + "',`zoneid`= " + cbzonename.SelectedValue + " where id="+ID3+"";
                        MySqlDataReader row3;
                        row3 = con.ExecuteReader(query3);
                        if (row3.RecordsAffected == 1)
                        {
                            //this.Close();
                            //Form2 frm2 = new Form2();
                            //frm2.Show();
                            MessageBox.Show("Ward updated successfully...", "Information");
                            DisplayData();
                            ClearData();
                        }

                    }
                }
                else
                {
                    MessageBox.Show("Please Select Record to Update", "Information");
                }
            }
            catch
            {
                MessageBox.Show("Connection Error", "Information");
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtward.Text != "")
                {
                    con.Open();
                    string query = "select  * from tbl_ward WHERE name ='" + txtward.Text + "'";
                    MySqlDataReader row;
                    row = con.ExecuteReader(query);
                    if (row.HasRows)
                    {

                        con.Close();
                        con.Open();
                        string query3 = "delete from  `wms`.`tbl_ward`  where id=" + ID3 + "";
                        MySqlDataReader row3;
                        row3 = con.ExecuteReader(query3);
                        if (row3.RecordsAffected == 1)
                        {
                            //this.Close();
                            //Form2 frm2 = new Form2();
                            //frm2.Show();
                            MessageBox.Show("Ward Deleted successfully...", "Information");
                            DisplayData();
                            ClearData();
                        }

                    }
                }
                else
                {
                    MessageBox.Show("Please Select Record to Delete", "Information");
                }
            }
            catch
            {
                MessageBox.Show("Connection Error", "Information");
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtdrivfname.Text != "" && txtdrivelname.Text != "" && txtdrivmo.Text != "" && txtdob.Text != "" && txtdriveadd1.Text != "" && txtdrivadd2.Text != "" && txtdrivcity.Text != "" && txtdrivestate.Text != "" && txtdrivelice.Text != "")
                {
                    con.Open();
                    string query = "select  * from tbl_drivermst WHERE fname ='" + txtdrivfname.Text + "' and lname='" + txtdrivelname + "' and licenceid='" + txtdrivelice.Text + "'";
                    MySqlDataReader row;
                    row = con.ExecuteReader(query);
                    if (row.HasRows)
                    {
                      
                        con.Close();
                        con.Open();
                        string query3 = "update  `wms`.`tbl_drivermst` set `fname`='" + txtdrivfname.Text + "',`lname`= '" + txtdrivelname.Text + "',`mobile`= '" + txtdrivmo.Text + "',`dob`='" + txtdob.Text + "',`addressline1`='" + txtdriveadd1.Text + "',`addressline2`='" + txtdrivadd2.Text + "',`state`='" + txtdrivestate.Text + "',`city`='" + txtdrivcity.Text + "',`licenceid`='" + txtdrivelice.Text + "'  where id=" + ID4+"";
                          
                        MySqlDataReader row3;
                        row3 = con.ExecuteReader(query3);
                        if (row3.RecordsAffected == 1)
                        {
                            //this.Close();
                            //Form2 frm2 = new Form2();
                            //frm2.Show();
                            MessageBox.Show("Driver detail updated successfully...", "Information");
                            DisplayData();
                            ClearData();
                        }

                    }
                }
                else
                {
                    MessageBox.Show("Please Select Record to update", "Information");
                }
            }
            catch
            {
                MessageBox.Show("Connection Error", "Information");
            }
        }

        private void dataGridView5_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ID4 = Convert.ToInt32(dataGridView5.Rows[e.RowIndex].Cells[0].Value.ToString());
            txtdrivfname.Text = dataGridView5.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtdrivelname.Text = dataGridView5.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtdrivmo.Text = dataGridView5.Rows[e.RowIndex].Cells[3].Value.ToString();
            txtdob.Text = dataGridView5.Rows[e.RowIndex].Cells[4].Value.ToString();
            txtdriveadd1.Text = dataGridView5.Rows[e.RowIndex].Cells[5].Value.ToString();
            txtdrivadd2.Text = dataGridView5.Rows[e.RowIndex].Cells[6].Value.ToString();
            txtdrivcity.Text = dataGridView5.Rows[e.RowIndex].Cells[7].Value.ToString();
            txtdrivestate.Text = dataGridView5.Rows[e.RowIndex].Cells[8].Value.ToString();
            txtdrivelice.Text = dataGridView5.Rows[e.RowIndex].Cells[9].Value.ToString();
        }

        private void button14_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtdrivfname.Text != "")
                {
                    con.Open();
                    string query = "select  * from tbl_drivermst WHERE fname ='" + txtdrivfname.Text + "' and lname='" + txtdrivelname + "' and licenceid='" + txtdrivelice.Text + "'";
                    MySqlDataReader row;
                    row = con.ExecuteReader(query);
                    if (row.HasRows)
                    {

                        con.Close();
                        con.Open();
                        string query3 = "delete from   `wms`.`tbl_drivermst` where id=" + ID4 + "";

                        MySqlDataReader row3;
                        row3 = con.ExecuteReader(query3);
                        if (row3.RecordsAffected == 1)
                        {
                            //this.Close();
                            //Form2 frm2 = new Form2();
                            //frm2.Show();
                            MessageBox.Show("Driver detail deleted successfully...", "Information");
                            DisplayData();
                            ClearData();
                        }

                    }
                }
                else
                {
                    MessageBox.Show("Please Select Record to delete", "Information");
                }
            }
            catch
            {
                MessageBox.Show("Connection Error", "Information");
            }
        }

        //private void dataGridView6_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        //{
        //    ID5 = Convert.ToInt32(dataGridView6.Rows[e.RowIndex].Cells[0].Value.ToString());
        //    txtvehicleno.Text = dataGridView6.Rows[e.RowIndex].Cells[1].Value.ToString();
        //    txttyp.Text = dataGridView6.Rows[e.RowIndex].Cells[2].Value.ToString();
        //    cbvendername.Text = dataGridView6.Rows[e.RowIndex].Cells[3].Value.ToString();
        //    cbdrvrname.Text = dataGridView6.Rows[e.RowIndex].Cells[4].Value.ToString();
        //    comboBox1.Text = dataGridView6.Rows[e.RowIndex].Cells[5].Value.ToString();
        //    cbwardname.Text = dataGridView6.Rows[e.RowIndex].Cells[6].Value.ToString();

        //}

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                string  vend, zone, ward, driver, vehtyp;
                ID5 = txtvehicleno.Text;
                vend = cbvendername.SelectedItem.ToString();
                zone = cbzonename.SelectedItem.ToString();
                ward = cbwardname.SelectedItem.ToString();
                driver = cbdrvrname.SelectedItem.ToString();
                vehtyp = txttyp.Text;



                strProvider = "server=" + host + ";Database=" + database + ";User ID=" + userDB + ";Password=" + password;
                conn = new MySqlConnection(strProvider);
                conn.Open();
                string q1 = "sp_updatevehicle";
                MySqlCommand sd = new MySqlCommand(q1, conn);
                sd.CommandType = CommandType.StoredProcedure;
                //sd.Parameters.Add("@id", MySqlDbType.VarChar, 225).Value = ID5;
                sd.Parameters.Add("@veh", MySqlDbType.VarChar, 225).Value = ID5;
                sd.Parameters.Add("@typ", MySqlDbType.VarChar, 225).Value = vehtyp;
                sd.Parameters.Add("@zone", MySqlDbType.VarChar, 225).Value = zone;
                sd.Parameters.Add("@ward", MySqlDbType.VarChar, 225).Value = ward;
                sd.Parameters.Add("@driver", MySqlDbType.VarChar, 225).Value = driver;
                sd.Parameters.Add("@vend", MySqlDbType.VarChar, 225).Value = vend;

                try
                {

                    sd.ExecuteNonQuery();
                    MessageBox.Show("Vehicle detail updated Successfully...", "Information");
                    DisplayData();
                    ClearData();


                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    sd.Connection.Close();

                }
            }
            catch
            {
                MessageBox.Show("Please Select Record to update", "Information");
            }

        }

        private void button17_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtvehicleno.Text != "")
                {
                    con.Open();
                    string query = "select  * from tbl_mstvehicle WHERE number='"+ID5+"'";
                    MySqlDataReader row;
                    row = con.ExecuteReader(query);
                    if (row.HasRows)
                    {

                        con.Close();
                        con.Open();
                        string query3 = "delete from   `wms`.`tbl_drivermst` where number='" + ID5 + "'";

                        MySqlDataReader row3;
                        row3 = con.ExecuteReader(query3);
                        if (row3.RecordsAffected == 1)
                        {
                           
                            MessageBox.Show("Vehicle detail deleted successfully...", "Information");
                            DisplayData();
                            ClearData();
                        }

                    }
                }
                else
                {
                    MessageBox.Show("Please Select Record to delete", "Information");
                }
            }
            catch
            {
                MessageBox.Show("Connection Error", "Information");
            }
        }

        private void txtvehicleno_TextChanged(object sender, EventArgs e)
        {
            

            txtvehicleno.Text.ToUpper();
            if (txtvehicleno.Text.Length == 10)
            {
               
                txttyp.Text = null;
                cbvendername.Text = null;
                cbdrvrname.Text = null;
                comboBox1.Text = null;
                cbwardname.Text = null;
                con.Open();
                string query = "SELECT A.*,B.NAME AS vendor,c.fname as driver,z.name as zonename,w.name as wardname FROM wms.tbl_mstvehicle AS A JOIN wms.tbl_vendormst AS B on a.vendorid = b.id  left join wms.tbl_drivermst as c on a.driverid = c.id left join tbl_zone as z on a.zoneid = z.id left join tbl_ward as w on a.wardid = w.id where number = '" + txtvehicleno.Text + "'";
                MySqlDataReader row;
                row = con.ExecuteReader(query);
                if (row.HasRows)
                {
                    if (row.Read())
                    {
                        txttyp.Text = row["type"].ToString();
                        cbvendername.Text = row["vendor"].ToString();
                        cbdrvrname.Text = row["driver"].ToString();
                        comboBox1.Text = row["zonename"].ToString();
                        cbwardname.Text = row["wardname"].ToString();
                    }
                }
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbwardname.Items.Clear();
            con.Open();
            string que1 = "SELECT * FROM wms.tbl_ward where zoneid=(select id from tbl_zone where name='" + comboBox1.Text + "')";
            MySqlDataReader row6;
            row6 = con.ExecuteReader(que1);
            if (row6.HasRows)
            {
                while (row6.Read())
                {

                    cbwardname.Items.Add(row6["name"].ToString());
                }
            }
            else
            {
                cbwardname.Text = null;
            }
        }
    }
}
