﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Weighbridge_management
{
    public partial class Form3 : Form
    {
        dbconnection con = new dbconnection();
        string name, password;
        public Form3()
        {
            InitializeComponent();
            txtloginid.Text = Form1.loginid;
            //this.TopMost = true;
            //this.WindowState = FormWindowState.Maximized;
        }

        private void btnchngpass_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtloginid.Text != "" && txtpassword.Text != "")
                {

                    con.Open();
                    string query = "UPDATE wms.tblmstusers SET  password ='" + txtpassword.Text + "' WHERE loginId ='" + txtloginid.Text + "'";
                    MySqlDataReader row;
                    row = con.ExecuteReader(query);
                    if (row.RecordsAffected==1)
                    {
                        while (row.Read())
                        {
                            name = row["loginid"].ToString();
                            password = row["password"].ToString();
                        }
                        this.Hide();
                        MessageBox.Show("Password updated successfully..", "Information");
                        Form1 frm1 = new Form1();
                        frm1.Show();

                    }
                    else
                    {
                        MessageBox.Show("Please check loginid..", "Information");
                    }
                }
                else
                {
                    MessageBox.Show("Loginid or Password is empty", "Information");
                }
            }
            catch
            {
                MessageBox.Show("Connection Error", "Information");
            }
        }

       
    }
}
