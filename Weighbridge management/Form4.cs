﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Weighbridge_management
{
    public partial class Form4 : Form
    {
        dbconnection con = new dbconnection();
        public Form4()
        {
            InitializeComponent();
            if (Form2.trip1 == true)
            {
                tbvehoutdry.Checked = true;
                rbvehclout.Checked = false;
            }
            else
            {
                rbvehclout.Checked = true;
                tbvehoutdry.Checked = false;
            }
        }

        private void btncntnu_Click(object sender, EventArgs e)
        {
           
            if (rbvehclout.Checked == true)
            {
                
                    con.Open();
                    string que1 = "update `wms`.`tbl_trip` set `weight3`='" + Form2.netweight + "',`weight4`='"+ Form2.w3 + "',outdatetime=now() where vehicleid=(select id from tbl_mstvehicle where number='" + Form2.vehno + "') and outdatetime is null";
                    MySqlDataReader r1;
                    r1 = con.ExecuteReader(que1);
                    con.Close();
                    this.Close();
                    MessageBox.Show("Trip completed successfully...");
                //Form8 frm8 = new Form8();
                //frm8.ShowDialog();
            }
            else if(tbvehoutdry.Checked==true)
            {
                con.Open();
                string query2 = "update `wms`.`tbl_trip` set `weight2`='" + Form2.tareweight + "', taredatetime=now(),tareuser='"+Form1.loginid+"' where vehicleid=(select id from tbl_mstvehicle where number='" + Form2.vehno + "') and taredatetime is null";
                MySqlDataReader row2;
                row2 = con.ExecuteReader(query2);               
                    con.Close();
                    this.Close();                    
                
            }
        }

        private void btncancle_Click(object sender, EventArgs e)
        {
            this.Close();

        }
    }
}
