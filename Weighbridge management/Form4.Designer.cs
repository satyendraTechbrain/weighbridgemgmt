﻿
namespace Weighbridge_management
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblvehclexit = new System.Windows.Forms.Label();
            this.rbvehclout = new System.Windows.Forms.RadioButton();
            this.tbvehoutdry = new System.Windows.Forms.RadioButton();
            this.btncntnu = new System.Windows.Forms.Button();
            this.btncancle = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblvehclexit
            // 
            this.lblvehclexit.AutoSize = true;
            this.lblvehclexit.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblvehclexit.Location = new System.Drawing.Point(203, 75);
            this.lblvehclexit.Name = "lblvehclexit";
            this.lblvehclexit.Size = new System.Drawing.Size(420, 31);
            this.lblvehclexit.TabIndex = 0;
            this.lblvehclexit.Text = "Please Select vehicle exit type.";
            this.lblvehclexit.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // rbvehclout
            // 
            this.rbvehclout.AutoSize = true;
            this.rbvehclout.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbvehclout.Location = new System.Drawing.Point(243, 138);
            this.rbvehclout.Name = "rbvehclout";
            this.rbvehclout.Size = new System.Drawing.Size(214, 35);
            this.rbvehclout.TabIndex = 1;
            this.rbvehclout.TabStop = true;
            this.rbvehclout.Text = "VEHICLE OUT";
            this.rbvehclout.UseVisualStyleBackColor = true;
            // 
            // tbvehoutdry
            // 
            this.tbvehoutdry.AutoSize = true;
            this.tbvehoutdry.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbvehoutdry.Location = new System.Drawing.Point(243, 191);
            this.tbvehoutdry.Name = "tbvehoutdry";
            this.tbvehoutdry.Size = new System.Drawing.Size(337, 35);
            this.tbvehoutdry.TabIndex = 2;
            this.tbvehoutdry.TabStop = true;
            this.tbvehoutdry.Text = "VEHICLE OUT + DRY IN";
            this.tbvehoutdry.UseVisualStyleBackColor = true;
            // 
            // btncntnu
            // 
            this.btncntnu.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncntnu.Location = new System.Drawing.Point(209, 256);
            this.btncntnu.Name = "btncntnu";
            this.btncntnu.Size = new System.Drawing.Size(151, 44);
            this.btncntnu.TabIndex = 3;
            this.btncntnu.Text = "Continue";
            this.btncntnu.UseVisualStyleBackColor = true;
            this.btncntnu.Click += new System.EventHandler(this.btncntnu_Click);
            // 
            // btncancle
            // 
            this.btncancle.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncancle.Location = new System.Drawing.Point(472, 256);
            this.btncancle.Name = "btncancle";
            this.btncancle.Size = new System.Drawing.Size(151, 44);
            this.btncancle.TabIndex = 4;
            this.btncancle.Text = "Cancle";
            this.btncancle.UseVisualStyleBackColor = true;
            this.btncancle.Click += new System.EventHandler(this.btncancle_Click);
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btncancle);
            this.Controls.Add(this.btncntnu);
            this.Controls.Add(this.tbvehoutdry);
            this.Controls.Add(this.rbvehclout);
            this.Controls.Add(this.lblvehclexit);
            this.Name = "Form4";
            this.Text = "Form4";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblvehclexit;
        private System.Windows.Forms.RadioButton rbvehclout;
        private System.Windows.Forms.RadioButton tbvehoutdry;
        private System.Windows.Forms.Button btncntnu;
        private System.Windows.Forms.Button btncancle;
    }
}