﻿
namespace Weighbridge_management
{
    partial class Form5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btncancle = new System.Windows.Forms.Button();
            this.btncntnu = new System.Windows.Forms.Button();
            this.rbvehclout = new System.Windows.Forms.RadioButton();
            this.lblvehclexit = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btncancle
            // 
            this.btncancle.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncancle.Location = new System.Drawing.Point(421, 241);
            this.btncancle.Name = "btncancle";
            this.btncancle.Size = new System.Drawing.Size(151, 44);
            this.btncancle.TabIndex = 9;
            this.btncancle.Text = "Cancle";
            this.btncancle.UseVisualStyleBackColor = true;
            // 
            // btncntnu
            // 
            this.btncntnu.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncntnu.Location = new System.Drawing.Point(196, 241);
            this.btncntnu.Name = "btncntnu";
            this.btncntnu.Size = new System.Drawing.Size(151, 44);
            this.btncntnu.TabIndex = 8;
            this.btncntnu.Text = "Continue";
            this.btncntnu.UseVisualStyleBackColor = true;
            this.btncntnu.Click += new System.EventHandler(this.btncntnu_Click);
            // 
            // rbvehclout
            // 
            this.rbvehclout.AutoSize = true;
            this.rbvehclout.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbvehclout.Location = new System.Drawing.Point(230, 176);
            this.rbvehclout.Name = "rbvehclout";
            this.rbvehclout.Size = new System.Drawing.Size(214, 35);
            this.rbvehclout.TabIndex = 6;
            this.rbvehclout.TabStop = true;
            this.rbvehclout.Text = "VEHICLE OUT";
            this.rbvehclout.UseVisualStyleBackColor = true;
            // 
            // lblvehclexit
            // 
            this.lblvehclexit.AutoSize = true;
            this.lblvehclexit.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblvehclexit.Location = new System.Drawing.Point(190, 113);
            this.lblvehclexit.Name = "lblvehclexit";
            this.lblvehclexit.Size = new System.Drawing.Size(420, 31);
            this.lblvehclexit.TabIndex = 5;
            this.lblvehclexit.Text = "Please Select vehicle exit type.";
            this.lblvehclexit.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Form5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btncancle);
            this.Controls.Add(this.btncntnu);
            this.Controls.Add(this.rbvehclout);
            this.Controls.Add(this.lblvehclexit);
            this.Name = "Form5";
            this.Text = "Form5";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btncancle;
        private System.Windows.Forms.Button btncntnu;
        private System.Windows.Forms.RadioButton rbvehclout;
        private System.Windows.Forms.Label lblvehclexit;
    }
}