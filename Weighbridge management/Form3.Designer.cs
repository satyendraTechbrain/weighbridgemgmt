﻿
namespace Weighbridge_management
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.txtpassword = new System.Windows.Forms.TextBox();
            this.lblchgnpass = new System.Windows.Forms.Label();
            this.txtloginid = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnchngpass = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(172, 140);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(456, 31);
            this.label3.TabIndex = 17;
            this.label3.Text = "Weighbridge Management System";
            // 
            // txtpassword
            // 
            this.txtpassword.Location = new System.Drawing.Point(407, 232);
            this.txtpassword.Name = "txtpassword";
            this.txtpassword.Size = new System.Drawing.Size(100, 20);
            this.txtpassword.TabIndex = 16;
            // 
            // lblchgnpass
            // 
            this.lblchgnpass.AutoSize = true;
            this.lblchgnpass.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchgnpass.Location = new System.Drawing.Point(244, 232);
            this.lblchgnpass.Name = "lblchgnpass";
            this.lblchgnpass.Size = new System.Drawing.Size(146, 20);
            this.lblchgnpass.TabIndex = 15;
            this.lblchgnpass.Text = "Add New Password";
            // 
            // txtloginid
            // 
            this.txtloginid.Location = new System.Drawing.Point(407, 194);
            this.txtloginid.Name = "txtloginid";
            this.txtloginid.Size = new System.Drawing.Size(100, 20);
            this.txtloginid.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(244, 194);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 20);
            this.label1.TabIndex = 13;
            this.label1.Text = "User Name";
            // 
            // btnchngpass
            // 
            this.btnchngpass.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnchngpass.Location = new System.Drawing.Point(274, 271);
            this.btnchngpass.Name = "btnchngpass";
            this.btnchngpass.Size = new System.Drawing.Size(198, 36);
            this.btnchngpass.TabIndex = 12;
            this.btnchngpass.Text = "Change Password";
            this.btnchngpass.UseVisualStyleBackColor = true;
            this.btnchngpass.Click += new System.EventHandler(this.btnchngpass_Click);
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtpassword);
            this.Controls.Add(this.lblchgnpass);
            this.Controls.Add(this.txtloginid);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnchngpass);
            this.Name = "Form3";
            this.Text = "Form3";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtpassword;
        private System.Windows.Forms.Label lblchgnpass;
        private System.Windows.Forms.TextBox txtloginid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnchngpass;
    }
}