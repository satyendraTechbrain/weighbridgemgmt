﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.IO.Ports;
using System.Threading;
using System.Globalization;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using System.Data.OleDb;


namespace Weighbridge_management
{
    public partial class Form2 : Form
    {

        public SerialPort sp;
        string dataReceived = string.Empty;
        //private delegate void SetTextDeleg(string text);


        dbconnection con = new dbconnection();
        //SqlDataAdapter da;
        bool ischange = false;
        bool islogout = false;
        bool ischeck1 = true;
        bool ischeck2 = false;
        public static string vehno = "";
        public static string vehid = "";
        public static string netweight = "";
        public static int w3;
        public static int id;
        public static string tareweight = "";
        public static string time1 = "";
        public static string date1 = "";
        public static string date2 = "";
        public static string veh = "";
        public static string type = "";
        public static string vend = "";
        public static string nettx = "";
        public static string weight3 = "";
        public static bool trip1 = false;


        MySql.Data.MySqlClient.MySqlConnection conn;
        static string host = "localhost";
        static string database = "wms";
        static string userDB = "root";
        static string password = "Techbrain@123";
        public static string strProvider = "server=" + host + ";Database=" + database + ";User ID=" + userDB + ";Password=" + password;
        public Form2()
        {
            InitializeComponent();
            //this.TopMost = true;
            //this.WindowState = FormWindowState.Maximized;
           
        }

        private void btnlogout_Click(object sender, EventArgs e)
        {
            islogout = true;
            this.Close();
            Form1 frm1 = new Form1();
            frm1.Show();
        }

        private void gbrecorddetail_Enter(object sender, EventArgs e)
        {

        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (islogout == false)
                Application.Exit();
        }

        private void rbtnvehcl_CheckedChanged(object sender, EventArgs e)
        {
            ischeck1 = true;
            ischeck2 = false;
            if (ischeck1 == true)
            {
               
                ischeck2 = false;
            }
        }

        private void rbtnbin_CheckedChanged(object sender, EventArgs e)
        {
            ischeck2 = true;
            ischeck1 = false;
            if (ischeck2 == true)
            {
               
                ischeck1 = false;
            }
        }

        private void btnchangepassword_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form3 frm3 = new Form3();
            frm3.ShowDialog();
        }

        public void fillgrid()

        {
            con.Open();
            string query = "SELECT A.*,B.NAME AS vendor,c.fname as driver,z.name as zonename,w.name as wardname FROM wms.tbl_mstvehicle AS A JOIN wms.tbl_vendormst AS B on a.vendorid = b.id  left join wms.tbl_drivermst as c on a.driverid = c.id left join tbl_zone as z on a.zoneid = z.id left join tbl_ward as w on a.wardid = w.id where number = '" + txtvehicleno.Text + "'";
            MySqlDataReader row;
            row = con.ExecuteReader(query);

            if (row.HasRows)
            {
                if (row.Read())
                {
                    vehno = txtvehicleno.Text;
                    vehid = row["id"].ToString();
                    txttyp.Text = row["type"].ToString();
                    cbvendername.Text = row["vendor"].ToString();
                    cbzonename.Text = row["zonename"].ToString();
                    cbwardname.Text = row["wardname"].ToString();
                    cbdrvrname.Text = row["driver"].ToString();
                    con.Close();
                }
            }
            else
            {
                MessageBox.Show("Please Check Vehicle number", "Information");
            }

        }
        void tabControl1_Selecting(object sender, TabControlCancelEventArgs e)
        {
            TabPage current = (sender as TabControl).SelectedTab;
           // MessageBox.Show(tb1vehicleweigh.SelectedTab.ToString());
            this.sp_getpendingtripdetailTableAdapter3.Fill(this.wmsDataSet8.sp_getpendingtripdetail);
           
        }


        private void Form2_Load(object sender, EventArgs e)
        {
            con.Open();
            string query0 = "SELECT * FROM wms.tbl_serialport";
            MySqlDataReader row0;
            row0 = con.ExecuteReader(query0);
            if (row0.HasRows)
            {
                if (row0.Read())
                {
                    string portname = row0["portname"].ToString();                    
                    int baudrate = int.Parse(row0["portname"].ToString());
                    int databits = int.Parse(row0["databits"].ToString());
                    sp = new SerialPort(portname, baudrate, Parity.None, databits, StopBits.One);
                }
            }




            if (ischange == false)
            {
                time1 = dateTimePicker3.Text;

            }
            // TODO: This line of code loads data into the 'wmsDataSet10.daysummary' table. You can move, or remove it, as needed.
            this.daysummaryTableAdapter1.Fill(this.wmsDataSet10.daysummary);
      
            tb1vehicleweigh.Selecting += new TabControlCancelEventHandler(tabControl1_Selecting);

           
            // TODO: This line of code loads data into the 'wmsDataSet8.sp_getpendingtripdetail' table. You can move, or remove it, as needed.
            this.sp_getpendingtripdetailTableAdapter3.Fill(this.wmsDataSet8.sp_getpendingtripdetail);
           


            lblusername.Text = Form1.loginid.ToUpper();
            con.Open();
            string query = "SELECT * FROM wms.tbl_vendormst order by name";
            MySqlDataReader row;
            row = con.ExecuteReader(query);
            if (row.HasRows)
            {

                while (row.Read())
                {
                    cbvendername.Items.Add(row["name"].ToString());
                    cbvndrname.Items.Add(row["name"].ToString());
                    comboBox2.Items.Add(row["name"].ToString());
                }
            }

            con.Close();
            con.Open();
            string que = "SELECT * FROM wms.tbl_zone order by name";
            MySqlDataReader row5;
            row5 = con.ExecuteReader(que);
            if (row5.HasRows)
            {
                while (row5.Read())
                {
                    cbzonename.Items.Add(row5["name"].ToString());
                    cbznname.Items.Add(row5["name"].ToString());
                }
            }

           

            con.Close();
            con.Open();
            string que2 = "SELECT * FROM wms.tbl_drivermst order by fname";
            MySqlDataReader row7;
            row7 = con.ExecuteReader(que2);
            if (row7.HasRows)
            {
                while (row7.Read())
                {
                    cbdrvrname.Items.Add(row7["fname"].ToString());
                    cbdrivername.Items.Add(row7["fname"].ToString());
                }
            }

            cbwastetype.Items.Add("MSW");
            cbwastetype.Items.Add("WET");
            cbwastetype.Items.Add("DRY");
            cbwsttyp.Items.Add("MSW");
            cbwsttyp.Items.Add("WET");
            cbwsttyp.Items.Add("DRY");
            cbwastetype.SelectedItem=cbwastetype.Items[0];
            cbwsttyp.SelectedItem= cbwsttyp.Items[0];
            con.Close();

            this.reportViewer1.RefreshReport();
        }

        private void txtvehicleno_TextChanged_1(object sender, EventArgs e)
        {
            cbvendername.Text = null;
            txttyp.Text = null;
            cbdrvrname.Text = null;
            cbzonename.Text = null;
            cbwardname.Text = null;
            textBox1.Text = null;
            grosstxt.Text = null;
            txtgrosstime.Text = null;
            txttaretime.Text = null;
            txttareweight.Text = null;
            txtnetweight.Text = null;
            txtweighweight.Text = null;
           
            txtvehicleno.Text.ToUpper();
            if (txtvehicleno.Text.Length == 10)
            {
                fillgrid();
            }

            this.Show();
        }

        private void btncapture_Click_1(object sender, EventArgs e)
        {            
            
        }

        private void btnnewrecord_Click_1(object sender, EventArgs e)
        {
            cbvendername.Text = null;
            txttyp.Text = null;
            cbdrvrname.Text = null;
            cbzonename.Text = null;
            cbwardname.Text = null;
            textBox1.Text = null;
            grosstxt.Text = null;
            txtgrosstime.Text = null;
            txttaretime.Text = null;
            txttareweight.Text = null;
            txtnetweight.Text = null;
            txtweighweight.Text = null;
           
            txtslipno.Text = null;
            txtvehicleno.Text = null;
            cbwastetype.Text = null;
        }

        private void txtweighweight_TextChanged_1(object sender, EventArgs e)
        {
            if (txtweighweight.Text == null)
            {
                cbvendername.Text = null;
                txttyp.Text = null;
                cbdrvrname.Text = null;
                cbzonename.Text = null;
                cbwardname.Text = null;
                textBox1.Text = null;
                cbwastetype.Text = null;
                grosstxt.Text = null;
                txtgrosstime.Text = null;
                txttaretime.Text = null;
                txttareweight.Text = null;
                txtnetweight.Text = null;
                txtweighweight.Text = null;
               
                txtvehicleno.Text = null;
                txtslipno.Text = null;
            }
        }

        private void btnconfig_Click(object sender, EventArgs e)
        {
     
            Form6 frm6 = new Form6();
            frm6.ShowDialog();
            
        }

        private void cbzonename_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbwardname.Items.Clear();
            con.Open();
              string que1 = "SELECT * FROM wms.tbl_ward where zoneid=(select id from tbl_zone where name='" + cbzonename.Text + "')";
            MySqlDataReader row6;
            row6 = con.ExecuteReader(que1);
            if (row6.HasRows)
            {
                while (row6.Read())
                {
                   
                    cbwardname.Items.Add(row6["name"].ToString());
                }
            }
            else
            {
                cbwardname.Text = null;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //try
            //{
            //    if (sp.IsOpen)
            //    {
            //        dataReceived += sp.ReadExisting();

            //        //using (StreamWriter writer = new StreamWriter(@"c:\weight.txt",true))
            //        //{
            //        //    writer.WriteLine(dataReceived);
            //        //}


            //        var sRec = dataReceived.Split('€');
            //        dataReceived = "";
            //        foreach (var item in sRec)
            //        {
            //            int intWeight = 0;
            //            if (item.Length > 8)
            //                if (int.TryParse(item.Substring(0, 8).Trim('?').Trim(), out intWeight))
            //                    txtweighweight.Text = intWeight.ToString();
            //                else
            //                    txtweighweight.Text = item;
            //        }

            //    }
            //    else
            //    {
            //        sp.Open();
            //    }
            //}
            //catch
            //{
            //    MessageBox.Show("COM Not found");
            //}
        }

        private void btncompltrip_Click(object sender, EventArgs e)
        {
            if(dataGridView2.SelectedRows.Count >0)
            {
                object obj= dataGridView2.SelectedRows[0].Cells[0].Value.ToString();
                id = int.Parse(obj.ToString());
                con.Open();
                string query = "SELECT * FROM wms.tbl_trip  where id = " + int.Parse(obj.ToString()) + "";
                MySqlDataReader row;
                row = con.ExecuteReader(query);
                if (row.HasRows)
                {
                    if (row.Read())
                    {                        
                        int veh = int.Parse(row["vehicleid"].ToString());
                        if (veh != 0)
                        {
                            //con.Close();
                            con.Open();
                            string query1 = "SELECT * FROM wms.tbl_mstvehicle  where id = '" + veh + "'";
                            MySqlDataReader row1;
                            row1 = con.ExecuteReader(query1);
                            if(row1.HasRows)
                            {
                                if (row1.Read())
                                {
                                    txtvehicleno.Text= row1["number"].ToString();
                                    if (txtvehicleno.Text != null)
                                    {
                                        fillgrid();
                                    }
                                }
                            }
                            txtslipno.Text = row["id"].ToString();
                            grosstxt.Text = row["weight1"].ToString();
                            txtgrosstime.Text = row["indatetime"].ToString();
                           


                        }

                        tb1vehicleweigh.SelectTab(0);
                    }
                }
            }
            else
            {
                MessageBox.Show ("Please select row");
            }

        }

        private void btnignoretrip_Click(object sender, EventArgs e)
        {
            object obj = dataGridView2.SelectedRows[0].Cells[0].Value.ToString();
            id = int.Parse(obj.ToString());
            Form7 frm7 = new Form7();
            frm7.ShowDialog();
            con.Open();
            this.sp_getpendingtripdetailTableAdapter3.Fill(this.wmsDataSet8.sp_getpendingtripdetail);
            this.daysummaryTableAdapter1.Fill(this.wmsDataSet10.daysummary);
        }

        private void btnsaveprint_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtvehicleno.Text != "")
                {
                    con.Close();
                    con.Open();
                    string query1 = "SELECT * FROM wms.tbl_trip where vehicleid=(select id from tbl_mstvehicle where number='" + txtvehicleno.Text + "') and outdatetime is null;";
                    MySqlDataReader row1;
                    row1 = con.ExecuteReader(query1);
                    if (row1.HasRows)
                    {
                        trip1 = false;

                        //door to door
                        if (row1.Read())
                        {
                            string typ = row1["type"].ToString();

                            ////msw
                            if (typ == "MSW")
                            {
                                float f1, m1, net1;
                                grosstxt.Text = row1["weight1"].ToString();
                                txtslipno.Text = row1["id"].ToString();
                                float.TryParse(grosstxt.Text, out f1);
                                txtgrosstime.Text = row1["indatetime"].ToString();
                                textBox1.Text = row1["tripid"].ToString();
                                
                                txttareweight.Text = txtweighweight.Text;
                                weight3 = txtweighweight.Text;
                                float.TryParse(txtweighweight.Text, out m1);
                                net1 = (f1 - m1);
                                nettx = net1.ToString();
                                txtnetweight.Text = net1.ToString();
                                cbwastetype.Text = "MSW";
                                con.Close();
                                Form5 frm5 = new Form5();
                                frm5.ShowDialog();
                                con.Open();
                                string quer1 = "SELECT * FROM wms.tbl_trip where  id=" + txtslipno.Text + " and vehicleid=(select id from tbl_mstvehicle where number='" + txtvehicleno.Text + "' ) ;";
                                MySqlDataReader ro1;
                                ro1 = con.ExecuteReader(quer1);
                                if (ro1.HasRows)
                                {
                                    if (ro1.Read())
                                    {                                       
                                        txttaretime.Text = ro1["outdatetime"].ToString();
                                    }
                                    this.sp_getpendingtripdetailTableAdapter2.Fill(this.wmsDataSet7.sp_getpendingtripdetail);
                                    this.daysummaryTableAdapter1.Fill(this.wmsDataSet10.daysummary);
                                }
                                con.Close();

                               
                                this.sp_getpendingtripdetailTableAdapter2.Fill(this.wmsDataSet7.sp_getpendingtripdetail);
                                this.daysummaryTableAdapter1.Fill(this.wmsDataSet10.daysummary);

                            }
                            /////door to door
                            else if (typ == "WET")
                            {

                                con.Close();
                                con.Open();
                                this.sp_getpendingtripdetailTableAdapter2.Fill(this.wmsDataSet7.sp_getpendingtripdetail);
                                this.daysummaryTableAdapter1.Fill(this.wmsDataSet10.daysummary);
                                string quer = "SELECT * FROM wms.tbl_trip where  id=" + txtslipno.Text + " and vehicleid=(select id from tbl_mstvehicle where number='" + txtvehicleno.Text + "') and taredatetime is null;";
                                MySqlDataReader ro;
                                ro = con.ExecuteReader(quer);
                                if (ro.HasRows)
                                {

                                    if (ro.Read())
                                    {

                                        grosstxt.Text = ro["weight1"].ToString();
                                        txtslipno.Text = ro["id"].ToString();
                                        txtgrosstime.Text = ro["indatetime"].ToString();
                                        textBox1.Text = ro["tripid"].ToString();

                                        tareweight = txtweighweight.Text;
                                        cbwastetype.Text = "DRY";
                                        con.Close();
                                        Form4 frm4 = new Form4();
                                        frm4.ShowDialog();

                                       

                                    }

                                    this.sp_getpendingtripdetailTableAdapter2.Fill(this.wmsDataSet7.sp_getpendingtripdetail);
                                    this.daysummaryTableAdapter1.Fill(this.wmsDataSet10.daysummary);
                                }
                                else
                                {
                                    con.Open();
                                    this.sp_getpendingtripdetailTableAdapter2.Fill(this.wmsDataSet7.sp_getpendingtripdetail);
                                    this.daysummaryTableAdapter1.Fill(this.wmsDataSet10.daysummary);
                                    string Q1 = "SELECT * FROM wms.tbl_trip where  id=" + txtslipno.Text + " and vehicleid=(select id from tbl_mstvehicle where number='" + txtvehicleno.Text + "') and outdatetime is null;";
                                    MySqlDataReader R1;
                                    R1 = con.ExecuteReader(Q1);
                                    if (R1.HasRows)
                                    {
                                        if (R1.Read())
                                        {
                                            cbwastetype.Text = "DRY";
                                            grosstxt.Text = R1["weight1"].ToString();
                                            txtslipno.Text = R1["id"].ToString();
                                            txtgrosstime.Text = R1["indatetime"].ToString();
                                            textBox1.Text = R1["tripid"].ToString();
                                            // txttareweight.Text = R1["weight2"].ToString();
                                            //txttaretime.Text = R1["taredatetime"].ToString();
                                           
                                            netweight = txtweighweight.Text;
                                            w3 = int.Parse(R1["weight1"].ToString()) - int.Parse(netweight);
                                            con.Close();
                                            Form4 frm4 = new Form4();
                                            frm4.ShowDialog();
                                            txtnetweight.Text = w3.ToString();

                                            con.Open();
                                            string quer1 = "SELECT * FROM wms.tbl_trip where  id=" + txtslipno.Text + " and vehicleid=(select id from tbl_mstvehicle where number='" + txtvehicleno.Text + "' ) and tripid=" + textBox1.Text + " ;";
                                            MySqlDataReader ro1;
                                            ro1 = con.ExecuteReader(quer1);
                                            if (ro1.HasRows)
                                            {
                                                if (ro1.Read())
                                                {
                                                    txttareweight.Text = ro1["weight3"].ToString();
                                                    txttaretime.Text = ro1["outdatetime"].ToString();
                                                }
                                                this.sp_getpendingtripdetailTableAdapter2.Fill(this.wmsDataSet7.sp_getpendingtripdetail);
                                                this.daysummaryTableAdapter1.Fill(this.wmsDataSet10.daysummary);
                                            }
                                            con.Close();
                                        }

                                    }
                                }


                            }

             

                        }

                    }


                    else
                    {

                        trip1 = true;
                        int tripid = 1;
                        con.Close();
                        con.Open();
                        string qry = "SELECT * FROM wms.tbl_trip where vehicleid=(select id from tbl_mstvehicle where number='" + txtvehicleno.Text + "')  and outdatetime> TIMESTAMP(CURDATE())  order by id desc limit 1;";
                        MySqlDataReader rw1;
                        rw1 = con.ExecuteReader(qry);

                        if (rw1.HasRows)
                        {
                            if (rw1.Read())
                            {

                                tripid = tripid + int.Parse(rw1["tripid"].ToString()); ;
                            }

                        }
                        else
                        {
                            tripid = 1;
                        }
                        con.Close();
                        con.Open();

                        string query3 = "INSERT INTO `wms`.`tbl_trip`(`vehicleid`,`indatetime`,`weight1`,`type`,`tripid`,`grossuser`)VALUES(" + vehid + ",now(),'" + txtweighweight.Text + "','" + cbwastetype.SelectedItem.ToString() + "'," + tripid + ",'"+Form1.loginid+"')";
                        MySqlDataReader row3;
                        row3 = con.ExecuteReader(query3);
                        if (row3.RecordsAffected == 1)
                        {
                            con.Close();
                            con.Open();
                            string query4 = "SELECT * FROM wms.tbl_trip where vehicleid=(select id from tbl_mstvehicle where number='" + txtvehicleno.Text + "') and outdatetime is null;";
                            MySqlDataReader row4;
                            row4 = con.ExecuteReader(query4);
                            if (row4.HasRows)
                            {
                                if (row4.Read())
                                {

                                    txtslipno.Text = row4["id"].ToString();
                                    txtgrosstime.Text = row4["indatetime"].ToString();
                                    textBox1.Text = row4["tripid"].ToString();
                                    grosstxt.Text = txtweighweight.Text;
                                    cbwastetype.Text = row4["type"].ToString();
                                    MessageBox.Show("Vehicle IN");
                                   
                                    this.sp_getpendingtripdetailTableAdapter2.Fill(this.wmsDataSet7.sp_getpendingtripdetail);
                                    con.Close();
                                    this.daysummaryTableAdapter1.Fill(this.wmsDataSet10.daysummary);


                                }
                            }

                        }
                    }


                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnverifyfinger_Click(object sender, EventArgs e)
        {

        }

        private void btnviewreprt_Click(object sender, EventArgs e)
        {
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.ReportPath = "Report1.rdlc";
            ReportDataSource rds = new ReportDataSource("DataSet1", getdetail());
            this.reportViewer1.LocalReport.DataSources.Add(rds);
            this.reportViewer1.LocalReport.Refresh();
            this.reportViewer1.RefreshReport();

        }
        private List<report> getdetail()
        {
           
           
            string time1 = Form2.time1;
            type = cbrepttyp.SelectedItem.ToString();
            vend = comboBox2.SelectedItem.ToString();
            strProvider = "server=" + host + ";Database=" + database + ";User ID=" + userDB + ";Password=" + password;
            conn = new MySqlConnection(strProvider);
            conn.Open();
            string q1 = "sp_report";
            MySqlCommand sd = new MySqlCommand(q1, conn);
            sd.CommandType = CommandType.StoredProcedure;
            sd.Parameters.Add("@typ", MySqlDbType.VarChar, 225).Value = type;
            sd.Parameters.Add("@vend", MySqlDbType.VarChar, 225).Value = vend;
            sd.Parameters.Add("@fromdate", MySqlDbType.VarChar, 225).Value = time1;
            MySqlDataReader read = sd.ExecuteReader();
            List<report> TestList = new List<report>();
            report test = null;
            if (read.HasRows)
            {
                while (read.Read())
                {
                    test = new report();
                    test.slipno = int.Parse(read["id"].ToString());
                    test.trip = int.Parse(read["tripid"].ToString());
                    test.vehicle = read["number"].ToString();
                    test.type = read["type"].ToString();
                    test.ward = read["name"].ToString();
                    test.indatetime = DateTime.Parse(read["grossdate"].ToString());
                    test.intime = DateTime.Parse(read["intime"].ToString());
                    test.outtime = DateTime.Parse(read["outtime"].ToString());                    
                    test.outtime.ToString("hh:mm tt");
                    test.weight1 = read["weight1"].ToString();
                    test.weight3 = read["weight3"].ToString();
                    test.weight4 = read["weight4"].ToString();
                    if(type== "Residential")
                    {
                        test.time = " 12:00 AM To 04:59 PM (Residential)";
                        test.date = time1;
                        test.vend = vend;
                    }
                    else
                    {
                        test.time = " 05:00 PM To 11:59 PM (Commercial)";
                        test.date = time1;
                        test.vend = vend;
                    }
                    var culture = new CultureInfo("en-US");
                    test.print= DateTime.Parse(DateTime.Now.ToString(culture));                  
                    test.print.ToString("dd-MM-yyyy hh:mm tt");

                    TestList.Add(test);
                }
            }
            else
            {
                TestList.Add(null);
            }

            return TestList;
        }
        private void tb7ReportsMore_Click(object sender, EventArgs e)
        {
            
        }

        public void dateTimePicker3_ValueChanged(object sender, EventArgs e)
        {
            ischange = true;
            time1 = dateTimePicker3.Value.ToString("dd-MM-yyyy");
        }

        private void btngetrec_Click(object sender, EventArgs e)
        {
            //SEARCH BY VEHICLE NUMBER
             date1=dateTimePicker2.Text;
             date2=dateTimePicker1.Text;
             veh = txtvehclnu.Text;
            if (txtvehclnu.Text != "" && dateTimePicker2.Text!="" && dateTimePicker1.Text != "")
            {
                string vehno = txtvehclnu.Text; con.Close();
                con.Open();
                string query0 = "SELECT a.*,b.type as vehtype,c.name as vend,d.fname as drivername,e.name as zonename,f.name as wardname FROM wms.tbl_trip as a join tbl_mstvehicle as b on a.vehicleid=b.id join tbl_vendormst as c on b.vendorid=c.id join tbl_drivermst as d on b.driverid=d.id join tbl_zone as e on b.zoneid=e.id join tbl_ward as f on b.wardid=f.id  where b.number='" + vehno + "';";
                MySqlDataReader row0;
                row0 = con.ExecuteReader(query0);
                if (row0.HasRows)
                {
                    strProvider = "server=" + host + ";Database=" + database + ";User ID=" + userDB + ";Password=" + password;
                    conn = new MySqlConnection(strProvider);
                    conn.Open();
                    string q1 = "sp_vehicletripdetail";
                    MySqlCommand sd = new MySqlCommand(q1, conn);
                    sd.CommandType = CommandType.StoredProcedure;
                    sd.Parameters.Add("@no", MySqlDbType.VarChar, 225).Value = veh;
                    sd.Parameters.Add("@fromdate", MySqlDbType.VarChar, 225).Value = date1;
                    sd.Parameters.Add("@todate", MySqlDbType.VarChar, 225).Value = date2;
                    try
                    {

                        sd.ExecuteNonQuery();
                        this.sp_vehicletripdetailTableAdapter.Fill(this.wmsDataSet29.sp_vehicletripdetail,veh,date1,date2);
                        
                        
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    finally
                    {
                        sd.Connection.Close();

                    }
                }
            }
            else
            {
                MessageBox.Show("Please Enter Details");
            }
        }

       

        private void btnsearch_Click(object sender, EventArgs e)
        {
            //SEARCH BY SLIP NUMBER
            long SLIPNO=int.Parse(textBox3.Text);
            if (textBox3.Text != "")
            {
                con.Close();
                con.Open();
                string query1 = "SELECT a.*,b.type as vehtype,c.name as vend,d.fname as drivername,e.name as zonename,f.name as wardname FROM wms.tbl_trip as a join tbl_mstvehicle as b on a.vehicleid=b.id join tbl_vendormst as c on b.vendorid=c.id join tbl_drivermst as d on b.driverid=d.id join tbl_zone as e on b.zoneid=e.id join tbl_ward as f on b.wardid=f.id  where a.id='" + SLIPNO + "';";
                MySqlDataReader row1;
                row1 = con.ExecuteReader(query1);
                if (row1.HasRows)
                {                   
                    if (row1.Read())
                    {
                        txtslipid.Text = SLIPNO.ToString() ;
                        txttripid.Text = row1["tripid"].ToString();
                        cbvehcltyp.Text = row1["vehtype"].ToString();
                        cbvndrname.Text = row1["vend"].ToString();
                        cbdrivername.Text = row1["drivername"].ToString();
                        cbwsttyp.Text = row1["type"].ToString();
                        cbznname.Text = row1["zonename"].ToString();
                        cbwrdname.Text = row1["wardname"].ToString();
                        txtnetweigh.Text= row1["weight4"].ToString();
                        txtgrossweigh.Text= row1["weight1"].ToString();
                        txtgrossweitim.Text= row1["indatetime"].ToString();
                        txttareweigh.Text= row1["weight3"].ToString();
                        txttareweightime.Text= row1["outdatetime"].ToString();
                        txtgrossuser.Text= row1["grossuser"].ToString();
                        txttareuser.Text= row1["tareuser"].ToString();
                        con.Close();
                    }

                }
            }
            else
            {
                MessageBox.Show("Please Enter SlipNo.");
            }
        }

        private void cbznname_SelectedIndexChanged(object sender, EventArgs e)
        {
                cbwrdname.Items.Clear();
                con.Open();
                string que1 = "SELECT * FROM wms.tbl_ward where zoneid=(select id from tbl_zone where name='" + cbznname.Text + "')";
                MySqlDataReader row6;
                row6 = con.ExecuteReader(que1);
                if (row6.HasRows)
                {
                    while (row6.Read())
                    {

                        cbwrdname.Items.Add(row6["name"].ToString());
                    }
                }
                else
                {
                 cbwrdname.Text = null;
                }            
        }

        private void spvehicletripdetailBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }
    }
}
