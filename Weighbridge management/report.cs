﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weighbridge_management
{
   public class report
    {
        public long slipno { get; set; }
        public long trip{ get; set; }
        public string vehicle { get; set; }
        public string weight1 { get; set; }
        public string weight3 { get; set; }
        public string weight4 { get; set; }
        public string type { get; set; }
        public string ward { get; set; }
        public string time { get; set; }
        public DateTime indatetime { get; set; }
        public DateTime intime { get; set; }
        public DateTime outtime { get; set; }
        public DateTime print { get; set; }
        public string date{ get; set; }
        public string vend{ get; set; }
    }
}
