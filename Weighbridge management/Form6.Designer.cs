﻿
namespace Weighbridge_management
{
    partial class Form6
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabuser = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.spuserlistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wmsDataSet12 = new Weighbridge_management.wmsDataSet12();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txtcnfrmpass = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtpass = new System.Windows.Forms.TextBox();
            this.txtloginid = new System.Windows.Forms.TextBox();
            this.txtmobi = new System.Windows.Forms.TextBox();
            this.txtlname = new System.Windows.Forms.TextBox();
            this.txtfname = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblfname = new System.Windows.Forms.Label();
            this.tabvendr = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.spvendorlistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wmsDataSet13 = new Weighbridge_management.wmsDataSet13();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button9 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.txtpin = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lbladd1 = new System.Windows.Forms.Label();
            this.txtstate = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtcity = new System.Windows.Forms.TextBox();
            this.lbladd2 = new System.Windows.Forms.Label();
            this.txtadd2 = new System.Windows.Forms.TextBox();
            this.lblcity = new System.Windows.Forms.Label();
            this.txtadd1 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtname = new System.Windows.Forms.TextBox();
            this.tabzone = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.spzonelistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wmsDataSet14 = new Weighbridge_management.wmsDataSet14();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button11 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.txtzone = new System.Windows.Forms.TextBox();
            this.button13 = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.tabward = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.spwardlistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wmsDataSet15 = new Weighbridge_management.wmsDataSet15();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.button12 = new System.Windows.Forms.Button();
            this.cbzonename = new System.Windows.Forms.ComboBox();
            this.tblzoneBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wmsDataSet4 = new Weighbridge_management.wmsDataSet4();
            this.button15 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.txtward = new System.Windows.Forms.TextBox();
            this.tabdriver = new System.Windows.Forms.TabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.dataGridView5 = new System.Windows.Forms.DataGridView();
            this.spdriverlistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wmsDataSet16 = new Weighbridge_management.wmsDataSet16();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.button14 = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.button16 = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtdrivelice = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtdrivadd2 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtdriveadd1 = new System.Windows.Forms.TextBox();
            this.txtdrivfname = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtdrivelname = new System.Windows.Forms.TextBox();
            this.txtdrivmo = new System.Windows.Forms.TextBox();
            this.button6 = new System.Windows.Forms.Button();
            this.txtdob = new System.Windows.Forms.TextBox();
            this.txtdrivestate = new System.Windows.Forms.TextBox();
            this.txtdrivcity = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.dataGridView6 = new System.Windows.Forms.DataGridView();
            this.spvehicleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wmsDataSet18 = new Weighbridge_management.wmsDataSet18();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.button17 = new System.Windows.Forms.Button();
            this.cbdrvrname = new System.Windows.Forms.ComboBox();
            this.txttyp = new System.Windows.Forms.TextBox();
            this.button18 = new System.Windows.Forms.Button();
            this.btnnewrecord = new System.Windows.Forms.Button();
            this.lblvehicleno = new System.Windows.Forms.Label();
            this.cbwardname = new System.Windows.Forms.ComboBox();
            this.txtvehicleno = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.lbltype = new System.Windows.Forms.Label();
            this.cbvendername = new System.Windows.Forms.ComboBox();
            this.lblvendorname = new System.Windows.Forms.Label();
            this.lblwardname = new System.Windows.Forms.Label();
            this.lbldrivername = new System.Windows.Forms.Label();
            this.lblzonename = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.tbl_zoneTableAdapter = new Weighbridge_management.wmsDataSet4TableAdapters.tbl_zoneTableAdapter();
            this.wmsDataSet11 = new Weighbridge_management.wmsDataSet11();
            this.sp_userlistTableAdapter = new Weighbridge_management.wmsDataSet12TableAdapters.sp_userlistTableAdapter();
            this.sp_vendorlistTableAdapter = new Weighbridge_management.wmsDataSet13TableAdapters.sp_vendorlistTableAdapter();
            this.sp_zonelistTableAdapter = new Weighbridge_management.wmsDataSet14TableAdapters.sp_zonelistTableAdapter();
            this.sp_wardlistTableAdapter = new Weighbridge_management.wmsDataSet15TableAdapters.sp_wardlistTableAdapter();
            this.sp_driverlistTableAdapter = new Weighbridge_management.wmsDataSet16TableAdapters.sp_driverlistTableAdapter();
            this.wmsDataSet17 = new Weighbridge_management.wmsDataSet17();
            this.spdriverlistBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.sp_driverlistTableAdapter1 = new Weighbridge_management.wmsDataSet17TableAdapters.sp_driverlistTableAdapter();
            this.sp_vehicleTableAdapter = new Weighbridge_management.wmsDataSet18TableAdapters.sp_vehicleTableAdapter();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zoneidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mobileDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dobDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addressline1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addressline2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stateDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cityDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.licenceidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pincodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.address2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.address1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wardidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zoneidDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.driveridDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vendoridDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.useridDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.firstnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mobilenumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.loginIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.passwordDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1.SuspendLayout();
            this.tabuser.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spuserlistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet12)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tabvendr.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spvendorlistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet13)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.tabzone.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spzonelistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet14)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.tabward.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spwardlistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet15)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblzoneBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet4)).BeginInit();
            this.tabdriver.SuspendLayout();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spdriverlistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet16)).BeginInit();
            this.groupBox9.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spvehicleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet18)).BeginInit();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spdriverlistBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabuser);
            this.tabControl1.Controls.Add(this.tabvendr);
            this.tabControl1.Controls.Add(this.tabzone);
            this.tabControl1.Controls.Add(this.tabward);
            this.tabControl1.Controls.Add(this.tabdriver);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1560, 837);
            this.tabControl1.TabIndex = 0;
            // 
            // tabuser
            // 
            this.tabuser.Controls.Add(this.groupBox2);
            this.tabuser.Controls.Add(this.groupBox1);
            this.tabuser.Location = new System.Drawing.Point(4, 34);
            this.tabuser.Name = "tabuser";
            this.tabuser.Padding = new System.Windows.Forms.Padding(3);
            this.tabuser.Size = new System.Drawing.Size(1552, 799);
            this.tabuser.TabIndex = 0;
            this.tabuser.Text = "USER";
            this.tabuser.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridView2);
            this.groupBox2.Location = new System.Drawing.Point(512, 21);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1015, 757);
            this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "User List";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.useridDataGridViewTextBoxColumn,
            this.firstnameDataGridViewTextBoxColumn,
            this.lastnameDataGridViewTextBoxColumn,
            this.mobilenumberDataGridViewTextBoxColumn,
            this.loginIdDataGridViewTextBoxColumn,
            this.passwordDataGridViewTextBoxColumn});
            this.dataGridView2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.dataGridView2.DataSource = this.spuserlistBindingSource;
            this.dataGridView2.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridView2.Location = new System.Drawing.Point(53, 74);
            this.dataGridView2.MultiSelect = false;
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(918, 659);
            this.dataGridView2.StandardTab = true;
            this.dataGridView2.TabIndex = 14;
            this.dataGridView2.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView2_RowHeaderMouseDoubleClick);
            // 
            // spuserlistBindingSource
            // 
            this.spuserlistBindingSource.DataMember = "sp_userlist";
            this.spuserlistBindingSource.DataSource = this.wmsDataSet12;
            // 
            // wmsDataSet12
            // 
            this.wmsDataSet12.DataSetName = "wmsDataSet12";
            this.wmsDataSet12.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button8);
            this.groupBox1.Controls.Add(this.button7);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.txtcnfrmpass);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtpass);
            this.groupBox1.Controls.Add(this.txtloginid);
            this.groupBox1.Controls.Add(this.txtmobi);
            this.groupBox1.Controls.Add(this.txtlname);
            this.groupBox1.Controls.Add(this.txtfname);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.lblfname);
            this.groupBox1.Location = new System.Drawing.Point(23, 21);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(455, 757);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Add User";
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(320, 446);
            this.button8.Name = "button8";
            this.button8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.button8.Size = new System.Drawing.Size(129, 39);
            this.button8.TabIndex = 8;
            this.button8.Text = "DELETE";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(163, 446);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(129, 39);
            this.button7.TabIndex = 7;
            this.button7.Text = "UPDATE";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(6, 446);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(129, 39);
            this.button1.TabIndex = 6;
            this.button1.Text = "ADD";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // txtcnfrmpass
            // 
            this.txtcnfrmpass.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcnfrmpass.Location = new System.Drawing.Point(229, 372);
            this.txtcnfrmpass.Name = "txtcnfrmpass";
            this.txtcnfrmpass.Size = new System.Drawing.Size(206, 31);
            this.txtcnfrmpass.TabIndex = 5;
            this.txtcnfrmpass.UseSystemPasswordChar = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(20, 374);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(186, 25);
            this.label5.TabIndex = 23;
            this.label5.Text = "Confirm Password";
            // 
            // txtpass
            // 
            this.txtpass.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpass.Location = new System.Drawing.Point(229, 316);
            this.txtpass.Name = "txtpass";
            this.txtpass.Size = new System.Drawing.Size(206, 31);
            this.txtpass.TabIndex = 4;
            this.txtpass.UseSystemPasswordChar = true;
            // 
            // txtloginid
            // 
            this.txtloginid.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtloginid.Location = new System.Drawing.Point(229, 260);
            this.txtloginid.Name = "txtloginid";
            this.txtloginid.Size = new System.Drawing.Size(206, 31);
            this.txtloginid.TabIndex = 3;
            // 
            // txtmobi
            // 
            this.txtmobi.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmobi.Location = new System.Drawing.Point(229, 204);
            this.txtmobi.Name = "txtmobi";
            this.txtmobi.Size = new System.Drawing.Size(206, 31);
            this.txtmobi.TabIndex = 2;
            // 
            // txtlname
            // 
            this.txtlname.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtlname.Location = new System.Drawing.Point(229, 148);
            this.txtlname.Name = "txtlname";
            this.txtlname.Size = new System.Drawing.Size(206, 31);
            this.txtlname.TabIndex = 1;
            // 
            // txtfname
            // 
            this.txtfname.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfname.Location = new System.Drawing.Point(229, 92);
            this.txtfname.Name = "txtfname";
            this.txtfname.Size = new System.Drawing.Size(206, 31);
            this.txtfname.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(20, 318);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 25);
            this.label4.TabIndex = 17;
            this.label4.Text = "Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(20, 262);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 25);
            this.label3.TabIndex = 16;
            this.label3.Text = "LoginId";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 206);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 25);
            this.label2.TabIndex = 15;
            this.label2.Text = "Mobile";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(20, 150);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 25);
            this.label1.TabIndex = 14;
            this.label1.Text = "Last Name";
            // 
            // lblfname
            // 
            this.lblfname.AutoSize = true;
            this.lblfname.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfname.Location = new System.Drawing.Point(20, 94);
            this.lblfname.Name = "lblfname";
            this.lblfname.Size = new System.Drawing.Size(116, 25);
            this.lblfname.TabIndex = 13;
            this.lblfname.Text = "First Name";
            // 
            // tabvendr
            // 
            this.tabvendr.Controls.Add(this.groupBox4);
            this.tabvendr.Controls.Add(this.groupBox3);
            this.tabvendr.Location = new System.Drawing.Point(4, 34);
            this.tabvendr.Name = "tabvendr";
            this.tabvendr.Size = new System.Drawing.Size(1552, 799);
            this.tabvendr.TabIndex = 0;
            this.tabvendr.Text = "VENDOR";
            this.tabvendr.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.dataGridView1);
            this.groupBox4.Location = new System.Drawing.Point(498, 15);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1015, 757);
            this.groupBox4.TabIndex = 27;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Vendor List";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.nameDataGridViewTextBoxColumn,
            this.address1DataGridViewTextBoxColumn,
            this.address2DataGridViewTextBoxColumn,
            this.cityDataGridViewTextBoxColumn,
            this.stateDataGridViewTextBoxColumn,
            this.pincodeDataGridViewTextBoxColumn});
            this.dataGridView1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.dataGridView1.DataSource = this.spvendorlistBindingSource;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridView1.Location = new System.Drawing.Point(53, 74);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(918, 659);
            this.dataGridView1.StandardTab = true;
            this.dataGridView1.TabIndex = 14;
            this.dataGridView1.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_RowHeaderMouseDoubleClick);
            // 
            // spvendorlistBindingSource
            // 
            this.spvendorlistBindingSource.DataMember = "sp_vendorlist";
            this.spvendorlistBindingSource.DataSource = this.wmsDataSet13;
            // 
            // wmsDataSet13
            // 
            this.wmsDataSet13.DataSetName = "wmsDataSet13";
            this.wmsDataSet13.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button9);
            this.groupBox3.Controls.Add(this.button2);
            this.groupBox3.Controls.Add(this.button10);
            this.groupBox3.Controls.Add(this.txtpin);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.lbladd1);
            this.groupBox3.Controls.Add(this.txtstate);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.txtcity);
            this.groupBox3.Controls.Add(this.lbladd2);
            this.groupBox3.Controls.Add(this.txtadd2);
            this.groupBox3.Controls.Add(this.lblcity);
            this.groupBox3.Controls.Add(this.txtadd1);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.txtname);
            this.groupBox3.Location = new System.Drawing.Point(16, 15);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(455, 757);
            this.groupBox3.TabIndex = 26;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Add Vendor";
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Location = new System.Drawing.Point(320, 433);
            this.button9.Name = "button9";
            this.button9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.button9.Size = new System.Drawing.Size(129, 39);
            this.button9.TabIndex = 8;
            this.button9.Text = "DELETE";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(6, 433);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(129, 39);
            this.button2.TabIndex = 6;
            this.button2.Text = "ADD";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button10
            // 
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.Location = new System.Drawing.Point(163, 433);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(129, 39);
            this.button10.TabIndex = 7;
            this.button10.Text = "UPDATE";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // txtpin
            // 
            this.txtpin.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpin.Location = new System.Drawing.Point(229, 325);
            this.txtpin.Name = "txtpin";
            this.txtpin.Size = new System.Drawing.Size(206, 31);
            this.txtpin.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(20, 327);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 25);
            this.label6.TabIndex = 23;
            this.label6.Text = "Pincode";
            // 
            // lbladd1
            // 
            this.lbladd1.AutoSize = true;
            this.lbladd1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbladd1.Location = new System.Drawing.Point(20, 103);
            this.lbladd1.Name = "lbladd1";
            this.lbladd1.Size = new System.Drawing.Size(109, 25);
            this.lbladd1.TabIndex = 14;
            this.lbladd1.Text = "Address 1";
            // 
            // txtstate
            // 
            this.txtstate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtstate.Location = new System.Drawing.Point(229, 269);
            this.txtstate.Name = "txtstate";
            this.txtstate.Size = new System.Drawing.Size(206, 31);
            this.txtstate.TabIndex = 4;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(20, 47);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 25);
            this.label11.TabIndex = 13;
            this.label11.Text = "Name";
            // 
            // txtcity
            // 
            this.txtcity.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcity.Location = new System.Drawing.Point(229, 213);
            this.txtcity.Name = "txtcity";
            this.txtcity.Size = new System.Drawing.Size(206, 31);
            this.txtcity.TabIndex = 4;
            // 
            // lbladd2
            // 
            this.lbladd2.AutoSize = true;
            this.lbladd2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbladd2.Location = new System.Drawing.Point(20, 159);
            this.lbladd2.Name = "lbladd2";
            this.lbladd2.Size = new System.Drawing.Size(109, 25);
            this.lbladd2.TabIndex = 15;
            this.lbladd2.Text = "Address 2";
            // 
            // txtadd2
            // 
            this.txtadd2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtadd2.Location = new System.Drawing.Point(229, 157);
            this.txtadd2.Name = "txtadd2";
            this.txtadd2.Size = new System.Drawing.Size(206, 31);
            this.txtadd2.TabIndex = 2;
            // 
            // lblcity
            // 
            this.lblcity.AutoSize = true;
            this.lblcity.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcity.Location = new System.Drawing.Point(20, 215);
            this.lblcity.Name = "lblcity";
            this.lblcity.Size = new System.Drawing.Size(49, 25);
            this.lblcity.TabIndex = 16;
            this.lblcity.Text = "City";
            // 
            // txtadd1
            // 
            this.txtadd1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtadd1.Location = new System.Drawing.Point(229, 101);
            this.txtadd1.Name = "txtadd1";
            this.txtadd1.Size = new System.Drawing.Size(206, 31);
            this.txtadd1.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(20, 271);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 25);
            this.label7.TabIndex = 17;
            this.label7.Text = "State";
            // 
            // txtname
            // 
            this.txtname.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtname.Location = new System.Drawing.Point(229, 45);
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(206, 31);
            this.txtname.TabIndex = 0;
            // 
            // tabzone
            // 
            this.tabzone.Controls.Add(this.groupBox6);
            this.tabzone.Controls.Add(this.groupBox5);
            this.tabzone.Location = new System.Drawing.Point(4, 34);
            this.tabzone.Name = "tabzone";
            this.tabzone.Padding = new System.Windows.Forms.Padding(3);
            this.tabzone.Size = new System.Drawing.Size(1552, 799);
            this.tabzone.TabIndex = 1;
            this.tabzone.Text = "ZONE";
            this.tabzone.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.dataGridView3);
            this.groupBox6.Location = new System.Drawing.Point(512, 20);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(1015, 757);
            this.groupBox6.TabIndex = 28;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Zone List";
            // 
            // dataGridView3
            // 
            this.dataGridView3.AutoGenerateColumns = false;
            this.dataGridView3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView3.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn1});
            this.dataGridView3.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.dataGridView3.DataSource = this.spzonelistBindingSource;
            this.dataGridView3.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridView3.Location = new System.Drawing.Point(53, 74);
            this.dataGridView3.MultiSelect = false;
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView3.Size = new System.Drawing.Size(918, 659);
            this.dataGridView3.StandardTab = true;
            this.dataGridView3.TabIndex = 14;
            this.dataGridView3.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView3_RowHeaderMouseDoubleClick);
            // 
            // spzonelistBindingSource
            // 
            this.spzonelistBindingSource.DataMember = "sp_zonelist";
            this.spzonelistBindingSource.DataSource = this.wmsDataSet14;
            // 
            // wmsDataSet14
            // 
            this.wmsDataSet14.DataSetName = "wmsDataSet14";
            this.wmsDataSet14.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.button11);
            this.groupBox5.Controls.Add(this.button4);
            this.groupBox5.Controls.Add(this.txtzone);
            this.groupBox5.Controls.Add(this.button13);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Location = new System.Drawing.Point(20, 20);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(455, 757);
            this.groupBox5.TabIndex = 27;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Add Zone";
            // 
            // button11
            // 
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.Location = new System.Drawing.Point(335, 139);
            this.button11.Name = "button11";
            this.button11.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.button11.Size = new System.Drawing.Size(114, 39);
            this.button11.TabIndex = 3;
            this.button11.Text = "DELETE";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(0, 139);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(114, 39);
            this.button4.TabIndex = 1;
            this.button4.Text = "ADD";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // txtzone
            // 
            this.txtzone.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtzone.Location = new System.Drawing.Point(228, 57);
            this.txtzone.Name = "txtzone";
            this.txtzone.Size = new System.Drawing.Size(206, 31);
            this.txtzone.TabIndex = 0;
            this.txtzone.TextChanged += new System.EventHandler(this.txtzone_TextChanged);
            // 
            // button13
            // 
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.Location = new System.Drawing.Point(171, 139);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(114, 39);
            this.button13.TabIndex = 2;
            this.button13.Text = "UPDATE";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(19, 59);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(123, 25);
            this.label14.TabIndex = 13;
            this.label14.Text = "Zone Name";
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // tabward
            // 
            this.tabward.Controls.Add(this.groupBox8);
            this.tabward.Controls.Add(this.groupBox7);
            this.tabward.Location = new System.Drawing.Point(4, 34);
            this.tabward.Name = "tabward";
            this.tabward.Padding = new System.Windows.Forms.Padding(3);
            this.tabward.Size = new System.Drawing.Size(1552, 799);
            this.tabward.TabIndex = 2;
            this.tabward.Text = "WARD";
            this.tabward.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.dataGridView4);
            this.groupBox8.Location = new System.Drawing.Point(512, 27);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(1015, 757);
            this.groupBox8.TabIndex = 40;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Ward List";
            // 
            // dataGridView4
            // 
            this.dataGridView4.AutoGenerateColumns = false;
            this.dataGridView4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView4.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn1,
            this.nameDataGridViewTextBoxColumn2,
            this.zoneidDataGridViewTextBoxColumn});
            this.dataGridView4.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.dataGridView4.DataSource = this.spwardlistBindingSource;
            this.dataGridView4.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridView4.Location = new System.Drawing.Point(53, 74);
            this.dataGridView4.MultiSelect = false;
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView4.Size = new System.Drawing.Size(918, 659);
            this.dataGridView4.StandardTab = true;
            this.dataGridView4.TabIndex = 14;
            this.dataGridView4.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView4_RowHeaderMouseDoubleClick);
            // 
            // spwardlistBindingSource
            // 
            this.spwardlistBindingSource.DataMember = "sp_wardlist";
            this.spwardlistBindingSource.DataSource = this.wmsDataSet15;
            // 
            // wmsDataSet15
            // 
            this.wmsDataSet15.DataSetName = "wmsDataSet15";
            this.wmsDataSet15.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.button12);
            this.groupBox7.Controls.Add(this.cbzonename);
            this.groupBox7.Controls.Add(this.button15);
            this.groupBox7.Controls.Add(this.label9);
            this.groupBox7.Controls.Add(this.button5);
            this.groupBox7.Controls.Add(this.label8);
            this.groupBox7.Controls.Add(this.txtward);
            this.groupBox7.Location = new System.Drawing.Point(22, 27);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(455, 757);
            this.groupBox7.TabIndex = 39;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Add Ward";
            // 
            // button12
            // 
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.Location = new System.Drawing.Point(320, 172);
            this.button12.Name = "button12";
            this.button12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.button12.Size = new System.Drawing.Size(129, 39);
            this.button12.TabIndex = 5;
            this.button12.Text = "DELETE";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // cbzonename
            // 
            this.cbzonename.DataSource = this.tblzoneBindingSource;
            this.cbzonename.DisplayMember = "name";
            this.cbzonename.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbzonename.FormattingEnabled = true;
            this.cbzonename.Location = new System.Drawing.Point(229, 101);
            this.cbzonename.Name = "cbzonename";
            this.cbzonename.Size = new System.Drawing.Size(206, 33);
            this.cbzonename.TabIndex = 1;
            this.cbzonename.ValueMember = "id";
            // 
            // tblzoneBindingSource
            // 
            this.tblzoneBindingSource.DataMember = "tbl_zone";
            this.tblzoneBindingSource.DataSource = this.wmsDataSet4;
            // 
            // wmsDataSet4
            // 
            this.wmsDataSet4.DataSetName = "wmsDataSet4";
            this.wmsDataSet4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // button15
            // 
            this.button15.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button15.Location = new System.Drawing.Point(163, 172);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(129, 39);
            this.button15.TabIndex = 4;
            this.button15.Text = "UPDATE";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(20, 109);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(123, 25);
            this.label9.TabIndex = 29;
            this.label9.Text = "Zone Name";
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(6, 172);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(129, 39);
            this.button5.TabIndex = 2;
            this.button5.Text = "SUBMIT";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(20, 33);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(125, 25);
            this.label8.TabIndex = 26;
            this.label8.Text = "Ward Name";
            // 
            // txtward
            // 
            this.txtward.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtward.Location = new System.Drawing.Point(229, 31);
            this.txtward.Name = "txtward";
            this.txtward.Size = new System.Drawing.Size(206, 31);
            this.txtward.TabIndex = 0;
            // 
            // tabdriver
            // 
            this.tabdriver.Controls.Add(this.groupBox10);
            this.tabdriver.Controls.Add(this.groupBox9);
            this.tabdriver.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tabdriver.Location = new System.Drawing.Point(4, 34);
            this.tabdriver.Name = "tabdriver";
            this.tabdriver.Padding = new System.Windows.Forms.Padding(3);
            this.tabdriver.Size = new System.Drawing.Size(1552, 799);
            this.tabdriver.TabIndex = 3;
            this.tabdriver.Text = "DRIVER";
            this.tabdriver.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.dataGridView5);
            this.groupBox10.Location = new System.Drawing.Point(512, 18);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(1015, 757);
            this.groupBox10.TabIndex = 41;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Driver List";
            // 
            // dataGridView5
            // 
            this.dataGridView5.AutoGenerateColumns = false;
            this.dataGridView5.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView5.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column2,
            this.fnameDataGridViewTextBoxColumn,
            this.lnameDataGridViewTextBoxColumn,
            this.mobileDataGridViewTextBoxColumn,
            this.dobDataGridViewTextBoxColumn,
            this.addressline1DataGridViewTextBoxColumn,
            this.addressline2DataGridViewTextBoxColumn,
            this.stateDataGridViewTextBoxColumn1,
            this.cityDataGridViewTextBoxColumn1,
            this.licenceidDataGridViewTextBoxColumn});
            this.dataGridView5.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.dataGridView5.DataSource = this.spdriverlistBindingSource;
            this.dataGridView5.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridView5.Location = new System.Drawing.Point(53, 74);
            this.dataGridView5.MultiSelect = false;
            this.dataGridView5.Name = "dataGridView5";
            this.dataGridView5.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView5.Size = new System.Drawing.Size(918, 659);
            this.dataGridView5.StandardTab = true;
            this.dataGridView5.TabIndex = 14;
            this.dataGridView5.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView5_RowHeaderMouseDoubleClick);
            // 
            // spdriverlistBindingSource
            // 
            this.spdriverlistBindingSource.DataMember = "sp_driverlist";
            this.spdriverlistBindingSource.DataSource = this.wmsDataSet16;
            // 
            // wmsDataSet16
            // 
            this.wmsDataSet16.DataSetName = "wmsDataSet16";
            this.wmsDataSet16.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.button14);
            this.groupBox9.Controls.Add(this.label19);
            this.groupBox9.Controls.Add(this.button16);
            this.groupBox9.Controls.Add(this.label17);
            this.groupBox9.Controls.Add(this.label16);
            this.groupBox9.Controls.Add(this.txtdrivelice);
            this.groupBox9.Controls.Add(this.label15);
            this.groupBox9.Controls.Add(this.label20);
            this.groupBox9.Controls.Add(this.label13);
            this.groupBox9.Controls.Add(this.txtdrivadd2);
            this.groupBox9.Controls.Add(this.label12);
            this.groupBox9.Controls.Add(this.txtdriveadd1);
            this.groupBox9.Controls.Add(this.txtdrivfname);
            this.groupBox9.Controls.Add(this.label18);
            this.groupBox9.Controls.Add(this.txtdrivelname);
            this.groupBox9.Controls.Add(this.txtdrivmo);
            this.groupBox9.Controls.Add(this.button6);
            this.groupBox9.Controls.Add(this.txtdob);
            this.groupBox9.Controls.Add(this.txtdrivestate);
            this.groupBox9.Controls.Add(this.txtdrivcity);
            this.groupBox9.Controls.Add(this.label10);
            this.groupBox9.Location = new System.Drawing.Point(19, 18);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(455, 757);
            this.groupBox9.TabIndex = 40;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Add Driver";
            // 
            // button14
            // 
            this.button14.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.Location = new System.Drawing.Point(314, 521);
            this.button14.Name = "button14";
            this.button14.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.button14.Size = new System.Drawing.Size(135, 39);
            this.button14.TabIndex = 11;
            this.button14.Text = "DELETE";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(15, 243);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(109, 25);
            this.label19.TabIndex = 26;
            this.label19.Text = "Address 1";
            // 
            // button16
            // 
            this.button16.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button16.Location = new System.Drawing.Point(157, 521);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(135, 39);
            this.button16.TabIndex = 10;
            this.button16.Text = "UPDATE";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(15, 39);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(116, 25);
            this.label17.TabIndex = 13;
            this.label17.Text = "First Name";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(15, 90);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(115, 25);
            this.label16.TabIndex = 14;
            this.label16.Text = "Last Name";
            // 
            // txtdrivelice
            // 
            this.txtdrivelice.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdrivelice.Location = new System.Drawing.Point(224, 445);
            this.txtdrivelice.Name = "txtdrivelice";
            this.txtdrivelice.Size = new System.Drawing.Size(206, 31);
            this.txtdrivelice.TabIndex = 8;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(15, 141);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(76, 25);
            this.label15.TabIndex = 15;
            this.label15.Text = "Mobile";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(15, 447);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(87, 25);
            this.label20.TabIndex = 30;
            this.label20.Text = "Licence";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(15, 192);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(57, 25);
            this.label13.TabIndex = 16;
            this.label13.Text = "DOB";
            // 
            // txtdrivadd2
            // 
            this.txtdrivadd2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdrivadd2.Location = new System.Drawing.Point(224, 292);
            this.txtdrivadd2.Name = "txtdrivadd2";
            this.txtdrivadd2.Size = new System.Drawing.Size(206, 31);
            this.txtdrivadd2.TabIndex = 5;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(15, 345);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(49, 25);
            this.label12.TabIndex = 17;
            this.label12.Text = "City";
            // 
            // txtdriveadd1
            // 
            this.txtdriveadd1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdriveadd1.Location = new System.Drawing.Point(224, 241);
            this.txtdriveadd1.Name = "txtdriveadd1";
            this.txtdriveadd1.Size = new System.Drawing.Size(206, 31);
            this.txtdriveadd1.TabIndex = 4;
            // 
            // txtdrivfname
            // 
            this.txtdrivfname.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdrivfname.Location = new System.Drawing.Point(224, 37);
            this.txtdrivfname.Name = "txtdrivfname";
            this.txtdrivfname.Size = new System.Drawing.Size(206, 31);
            this.txtdrivfname.TabIndex = 0;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(15, 294);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(109, 25);
            this.label18.TabIndex = 27;
            this.label18.Text = "Address 2";
            // 
            // txtdrivelname
            // 
            this.txtdrivelname.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdrivelname.Location = new System.Drawing.Point(224, 88);
            this.txtdrivelname.Name = "txtdrivelname";
            this.txtdrivelname.Size = new System.Drawing.Size(206, 31);
            this.txtdrivelname.TabIndex = 1;
            // 
            // txtdrivmo
            // 
            this.txtdrivmo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdrivmo.Location = new System.Drawing.Point(224, 139);
            this.txtdrivmo.Name = "txtdrivmo";
            this.txtdrivmo.Size = new System.Drawing.Size(206, 31);
            this.txtdrivmo.TabIndex = 2;
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(0, 521);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(135, 39);
            this.button6.TabIndex = 9;
            this.button6.Text = "SUBMIT";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // txtdob
            // 
            this.txtdob.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdob.Location = new System.Drawing.Point(224, 190);
            this.txtdob.Name = "txtdob";
            this.txtdob.Size = new System.Drawing.Size(206, 31);
            this.txtdob.TabIndex = 3;
            // 
            // txtdrivestate
            // 
            this.txtdrivestate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdrivestate.Location = new System.Drawing.Point(224, 394);
            this.txtdrivestate.Name = "txtdrivestate";
            this.txtdrivestate.Size = new System.Drawing.Size(206, 31);
            this.txtdrivestate.TabIndex = 7;
            // 
            // txtdrivcity
            // 
            this.txtdrivcity.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdrivcity.Location = new System.Drawing.Point(224, 343);
            this.txtdrivcity.Name = "txtdrivcity";
            this.txtdrivcity.Size = new System.Drawing.Size(206, 31);
            this.txtdrivcity.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(15, 396);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 25);
            this.label10.TabIndex = 23;
            this.label10.Text = "State";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox12);
            this.tabPage1.Controls.Add(this.groupBox11);
            this.tabPage1.Location = new System.Drawing.Point(4, 34);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1552, 799);
            this.tabPage1.TabIndex = 4;
            this.tabPage1.Text = "VEHICLE";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.dataGridView6);
            this.groupBox12.Location = new System.Drawing.Point(803, 23);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(1015, 757);
            this.groupBox12.TabIndex = 86;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Vehicle List";
            // 
            // dataGridView6
            // 
            this.dataGridView6.AutoGenerateColumns = false;
            this.dataGridView6.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView6.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView6.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column3,
            this.numberDataGridViewTextBoxColumn,
            this.typeDataGridViewTextBoxColumn,
            this.vendoridDataGridViewTextBoxColumn,
            this.driveridDataGridViewTextBoxColumn,
            this.zoneidDataGridViewTextBoxColumn1,
            this.wardidDataGridViewTextBoxColumn});
            this.dataGridView6.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.dataGridView6.DataSource = this.spvehicleBindingSource;
            this.dataGridView6.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridView6.Location = new System.Drawing.Point(30, 74);
            this.dataGridView6.MultiSelect = false;
            this.dataGridView6.Name = "dataGridView6";
            this.dataGridView6.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView6.Size = new System.Drawing.Size(694, 659);
            this.dataGridView6.StandardTab = true;
            this.dataGridView6.TabIndex = 14;
            // 
            // spvehicleBindingSource
            // 
            this.spvehicleBindingSource.DataMember = "sp_vehicle";
            this.spvehicleBindingSource.DataSource = this.wmsDataSet18;
            // 
            // wmsDataSet18
            // 
            this.wmsDataSet18.DataSetName = "wmsDataSet18";
            this.wmsDataSet18.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.button17);
            this.groupBox11.Controls.Add(this.cbdrvrname);
            this.groupBox11.Controls.Add(this.txttyp);
            this.groupBox11.Controls.Add(this.button18);
            this.groupBox11.Controls.Add(this.btnnewrecord);
            this.groupBox11.Controls.Add(this.lblvehicleno);
            this.groupBox11.Controls.Add(this.cbwardname);
            this.groupBox11.Controls.Add(this.txtvehicleno);
            this.groupBox11.Controls.Add(this.comboBox1);
            this.groupBox11.Controls.Add(this.lbltype);
            this.groupBox11.Controls.Add(this.cbvendername);
            this.groupBox11.Controls.Add(this.lblvendorname);
            this.groupBox11.Controls.Add(this.lblwardname);
            this.groupBox11.Controls.Add(this.lbldrivername);
            this.groupBox11.Controls.Add(this.lblzonename);
            this.groupBox11.Location = new System.Drawing.Point(25, 23);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(742, 757);
            this.groupBox11.TabIndex = 85;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Add Vehicle";
            // 
            // button17
            // 
            this.button17.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button17.Location = new System.Drawing.Point(503, 438);
            this.button17.Name = "button17";
            this.button17.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.button17.Size = new System.Drawing.Size(151, 39);
            this.button17.TabIndex = 8;
            this.button17.Text = "DELETE";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // cbdrvrname
            // 
            this.cbdrvrname.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.RecentlyUsedList;
            this.cbdrvrname.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbdrvrname.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbdrvrname.FormattingEnabled = true;
            this.cbdrvrname.Location = new System.Drawing.Point(193, 213);
            this.cbdrvrname.Name = "cbdrvrname";
            this.cbdrvrname.Size = new System.Drawing.Size(517, 33);
            this.cbdrvrname.TabIndex = 3;
            // 
            // txttyp
            // 
            this.txttyp.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttyp.Location = new System.Drawing.Point(428, 65);
            this.txttyp.MaxLength = 100;
            this.txttyp.Name = "txttyp";
            this.txttyp.Size = new System.Drawing.Size(280, 31);
            this.txttyp.TabIndex = 1;
            // 
            // button18
            // 
            this.button18.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button18.Location = new System.Drawing.Point(268, 439);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(151, 39);
            this.button18.TabIndex = 7;
            this.button18.Text = "UPDATE";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // btnnewrecord
            // 
            this.btnnewrecord.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnnewrecord.Location = new System.Drawing.Point(34, 439);
            this.btnnewrecord.Name = "btnnewrecord";
            this.btnnewrecord.Size = new System.Drawing.Size(150, 36);
            this.btnnewrecord.TabIndex = 6;
            this.btnnewrecord.Text = "ADD";
            this.btnnewrecord.UseVisualStyleBackColor = true;
            this.btnnewrecord.Click += new System.EventHandler(this.btnnewrecord_Click);
            // 
            // lblvehicleno
            // 
            this.lblvehicleno.AutoSize = true;
            this.lblvehicleno.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblvehicleno.Location = new System.Drawing.Point(8, 68);
            this.lblvehicleno.Name = "lblvehicleno";
            this.lblvehicleno.Size = new System.Drawing.Size(119, 25);
            this.lblvehicleno.TabIndex = 66;
            this.lblvehicleno.Text = "Vehicle no.";
            this.lblvehicleno.UseWaitCursor = true;
            // 
            // cbwardname
            // 
            this.cbwardname.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbwardname.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbwardname.FormattingEnabled = true;
            this.cbwardname.Location = new System.Drawing.Point(192, 363);
            this.cbwardname.Name = "cbwardname";
            this.cbwardname.Size = new System.Drawing.Size(517, 33);
            this.cbwardname.TabIndex = 5;
            // 
            // txtvehicleno
            // 
            this.txtvehicleno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtvehicleno.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvehicleno.Location = new System.Drawing.Point(192, 65);
            this.txtvehicleno.MaxLength = 10;
            this.txtvehicleno.Name = "txtvehicleno";
            this.txtvehicleno.Size = new System.Drawing.Size(162, 31);
            this.txtvehicleno.TabIndex = 0;
            this.txtvehicleno.TextChanged += new System.EventHandler(this.txtvehicleno_TextChanged);
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(193, 288);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(517, 33);
            this.comboBox1.TabIndex = 4;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // lbltype
            // 
            this.lbltype.AutoSize = true;
            this.lbltype.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltype.Location = new System.Drawing.Point(370, 68);
            this.lbltype.Name = "lbltype";
            this.lbltype.Size = new System.Drawing.Size(60, 25);
            this.lbltype.TabIndex = 74;
            this.lbltype.Text = "Type";
            this.lbltype.UseWaitCursor = true;
            // 
            // cbvendername
            // 
            this.cbvendername.Cursor = System.Windows.Forms.Cursors.VSplit;
            this.cbvendername.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbvendername.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbvendername.FormattingEnabled = true;
            this.cbvendername.Location = new System.Drawing.Point(191, 138);
            this.cbvendername.Name = "cbvendername";
            this.cbvendername.Size = new System.Drawing.Size(517, 33);
            this.cbvendername.TabIndex = 2;
            // 
            // lblvendorname
            // 
            this.lblvendorname.AutoSize = true;
            this.lblvendorname.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblvendorname.Location = new System.Drawing.Point(8, 141);
            this.lblvendorname.Name = "lblvendorname";
            this.lblvendorname.Size = new System.Drawing.Size(140, 25);
            this.lblvendorname.TabIndex = 77;
            this.lblvendorname.Text = "Vendor name";
            this.lblvendorname.UseWaitCursor = true;
            // 
            // lblwardname
            // 
            this.lblwardname.AutoSize = true;
            this.lblwardname.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblwardname.Location = new System.Drawing.Point(10, 360);
            this.lblwardname.Name = "lblwardname";
            this.lblwardname.Size = new System.Drawing.Size(125, 25);
            this.lblwardname.TabIndex = 83;
            this.lblwardname.Text = "Ward Name";
            this.lblwardname.UseWaitCursor = true;
            // 
            // lbldrivername
            // 
            this.lbldrivername.AutoSize = true;
            this.lbldrivername.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldrivername.Location = new System.Drawing.Point(8, 214);
            this.lbldrivername.Name = "lbldrivername";
            this.lbldrivername.Size = new System.Drawing.Size(128, 25);
            this.lbldrivername.TabIndex = 78;
            this.lbldrivername.Text = "Driver name";
            this.lbldrivername.UseWaitCursor = true;
            // 
            // lblzonename
            // 
            this.lblzonename.AutoSize = true;
            this.lblzonename.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblzonename.Location = new System.Drawing.Point(11, 287);
            this.lblzonename.Name = "lblzonename";
            this.lblzonename.Size = new System.Drawing.Size(123, 25);
            this.lblzonename.TabIndex = 82;
            this.lblzonename.Text = "Zone Name";
            this.lblzonename.UseWaitCursor = true;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(1365, 7);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(178, 32);
            this.button3.TabIndex = 1;
            this.button3.Text = "MAIN SCREEN";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // tbl_zoneTableAdapter
            // 
            this.tbl_zoneTableAdapter.ClearBeforeFill = true;
            // 
            // wmsDataSet11
            // 
            this.wmsDataSet11.DataSetName = "wmsDataSet11";
            this.wmsDataSet11.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp_userlistTableAdapter
            // 
            this.sp_userlistTableAdapter.ClearBeforeFill = true;
            // 
            // sp_vendorlistTableAdapter
            // 
            this.sp_vendorlistTableAdapter.ClearBeforeFill = true;
            // 
            // sp_zonelistTableAdapter
            // 
            this.sp_zonelistTableAdapter.ClearBeforeFill = true;
            // 
            // sp_wardlistTableAdapter
            // 
            this.sp_wardlistTableAdapter.ClearBeforeFill = true;
            // 
            // sp_driverlistTableAdapter
            // 
            this.sp_driverlistTableAdapter.ClearBeforeFill = true;
            // 
            // wmsDataSet17
            // 
            this.wmsDataSet17.DataSetName = "wmsDataSet17";
            this.wmsDataSet17.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // spdriverlistBindingSource1
            // 
            this.spdriverlistBindingSource1.DataMember = "sp_driverlist";
            this.spdriverlistBindingSource1.DataSource = this.wmsDataSet17;
            // 
            // sp_driverlistTableAdapter1
            // 
            this.sp_driverlistTableAdapter1.ClearBeforeFill = true;
            // 
            // sp_vehicleTableAdapter
            // 
            this.sp_vehicleTableAdapter.ClearBeforeFill = true;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            // 
            // nameDataGridViewTextBoxColumn1
            // 
            this.nameDataGridViewTextBoxColumn1.DataPropertyName = "name";
            this.nameDataGridViewTextBoxColumn1.HeaderText = "Zone Name";
            this.nameDataGridViewTextBoxColumn1.Name = "nameDataGridViewTextBoxColumn1";
            // 
            // idDataGridViewTextBoxColumn1
            // 
            this.idDataGridViewTextBoxColumn1.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn1.HeaderText = "id";
            this.idDataGridViewTextBoxColumn1.Name = "idDataGridViewTextBoxColumn1";
            // 
            // nameDataGridViewTextBoxColumn2
            // 
            this.nameDataGridViewTextBoxColumn2.DataPropertyName = "name";
            this.nameDataGridViewTextBoxColumn2.HeaderText = "Ward Name";
            this.nameDataGridViewTextBoxColumn2.Name = "nameDataGridViewTextBoxColumn2";
            // 
            // zoneidDataGridViewTextBoxColumn
            // 
            this.zoneidDataGridViewTextBoxColumn.DataPropertyName = "zoneid";
            this.zoneidDataGridViewTextBoxColumn.HeaderText = "zoneid";
            this.zoneidDataGridViewTextBoxColumn.Name = "zoneidDataGridViewTextBoxColumn";
            this.zoneidDataGridViewTextBoxColumn.Visible = false;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "id";
            this.Column2.HeaderText = "id";
            this.Column2.Name = "Column2";
            // 
            // fnameDataGridViewTextBoxColumn
            // 
            this.fnameDataGridViewTextBoxColumn.DataPropertyName = "fname";
            this.fnameDataGridViewTextBoxColumn.HeaderText = "First Name";
            this.fnameDataGridViewTextBoxColumn.Name = "fnameDataGridViewTextBoxColumn";
            // 
            // lnameDataGridViewTextBoxColumn
            // 
            this.lnameDataGridViewTextBoxColumn.DataPropertyName = "lname";
            this.lnameDataGridViewTextBoxColumn.HeaderText = "Last Name";
            this.lnameDataGridViewTextBoxColumn.Name = "lnameDataGridViewTextBoxColumn";
            // 
            // mobileDataGridViewTextBoxColumn
            // 
            this.mobileDataGridViewTextBoxColumn.DataPropertyName = "mobile";
            this.mobileDataGridViewTextBoxColumn.HeaderText = "Mobile";
            this.mobileDataGridViewTextBoxColumn.Name = "mobileDataGridViewTextBoxColumn";
            // 
            // dobDataGridViewTextBoxColumn
            // 
            this.dobDataGridViewTextBoxColumn.DataPropertyName = "dob";
            this.dobDataGridViewTextBoxColumn.HeaderText = "DOB";
            this.dobDataGridViewTextBoxColumn.Name = "dobDataGridViewTextBoxColumn";
            this.dobDataGridViewTextBoxColumn.Visible = false;
            // 
            // addressline1DataGridViewTextBoxColumn
            // 
            this.addressline1DataGridViewTextBoxColumn.DataPropertyName = "addressline1";
            this.addressline1DataGridViewTextBoxColumn.HeaderText = "Address 1";
            this.addressline1DataGridViewTextBoxColumn.Name = "addressline1DataGridViewTextBoxColumn";
            // 
            // addressline2DataGridViewTextBoxColumn
            // 
            this.addressline2DataGridViewTextBoxColumn.DataPropertyName = "addressline2";
            this.addressline2DataGridViewTextBoxColumn.HeaderText = "Address 2";
            this.addressline2DataGridViewTextBoxColumn.Name = "addressline2DataGridViewTextBoxColumn";
            // 
            // stateDataGridViewTextBoxColumn1
            // 
            this.stateDataGridViewTextBoxColumn1.DataPropertyName = "state";
            this.stateDataGridViewTextBoxColumn1.HeaderText = "State";
            this.stateDataGridViewTextBoxColumn1.Name = "stateDataGridViewTextBoxColumn1";
            this.stateDataGridViewTextBoxColumn1.Visible = false;
            // 
            // cityDataGridViewTextBoxColumn1
            // 
            this.cityDataGridViewTextBoxColumn1.DataPropertyName = "city";
            this.cityDataGridViewTextBoxColumn1.HeaderText = "City";
            this.cityDataGridViewTextBoxColumn1.Name = "cityDataGridViewTextBoxColumn1";
            this.cityDataGridViewTextBoxColumn1.Visible = false;
            // 
            // licenceidDataGridViewTextBoxColumn
            // 
            this.licenceidDataGridViewTextBoxColumn.DataPropertyName = "licenceid";
            this.licenceidDataGridViewTextBoxColumn.HeaderText = "Licence";
            this.licenceidDataGridViewTextBoxColumn.Name = "licenceidDataGridViewTextBoxColumn";
            // 
            // pincodeDataGridViewTextBoxColumn
            // 
            this.pincodeDataGridViewTextBoxColumn.DataPropertyName = "pincode";
            this.pincodeDataGridViewTextBoxColumn.FillWeight = 101.8613F;
            this.pincodeDataGridViewTextBoxColumn.HeaderText = "Pincode";
            this.pincodeDataGridViewTextBoxColumn.Name = "pincodeDataGridViewTextBoxColumn";
            // 
            // stateDataGridViewTextBoxColumn
            // 
            this.stateDataGridViewTextBoxColumn.DataPropertyName = "state";
            this.stateDataGridViewTextBoxColumn.FillWeight = 101.8613F;
            this.stateDataGridViewTextBoxColumn.HeaderText = "State";
            this.stateDataGridViewTextBoxColumn.Name = "stateDataGridViewTextBoxColumn";
            // 
            // cityDataGridViewTextBoxColumn
            // 
            this.cityDataGridViewTextBoxColumn.DataPropertyName = "city";
            this.cityDataGridViewTextBoxColumn.FillWeight = 101.8613F;
            this.cityDataGridViewTextBoxColumn.HeaderText = "City";
            this.cityDataGridViewTextBoxColumn.Name = "cityDataGridViewTextBoxColumn";
            // 
            // address2DataGridViewTextBoxColumn
            // 
            this.address2DataGridViewTextBoxColumn.DataPropertyName = "address2";
            this.address2DataGridViewTextBoxColumn.FillWeight = 101.8613F;
            this.address2DataGridViewTextBoxColumn.HeaderText = "Address2";
            this.address2DataGridViewTextBoxColumn.Name = "address2DataGridViewTextBoxColumn";
            // 
            // address1DataGridViewTextBoxColumn
            // 
            this.address1DataGridViewTextBoxColumn.DataPropertyName = "address1";
            this.address1DataGridViewTextBoxColumn.FillWeight = 101.8613F;
            this.address1DataGridViewTextBoxColumn.HeaderText = "Address1";
            this.address1DataGridViewTextBoxColumn.Name = "address1DataGridViewTextBoxColumn";
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "name";
            this.nameDataGridViewTextBoxColumn.FillWeight = 101.8613F;
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "id";
            this.Column1.FillWeight = 88.8325F;
            this.Column1.HeaderText = "id";
            this.Column1.Name = "Column1";
            // 
            // wardidDataGridViewTextBoxColumn
            // 
            this.wardidDataGridViewTextBoxColumn.DataPropertyName = "wardid";
            this.wardidDataGridViewTextBoxColumn.HeaderText = "wardid";
            this.wardidDataGridViewTextBoxColumn.Name = "wardidDataGridViewTextBoxColumn";
            this.wardidDataGridViewTextBoxColumn.Visible = false;
            // 
            // zoneidDataGridViewTextBoxColumn1
            // 
            this.zoneidDataGridViewTextBoxColumn1.DataPropertyName = "zoneid";
            this.zoneidDataGridViewTextBoxColumn1.HeaderText = "zoneid";
            this.zoneidDataGridViewTextBoxColumn1.Name = "zoneidDataGridViewTextBoxColumn1";
            this.zoneidDataGridViewTextBoxColumn1.Visible = false;
            // 
            // driveridDataGridViewTextBoxColumn
            // 
            this.driveridDataGridViewTextBoxColumn.DataPropertyName = "driverid";
            this.driveridDataGridViewTextBoxColumn.HeaderText = "driverid";
            this.driveridDataGridViewTextBoxColumn.Name = "driveridDataGridViewTextBoxColumn";
            this.driveridDataGridViewTextBoxColumn.Visible = false;
            // 
            // vendoridDataGridViewTextBoxColumn
            // 
            this.vendoridDataGridViewTextBoxColumn.DataPropertyName = "vendorid";
            this.vendoridDataGridViewTextBoxColumn.HeaderText = "vendorid";
            this.vendoridDataGridViewTextBoxColumn.Name = "vendoridDataGridViewTextBoxColumn";
            this.vendoridDataGridViewTextBoxColumn.Visible = false;
            // 
            // typeDataGridViewTextBoxColumn
            // 
            this.typeDataGridViewTextBoxColumn.DataPropertyName = "type";
            this.typeDataGridViewTextBoxColumn.HeaderText = "Vehicle Type";
            this.typeDataGridViewTextBoxColumn.Name = "typeDataGridViewTextBoxColumn";
            // 
            // numberDataGridViewTextBoxColumn
            // 
            this.numberDataGridViewTextBoxColumn.DataPropertyName = "number";
            this.numberDataGridViewTextBoxColumn.HeaderText = "Vehicle Number";
            this.numberDataGridViewTextBoxColumn.Name = "numberDataGridViewTextBoxColumn";
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "id";
            this.Column3.HeaderText = "id";
            this.Column3.Name = "Column3";
            // 
            // useridDataGridViewTextBoxColumn
            // 
            this.useridDataGridViewTextBoxColumn.DataPropertyName = "userid";
            this.useridDataGridViewTextBoxColumn.HeaderText = "userid";
            this.useridDataGridViewTextBoxColumn.Name = "useridDataGridViewTextBoxColumn";
            this.useridDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // firstnameDataGridViewTextBoxColumn
            // 
            this.firstnameDataGridViewTextBoxColumn.DataPropertyName = "firstname";
            this.firstnameDataGridViewTextBoxColumn.HeaderText = "First Name";
            this.firstnameDataGridViewTextBoxColumn.Name = "firstnameDataGridViewTextBoxColumn";
            this.firstnameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lastnameDataGridViewTextBoxColumn
            // 
            this.lastnameDataGridViewTextBoxColumn.DataPropertyName = "lastname";
            this.lastnameDataGridViewTextBoxColumn.HeaderText = "Last Name";
            this.lastnameDataGridViewTextBoxColumn.Name = "lastnameDataGridViewTextBoxColumn";
            this.lastnameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // mobilenumberDataGridViewTextBoxColumn
            // 
            this.mobilenumberDataGridViewTextBoxColumn.DataPropertyName = "mobilenumber";
            this.mobilenumberDataGridViewTextBoxColumn.HeaderText = "Mobile";
            this.mobilenumberDataGridViewTextBoxColumn.Name = "mobilenumberDataGridViewTextBoxColumn";
            this.mobilenumberDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // loginIdDataGridViewTextBoxColumn
            // 
            this.loginIdDataGridViewTextBoxColumn.DataPropertyName = "loginId";
            this.loginIdDataGridViewTextBoxColumn.HeaderText = "Login ID";
            this.loginIdDataGridViewTextBoxColumn.Name = "loginIdDataGridViewTextBoxColumn";
            this.loginIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // passwordDataGridViewTextBoxColumn
            // 
            this.passwordDataGridViewTextBoxColumn.DataPropertyName = "password";
            this.passwordDataGridViewTextBoxColumn.HeaderText = "Password";
            this.passwordDataGridViewTextBoxColumn.Name = "passwordDataGridViewTextBoxColumn";
            this.passwordDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // Form6
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1584, 861);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form6";
            this.Text = "Form6";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form6_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabuser.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spuserlistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet12)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabvendr.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spvendorlistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet13)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabzone.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spzonelistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet14)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tabward.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spwardlistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet15)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblzoneBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet4)).EndInit();
            this.tabdriver.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spdriverlistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet16)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spvehicleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet18)).EndInit();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spdriverlistBindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabuser;
        private System.Windows.Forms.TabPage tabzone;
        private System.Windows.Forms.TabPage tabward;
        private System.Windows.Forms.TabPage tabvendr;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox txtzone;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox txtward;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbzonename;
        private wmsDataSet4 wmsDataSet4;
        private System.Windows.Forms.BindingSource tblzoneBindingSource;
        private wmsDataSet4TableAdapters.tbl_zoneTableAdapter tbl_zoneTableAdapter;
        private System.Windows.Forms.TabPage tabdriver;
        private System.Windows.Forms.TextBox txtdrivelice;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtdrivadd2;
        private System.Windows.Forms.TextBox txtdriveadd1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox txtdrivestate;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtdrivcity;
        private System.Windows.Forms.TextBox txtdob;
        private System.Windows.Forms.TextBox txtdrivmo;
        private System.Windows.Forms.TextBox txtdrivelname;
        private System.Windows.Forms.TextBox txtdrivfname;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox txttyp;
        private System.Windows.Forms.Button btnnewrecord;
        private System.Windows.Forms.ComboBox cbwardname;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox cbvendername;
        private System.Windows.Forms.Label lblwardname;
        private System.Windows.Forms.Label lblzonename;
        private System.Windows.Forms.Label lbldrivername;
        private System.Windows.Forms.Label lblvendorname;
        private System.Windows.Forms.Label lbltype;
        private System.Windows.Forms.TextBox txtvehicleno;
        private System.Windows.Forms.Label lblvehicleno;
        private System.Windows.Forms.ComboBox cbdrvrname;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtcnfrmpass;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtpass;
        private System.Windows.Forms.TextBox txtloginid;
        private System.Windows.Forms.TextBox txtmobi;
        private System.Windows.Forms.TextBox txtlname;
        private System.Windows.Forms.TextBox txtfname;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblfname;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridView2;
        private wmsDataSet11 wmsDataSet11;
        private wmsDataSet12 wmsDataSet12;
        private System.Windows.Forms.BindingSource spuserlistBindingSource;
        private wmsDataSet12TableAdapters.sp_userlistTableAdapter sp_userlistTableAdapter;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView dataGridView1;
        private wmsDataSet13 wmsDataSet13;
        private System.Windows.Forms.BindingSource spvendorlistBindingSource;
        private wmsDataSet13TableAdapters.sp_vendorlistTableAdapter sp_vendorlistTableAdapter;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.TextBox txtpin;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbladd1;
        private System.Windows.Forms.TextBox txtstate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtcity;
        private System.Windows.Forms.Label lbladd2;
        private System.Windows.Forms.TextBox txtadd2;
        private System.Windows.Forms.Label lblcity;
        private System.Windows.Forms.TextBox txtadd1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.DataGridView dataGridView3;
        private wmsDataSet14 wmsDataSet14;
        private System.Windows.Forms.BindingSource spzonelistBindingSource;
        private wmsDataSet14TableAdapters.sp_zonelistTableAdapter sp_zonelistTableAdapter;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.DataGridView dataGridView4;
        private wmsDataSet15 wmsDataSet15;
        private System.Windows.Forms.BindingSource spwardlistBindingSource;
        private wmsDataSet15TableAdapters.sp_wardlistTableAdapter sp_wardlistTableAdapter;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.DataGridView dataGridView5;
        private wmsDataSet16 wmsDataSet16;
        private System.Windows.Forms.BindingSource spdriverlistBindingSource;
        private wmsDataSet16TableAdapters.sp_driverlistTableAdapter sp_driverlistTableAdapter;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.DataGridView dataGridView6;
        private wmsDataSet17 wmsDataSet17;
        private System.Windows.Forms.BindingSource spdriverlistBindingSource1;
        private wmsDataSet17TableAdapters.sp_driverlistTableAdapter sp_driverlistTableAdapter1;
        private wmsDataSet18 wmsDataSet18;
        private System.Windows.Forms.BindingSource spvehicleBindingSource;
        private wmsDataSet18TableAdapters.sp_vehicleTableAdapter sp_vehicleTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn zoneidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn fnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mobileDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dobDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn addressline1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn addressline2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn stateDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cityDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn licenceidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn address1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn address2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn stateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pincodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn numberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vendoridDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn driveridDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn zoneidDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn wardidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn useridDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn firstnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mobilenumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn loginIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn passwordDataGridViewTextBoxColumn;
    }
}