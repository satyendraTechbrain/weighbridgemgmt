﻿
namespace Weighbridge_management
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.lblusername = new System.Windows.Forms.Label();
            this.spgetpendingtripdetailBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.wmsDataSet8 = new Weighbridge_management.wmsDataSet8();
            this.spgetpendingtripdetailBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.wmsDataSet3 = new Weighbridge_management.wmsDataSet3();
            this.spvehicletripdetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wmsDataSet29 = new Weighbridge_management.wmsDataSet29();
            this.btnlogout = new System.Windows.Forms.Button();
            this.btnchangepassword = new System.Windows.Forms.Button();
            this.btnverifyfinger = new System.Windows.Forms.Button();
            this.lblcurrentuser = new System.Windows.Forms.Label();
            this.daysummaryBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.wmsDataSet10 = new Weighbridge_management.wmsDataSet10();
            this.spgetpendingtripdetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wmsDataSet7 = new Weighbridge_management.wmsDataSet7();
            this.tblzoneBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wmsDataSet5 = new Weighbridge_management.wmsDataSet5();
            this.wmsDataSet2 = new Weighbridge_management.wmsDataSet2();
            this.spgettripdetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wmsDataSet = new Weighbridge_management.wmsDataSet();
            this.sp_gettripdetailTableAdapter = new Weighbridge_management.wmsDataSetTableAdapters.sp_gettripdetailTableAdapter();
            this.wmsDataSet1 = new Weighbridge_management.wmsDataSet1();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.sp_getpendingtripdetailTableAdapter1 = new Weighbridge_management.wmsDataSet3TableAdapters.sp_getpendingtripdetailTableAdapter();
            this.btnconfig = new System.Windows.Forms.Button();
            this.tbl_zoneTableAdapter = new Weighbridge_management.wmsDataSet5TableAdapters.tbl_zoneTableAdapter();
            this.wmsDataSet6 = new Weighbridge_management.wmsDataSet6();
            this.sp_getpendingtripdetailTableAdapter = new Weighbridge_management.wmsDataSet2TableAdapters.sp_getpendingtripdetailTableAdapter();
            this.sp_getpendingtripdetailTableAdapter2 = new Weighbridge_management.wmsDataSet7TableAdapters.sp_getpendingtripdetailTableAdapter();
            this.sp_getpendingtripdetailTableAdapter3 = new Weighbridge_management.wmsDataSet8TableAdapters.sp_getpendingtripdetailTableAdapter();
            this.wmsDataSet9 = new Weighbridge_management.wmsDataSet9();
            this.daysummaryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.daysummaryTableAdapter = new Weighbridge_management.wmsDataSet9TableAdapters.daysummaryTableAdapter();
            this.daysummaryTableAdapter1 = new Weighbridge_management.wmsDataSet10TableAdapters.daysummaryTableAdapter();
            this.wmsDataSet19 = new Weighbridge_management.wmsDataSet19();
            this.wmsDataSet20 = new Weighbridge_management.wmsDataSet20();
            this.wmsDataSet21 = new Weighbridge_management.wmsDataSet21();
            this.wmsDataSet22 = new Weighbridge_management.wmsDataSet22();
            this.wmsDataSet23 = new Weighbridge_management.wmsDataSet23();
            this.wmsDataSet24 = new Weighbridge_management.wmsDataSet24();
            this.wmsDataSet25 = new Weighbridge_management.wmsDataSet25();
            this.wmsDataSet26 = new Weighbridge_management.wmsDataSet26();
            this.wmsDataSet27 = new Weighbridge_management.wmsDataSet27();
            this.wmsDataSet28 = new Weighbridge_management.wmsDataSet28();
            this.sp_vehicletripdetailTableAdapter = new Weighbridge_management.wmsDataSet29TableAdapters.sp_vehicletripdetailTableAdapter();
            this.tb7ReportsMore = new System.Windows.Forms.TabPage();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.label9 = new System.Windows.Forms.Label();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.btnviewreprt = new System.Windows.Forms.Button();
            this.cbrepttyp = new System.Windows.Forms.ComboBox();
            this.lblreprttyp = new System.Windows.Forms.Label();
            this.tb4Weighmenthistory = new System.Windows.Forms.TabPage();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.gbweighdet = new System.Windows.Forms.GroupBox();
            this.cbvehcltyp = new System.Windows.Forms.ComboBox();
            this.btnupdttrp = new System.Windows.Forms.Button();
            this.btnignrtrp = new System.Windows.Forms.Button();
            this.btnprntslp = new System.Windows.Forms.Button();
            this.txtnetweigh = new System.Windows.Forms.TextBox();
            this.cbwrdname = new System.Windows.Forms.ComboBox();
            this.cbznname = new System.Windows.Forms.ComboBox();
            this.cbwsttyp = new System.Windows.Forms.ComboBox();
            this.lblvehcltyp = new System.Windows.Forms.Label();
            this.cbdrivername = new System.Windows.Forms.ComboBox();
            this.cbvndrname = new System.Windows.Forms.ComboBox();
            this.txttripid = new System.Windows.Forms.TextBox();
            this.txtslipid = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblwsttyp = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lbltrpid = new System.Windows.Forms.Label();
            this.lblslpid = new System.Windows.Forms.Label();
            this.gbtareweight = new System.Windows.Forms.GroupBox();
            this.txttareuser = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txttareweightime = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txttareweigh = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.gbgrossweight = new System.Windows.Forms.GroupBox();
            this.txtgrossuser = new System.Windows.Forms.TextBox();
            this.txtgrossweitim = new System.Windows.Forms.TextBox();
            this.txtgrossweigh = new System.Windows.Forms.TextBox();
            this.lbluser = new System.Windows.Forms.Label();
            this.lbltim = new System.Windows.Forms.Label();
            this.lblweigh = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.bgserchresult = new System.Windows.Forms.GroupBox();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vehnumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tripidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vehtypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zonenameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wardnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.indatetimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.weight1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbslipsearch = new System.Windows.Forms.GroupBox();
            this.btnsearch = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.gbdaterange = new System.Windows.Forms.GroupBox();
            this.lblto = new System.Windows.Forms.Label();
            this.lblfrm = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.lblvehclnu = new System.Windows.Forms.Label();
            this.btngetrec = new System.Windows.Forms.Button();
            this.txtvehclnu = new System.Windows.Forms.TextBox();
            this.tb2PendingTrips = new System.Windows.Forms.TabPage();
            this.gbtarependlist = new System.Windows.Forms.GroupBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.btnignoretrip = new System.Windows.Forms.Button();
            this.btncompltrip = new System.Windows.Forms.Button();
            this.tb1Vehicleweighment = new System.Windows.Forms.TabPage();
            this.gbrecorddetail = new System.Windows.Forms.GroupBox();
            this.cbdrvrname = new System.Windows.Forms.ComboBox();
            this.txttyp = new System.Windows.Forms.TextBox();
            this.btnnewrecord = new System.Windows.Forms.Button();
            this.btnsaveprint = new System.Windows.Forms.Button();
            this.lblnetweight = new System.Windows.Forms.Label();
            this.txtnetweight = new System.Windows.Forms.TextBox();
            this.txttaretime = new System.Windows.Forms.TextBox();
            this.lbltaretime = new System.Windows.Forms.Label();
            this.lbltareweight = new System.Windows.Forms.Label();
            this.txttareweight = new System.Windows.Forms.TextBox();
            this.txtgrosstime = new System.Windows.Forms.TextBox();
            this.lblgrosstime = new System.Windows.Forms.Label();
            this.lblgrossweigh = new System.Windows.Forms.Label();
            this.grosstxt = new System.Windows.Forms.TextBox();
            this.cbwardname = new System.Windows.Forms.ComboBox();
            this.cbzonename = new System.Windows.Forms.ComboBox();
            this.cbwastetype = new System.Windows.Forms.ComboBox();
            this.txtslipno = new System.Windows.Forms.TextBox();
            this.cbvendername = new System.Windows.Forms.ComboBox();
            this.lblwardname = new System.Windows.Forms.Label();
            this.lblzonename = new System.Windows.Forms.Label();
            this.lblwastetype = new System.Windows.Forms.Label();
            this.lblslipno = new System.Windows.Forms.Label();
            this.lbldrivername = new System.Windows.Forms.Label();
            this.lblvendorname = new System.Windows.Forms.Label();
            this.cbvehicle = new System.Windows.Forms.CheckBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lbltripid = new System.Windows.Forms.Label();
            this.lbltype = new System.Windows.Forms.Label();
            this.txtvehicleno = new System.Windows.Forms.TextBox();
            this.lblvehicleno = new System.Windows.Forms.Label();
            this.btncapture = new System.Windows.Forms.Button();
            this.txtweighweight = new System.Windows.Forms.TextBox();
            this.lblweighweight = new System.Windows.Forms.Label();
            this.lblnw = new System.Windows.Forms.Label();
            this.lblcamera = new System.Windows.Forms.Label();
            this.lblfp = new System.Windows.Forms.Label();
            this.grpbx2livevideo = new System.Windows.Forms.GroupBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.grpbx3summary = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.vendorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vehiclesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tripDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.weightDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tb1vehicleweigh = new System.Windows.Forms.TabControl();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ward = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.weight2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.spgetpendingtripdetailBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spgetpendingtripdetailBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spvehicletripdetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.daysummaryBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spgetpendingtripdetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblzoneBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spgettripdetailBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.daysummaryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet28)).BeginInit();
            this.tb7ReportsMore.SuspendLayout();
            this.tb4Weighmenthistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.gbweighdet.SuspendLayout();
            this.gbtareweight.SuspendLayout();
            this.gbgrossweight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.bgserchresult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.gbslipsearch.SuspendLayout();
            this.gbdaterange.SuspendLayout();
            this.tb2PendingTrips.SuspendLayout();
            this.gbtarependlist.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tb1Vehicleweighment.SuspendLayout();
            this.gbrecorddetail.SuspendLayout();
            this.grpbx2livevideo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.grpbx3summary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tb1vehicleweigh.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblusername
            // 
            this.lblusername.AutoSize = true;
            this.lblusername.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblusername.Location = new System.Drawing.Point(202, 12);
            this.lblusername.Name = "lblusername";
            this.lblusername.Size = new System.Drawing.Size(134, 31);
            this.lblusername.TabIndex = 11;
            this.lblusername.Text = "username";
            this.lblusername.UseWaitCursor = true;
            // 
            // spgetpendingtripdetailBindingSource2
            // 
            this.spgetpendingtripdetailBindingSource2.DataMember = "sp_getpendingtripdetail";
            this.spgetpendingtripdetailBindingSource2.DataSource = this.wmsDataSet8;
            // 
            // wmsDataSet8
            // 
            this.wmsDataSet8.DataSetName = "wmsDataSet8";
            this.wmsDataSet8.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // spgetpendingtripdetailBindingSource1
            // 
            this.spgetpendingtripdetailBindingSource1.DataMember = "sp_getpendingtripdetail";
            this.spgetpendingtripdetailBindingSource1.DataSource = this.wmsDataSet3;
            // 
            // wmsDataSet3
            // 
            this.wmsDataSet3.DataSetName = "wmsDataSet3";
            this.wmsDataSet3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // spvehicletripdetailBindingSource
            // 
            this.spvehicletripdetailBindingSource.DataMember = "sp_vehicletripdetail";
            this.spvehicletripdetailBindingSource.DataSource = this.wmsDataSet29;
            this.spvehicletripdetailBindingSource.CurrentChanged += new System.EventHandler(this.spvehicletripdetailBindingSource_CurrentChanged);
            // 
            // wmsDataSet29
            // 
            this.wmsDataSet29.DataSetName = "wmsDataSet29";
            this.wmsDataSet29.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnlogout
            // 
            this.btnlogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnlogout.Location = new System.Drawing.Point(1430, 12);
            this.btnlogout.Name = "btnlogout";
            this.btnlogout.Size = new System.Drawing.Size(90, 36);
            this.btnlogout.TabIndex = 16;
            this.btnlogout.Text = "Logout";
            this.btnlogout.UseVisualStyleBackColor = true;
            this.btnlogout.Click += new System.EventHandler(this.btnlogout_Click);
            // 
            // btnchangepassword
            // 
            this.btnchangepassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnchangepassword.Location = new System.Drawing.Point(1265, 12);
            this.btnchangepassword.Name = "btnchangepassword";
            this.btnchangepassword.Size = new System.Drawing.Size(159, 36);
            this.btnchangepassword.TabIndex = 15;
            this.btnchangepassword.Text = "Change Password";
            this.btnchangepassword.UseVisualStyleBackColor = true;
            this.btnchangepassword.Click += new System.EventHandler(this.btnchangepassword_Click);
            // 
            // btnverifyfinger
            // 
            this.btnverifyfinger.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnverifyfinger.Location = new System.Drawing.Point(1100, 12);
            this.btnverifyfinger.Name = "btnverifyfinger";
            this.btnverifyfinger.Size = new System.Drawing.Size(159, 36);
            this.btnverifyfinger.TabIndex = 14;
            this.btnverifyfinger.Text = "Verify Fingerprint";
            this.btnverifyfinger.UseVisualStyleBackColor = true;
            this.btnverifyfinger.Click += new System.EventHandler(this.btnverifyfinger_Click);
            // 
            // lblcurrentuser
            // 
            this.lblcurrentuser.AutoSize = true;
            this.lblcurrentuser.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcurrentuser.Location = new System.Drawing.Point(26, 12);
            this.lblcurrentuser.Name = "lblcurrentuser";
            this.lblcurrentuser.Size = new System.Drawing.Size(170, 31);
            this.lblcurrentuser.TabIndex = 13;
            this.lblcurrentuser.Text = "Current User";
            // 
            // daysummaryBindingSource1
            // 
            this.daysummaryBindingSource1.DataMember = "daysummary";
            this.daysummaryBindingSource1.DataSource = this.wmsDataSet10;
            // 
            // wmsDataSet10
            // 
            this.wmsDataSet10.DataSetName = "wmsDataSet10";
            this.wmsDataSet10.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // spgetpendingtripdetailBindingSource
            // 
            this.spgetpendingtripdetailBindingSource.DataMember = "sp_getpendingtripdetail";
            this.spgetpendingtripdetailBindingSource.DataSource = this.wmsDataSet7;
            // 
            // wmsDataSet7
            // 
            this.wmsDataSet7.DataSetName = "wmsDataSet7";
            this.wmsDataSet7.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tblzoneBindingSource
            // 
            this.tblzoneBindingSource.DataMember = "tbl_zone";
            this.tblzoneBindingSource.DataSource = this.wmsDataSet5;
            // 
            // wmsDataSet5
            // 
            this.wmsDataSet5.DataSetName = "wmsDataSet5";
            this.wmsDataSet5.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // wmsDataSet2
            // 
            this.wmsDataSet2.DataSetName = "wmsDataSet2";
            this.wmsDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // spgettripdetailBindingSource
            // 
            this.spgettripdetailBindingSource.DataMember = "sp_gettripdetail";
            this.spgettripdetailBindingSource.DataSource = this.wmsDataSet;
            // 
            // wmsDataSet
            // 
            this.wmsDataSet.DataSetName = "wmsDataSet";
            this.wmsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp_gettripdetailTableAdapter
            // 
            this.sp_gettripdetailTableAdapter.ClearBeforeFill = true;
            // 
            // wmsDataSet1
            // 
            this.wmsDataSet1.DataSetName = "wmsDataSet1";
            this.wmsDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // sp_getpendingtripdetailTableAdapter1
            // 
            this.sp_getpendingtripdetailTableAdapter1.ClearBeforeFill = true;
            // 
            // btnconfig
            // 
            this.btnconfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnconfig.Location = new System.Drawing.Point(935, 12);
            this.btnconfig.Name = "btnconfig";
            this.btnconfig.Size = new System.Drawing.Size(159, 36);
            this.btnconfig.TabIndex = 17;
            this.btnconfig.Text = "Configuration";
            this.btnconfig.UseVisualStyleBackColor = true;
            this.btnconfig.Click += new System.EventHandler(this.btnconfig_Click);
            // 
            // tbl_zoneTableAdapter
            // 
            this.tbl_zoneTableAdapter.ClearBeforeFill = true;
            // 
            // wmsDataSet6
            // 
            this.wmsDataSet6.DataSetName = "wmsDataSet6";
            this.wmsDataSet6.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp_getpendingtripdetailTableAdapter
            // 
            this.sp_getpendingtripdetailTableAdapter.ClearBeforeFill = true;
            // 
            // sp_getpendingtripdetailTableAdapter2
            // 
            this.sp_getpendingtripdetailTableAdapter2.ClearBeforeFill = true;
            // 
            // sp_getpendingtripdetailTableAdapter3
            // 
            this.sp_getpendingtripdetailTableAdapter3.ClearBeforeFill = true;
            // 
            // wmsDataSet9
            // 
            this.wmsDataSet9.DataSetName = "wmsDataSet9";
            this.wmsDataSet9.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // daysummaryBindingSource
            // 
            this.daysummaryBindingSource.DataMember = "daysummary";
            this.daysummaryBindingSource.DataSource = this.wmsDataSet9;
            // 
            // daysummaryTableAdapter
            // 
            this.daysummaryTableAdapter.ClearBeforeFill = true;
            // 
            // daysummaryTableAdapter1
            // 
            this.daysummaryTableAdapter1.ClearBeforeFill = true;
            // 
            // wmsDataSet19
            // 
            this.wmsDataSet19.DataSetName = "wmsDataSet19";
            this.wmsDataSet19.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // wmsDataSet20
            // 
            this.wmsDataSet20.DataSetName = "wmsDataSet20";
            this.wmsDataSet20.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // wmsDataSet21
            // 
            this.wmsDataSet21.DataSetName = "wmsDataSet21";
            this.wmsDataSet21.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // wmsDataSet22
            // 
            this.wmsDataSet22.DataSetName = "wmsDataSet22";
            this.wmsDataSet22.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // wmsDataSet23
            // 
            this.wmsDataSet23.DataSetName = "wmsDataSet23";
            this.wmsDataSet23.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // wmsDataSet24
            // 
            this.wmsDataSet24.DataSetName = "wmsDataSet24";
            this.wmsDataSet24.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // wmsDataSet25
            // 
            this.wmsDataSet25.DataSetName = "wmsDataSet25";
            this.wmsDataSet25.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // wmsDataSet26
            // 
            this.wmsDataSet26.DataSetName = "wmsDataSet26";
            this.wmsDataSet26.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // wmsDataSet27
            // 
            this.wmsDataSet27.DataSetName = "wmsDataSet27";
            this.wmsDataSet27.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // wmsDataSet28
            // 
            this.wmsDataSet28.DataSetName = "wmsDataSet28";
            this.wmsDataSet28.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp_vehicletripdetailTableAdapter
            // 
            this.sp_vehicletripdetailTableAdapter.ClearBeforeFill = true;
            // 
            // tb7ReportsMore
            // 
            this.tb7ReportsMore.Controls.Add(this.comboBox2);
            this.tb7ReportsMore.Controls.Add(this.label5);
            this.tb7ReportsMore.Controls.Add(this.reportViewer1);
            this.tb7ReportsMore.Controls.Add(this.label9);
            this.tb7ReportsMore.Controls.Add(this.dateTimePicker3);
            this.tb7ReportsMore.Controls.Add(this.btnviewreprt);
            this.tb7ReportsMore.Controls.Add(this.cbrepttyp);
            this.tb7ReportsMore.Controls.Add(this.lblreprttyp);
            this.tb7ReportsMore.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb7ReportsMore.Location = new System.Drawing.Point(4, 34);
            this.tb7ReportsMore.Name = "tb7ReportsMore";
            this.tb7ReportsMore.Padding = new System.Windows.Forms.Padding(3);
            this.tb7ReportsMore.Size = new System.Drawing.Size(1552, 720);
            this.tb7ReportsMore.TabIndex = 6;
            this.tb7ReportsMore.Text = "Reports";
            this.tb7ReportsMore.UseVisualStyleBackColor = true;
            this.tb7ReportsMore.Click += new System.EventHandler(this.tb7ReportsMore_Click);
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(790, 21);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(197, 32);
            this.comboBox2.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(666, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 31);
            this.label5.TabIndex = 82;
            this.label5.Text = "Vendor";
            this.label5.UseWaitCursor = true;
            // 
            // reportViewer1
            // 
            this.reportViewer1.Location = new System.Drawing.Point(29, 79);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ServerReport.BearerToken = null;
            this.reportViewer1.Size = new System.Drawing.Size(793, 1122);
            this.reportViewer1.TabIndex = 81;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(430, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 25);
            this.label9.TabIndex = 80;
            this.label9.Text = "Date";
            this.label9.UseWaitCursor = true;
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker3.Checked = false;
            this.dateTimePicker3.CustomFormat = "dd-MM-yyyy";
            this.dateTimePicker3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker3.Location = new System.Drawing.Point(510, 24);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.Size = new System.Drawing.Size(133, 26);
            this.dateTimePicker3.TabIndex = 1;
            this.dateTimePicker3.Value = new System.DateTime(2021, 7, 22, 0, 0, 0, 0);
            this.dateTimePicker3.ValueChanged += new System.EventHandler(this.dateTimePicker3_ValueChanged);
            // 
            // btnviewreprt
            // 
            this.btnviewreprt.Location = new System.Drawing.Point(1010, 19);
            this.btnviewreprt.Name = "btnviewreprt";
            this.btnviewreprt.Size = new System.Drawing.Size(197, 36);
            this.btnviewreprt.TabIndex = 3;
            this.btnviewreprt.Text = "View Report";
            this.btnviewreprt.UseVisualStyleBackColor = true;
            this.btnviewreprt.Click += new System.EventHandler(this.btnviewreprt_Click);
            // 
            // cbrepttyp
            // 
            this.cbrepttyp.FormattingEnabled = true;
            this.cbrepttyp.Items.AddRange(new object[] {
            "Residential",
            "Commercial"});
            this.cbrepttyp.Location = new System.Drawing.Point(210, 21);
            this.cbrepttyp.Name = "cbrepttyp";
            this.cbrepttyp.Size = new System.Drawing.Size(197, 32);
            this.cbrepttyp.TabIndex = 0;
            // 
            // lblreprttyp
            // 
            this.lblreprttyp.AutoSize = true;
            this.lblreprttyp.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblreprttyp.Location = new System.Drawing.Point(23, 22);
            this.lblreprttyp.Name = "lblreprttyp";
            this.lblreprttyp.Size = new System.Drawing.Size(164, 31);
            this.lblreprttyp.TabIndex = 17;
            this.lblreprttyp.Text = "Report Type";
            this.lblreprttyp.UseWaitCursor = true;
            // 
            // tb4Weighmenthistory
            // 
            this.tb4Weighmenthistory.Controls.Add(this.pictureBox4);
            this.tb4Weighmenthistory.Controls.Add(this.gbweighdet);
            this.tb4Weighmenthistory.Controls.Add(this.pictureBox1);
            this.tb4Weighmenthistory.Controls.Add(this.bgserchresult);
            this.tb4Weighmenthistory.Controls.Add(this.gbslipsearch);
            this.tb4Weighmenthistory.Controls.Add(this.gbdaterange);
            this.tb4Weighmenthistory.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb4Weighmenthistory.Location = new System.Drawing.Point(4, 34);
            this.tb4Weighmenthistory.Name = "tb4Weighmenthistory";
            this.tb4Weighmenthistory.Padding = new System.Windows.Forms.Padding(3);
            this.tb4Weighmenthistory.Size = new System.Drawing.Size(1552, 720);
            this.tb4Weighmenthistory.TabIndex = 3;
            this.tb4Weighmenthistory.Text = "Weighment history";
            this.tb4Weighmenthistory.UseVisualStyleBackColor = true;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(1047, 531);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(484, 174);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 10;
            this.pictureBox4.TabStop = false;
            // 
            // gbweighdet
            // 
            this.gbweighdet.Controls.Add(this.cbvehcltyp);
            this.gbweighdet.Controls.Add(this.btnupdttrp);
            this.gbweighdet.Controls.Add(this.btnignrtrp);
            this.gbweighdet.Controls.Add(this.btnprntslp);
            this.gbweighdet.Controls.Add(this.txtnetweigh);
            this.gbweighdet.Controls.Add(this.cbwrdname);
            this.gbweighdet.Controls.Add(this.cbznname);
            this.gbweighdet.Controls.Add(this.cbwsttyp);
            this.gbweighdet.Controls.Add(this.lblvehcltyp);
            this.gbweighdet.Controls.Add(this.cbdrivername);
            this.gbweighdet.Controls.Add(this.cbvndrname);
            this.gbweighdet.Controls.Add(this.txttripid);
            this.gbweighdet.Controls.Add(this.txtslipid);
            this.gbweighdet.Controls.Add(this.label6);
            this.gbweighdet.Controls.Add(this.label7);
            this.gbweighdet.Controls.Add(this.label8);
            this.gbweighdet.Controls.Add(this.lblwsttyp);
            this.gbweighdet.Controls.Add(this.label11);
            this.gbweighdet.Controls.Add(this.label12);
            this.gbweighdet.Controls.Add(this.lbltrpid);
            this.gbweighdet.Controls.Add(this.lblslpid);
            this.gbweighdet.Controls.Add(this.gbtareweight);
            this.gbweighdet.Controls.Add(this.gbgrossweight);
            this.gbweighdet.Location = new System.Drawing.Point(16, 307);
            this.gbweighdet.Name = "gbweighdet";
            this.gbweighdet.Size = new System.Drawing.Size(1008, 398);
            this.gbweighdet.TabIndex = 8;
            this.gbweighdet.TabStop = false;
            this.gbweighdet.Text = "Weighment details";
            // 
            // cbvehcltyp
            // 
            this.cbvehcltyp.FormattingEnabled = true;
            this.cbvehcltyp.Location = new System.Drawing.Point(137, 81);
            this.cbvehcltyp.Name = "cbvehcltyp";
            this.cbvehcltyp.Size = new System.Drawing.Size(489, 32);
            this.cbvehcltyp.TabIndex = 85;
            // 
            // btnupdttrp
            // 
            this.btnupdttrp.Location = new System.Drawing.Point(495, 351);
            this.btnupdttrp.Name = "btnupdttrp";
            this.btnupdttrp.Size = new System.Drawing.Size(131, 33);
            this.btnupdttrp.TabIndex = 91;
            this.btnupdttrp.Text = "Update trip";
            this.btnupdttrp.UseVisualStyleBackColor = true;
            // 
            // btnignrtrp
            // 
            this.btnignrtrp.Location = new System.Drawing.Point(371, 352);
            this.btnignrtrp.Name = "btnignrtrp";
            this.btnignrtrp.Size = new System.Drawing.Size(103, 33);
            this.btnignrtrp.TabIndex = 90;
            this.btnignrtrp.Text = "Ignore trip";
            this.btnignrtrp.UseVisualStyleBackColor = true;
            // 
            // btnprntslp
            // 
            this.btnprntslp.Location = new System.Drawing.Point(247, 353);
            this.btnprntslp.Name = "btnprntslp";
            this.btnprntslp.Size = new System.Drawing.Size(103, 33);
            this.btnprntslp.TabIndex = 71;
            this.btnprntslp.Text = "Print slip";
            this.btnprntslp.UseVisualStyleBackColor = true;
            // 
            // txtnetweigh
            // 
            this.txtnetweigh.Location = new System.Drawing.Point(137, 351);
            this.txtnetweigh.Name = "txtnetweigh";
            this.txtnetweigh.Size = new System.Drawing.Size(100, 29);
            this.txtnetweigh.TabIndex = 89;
            // 
            // cbwrdname
            // 
            this.cbwrdname.FormattingEnabled = true;
            this.cbwrdname.Location = new System.Drawing.Point(137, 306);
            this.cbwrdname.Name = "cbwrdname";
            this.cbwrdname.Size = new System.Drawing.Size(489, 32);
            this.cbwrdname.TabIndex = 88;
            // 
            // cbznname
            // 
            this.cbznname.FormattingEnabled = true;
            this.cbznname.Location = new System.Drawing.Point(137, 261);
            this.cbznname.Name = "cbznname";
            this.cbznname.Size = new System.Drawing.Size(489, 32);
            this.cbznname.TabIndex = 87;
            this.cbznname.SelectedIndexChanged += new System.EventHandler(this.cbznname_SelectedIndexChanged);
            // 
            // cbwsttyp
            // 
            this.cbwsttyp.FormattingEnabled = true;
            this.cbwsttyp.Location = new System.Drawing.Point(137, 216);
            this.cbwsttyp.Name = "cbwsttyp";
            this.cbwsttyp.Size = new System.Drawing.Size(489, 32);
            this.cbwsttyp.TabIndex = 86;
            // 
            // lblvehcltyp
            // 
            this.lblvehcltyp.AutoSize = true;
            this.lblvehcltyp.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblvehcltyp.Location = new System.Drawing.Point(15, 83);
            this.lblvehcltyp.Name = "lblvehcltyp";
            this.lblvehcltyp.Size = new System.Drawing.Size(114, 24);
            this.lblvehcltyp.TabIndex = 84;
            this.lblvehcltyp.Text = "Vehicle type";
            this.lblvehcltyp.UseWaitCursor = true;
            // 
            // cbdrivername
            // 
            this.cbdrivername.FormattingEnabled = true;
            this.cbdrivername.Location = new System.Drawing.Point(137, 171);
            this.cbdrivername.Name = "cbdrivername";
            this.cbdrivername.Size = new System.Drawing.Size(489, 32);
            this.cbdrivername.TabIndex = 83;
            // 
            // cbvndrname
            // 
            this.cbvndrname.FormattingEnabled = true;
            this.cbvndrname.Location = new System.Drawing.Point(137, 126);
            this.cbvndrname.Name = "cbvndrname";
            this.cbvndrname.Size = new System.Drawing.Size(489, 32);
            this.cbvndrname.TabIndex = 82;
            // 
            // txttripid
            // 
            this.txttripid.Location = new System.Drawing.Point(413, 39);
            this.txttripid.Name = "txttripid";
            this.txttripid.Size = new System.Drawing.Size(213, 29);
            this.txttripid.TabIndex = 81;
            // 
            // txtslipid
            // 
            this.txtslipid.Location = new System.Drawing.Point(84, 39);
            this.txtslipid.Name = "txtslipid";
            this.txtslipid.Size = new System.Drawing.Size(252, 29);
            this.txtslipid.TabIndex = 72;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(15, 347);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(99, 24);
            this.label6.TabIndex = 80;
            this.label6.Text = "Net weight";
            this.label6.UseWaitCursor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(15, 303);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 24);
            this.label7.TabIndex = 79;
            this.label7.Text = "Ward Name";
            this.label7.UseWaitCursor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(15, 259);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 24);
            this.label8.TabIndex = 78;
            this.label8.Text = "Zone Name";
            this.label8.UseWaitCursor = true;
            // 
            // lblwsttyp
            // 
            this.lblwsttyp.AutoSize = true;
            this.lblwsttyp.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblwsttyp.Location = new System.Drawing.Point(15, 215);
            this.lblwsttyp.Name = "lblwsttyp";
            this.lblwsttyp.Size = new System.Drawing.Size(102, 24);
            this.lblwsttyp.TabIndex = 77;
            this.lblwsttyp.Text = "Waste type";
            this.lblwsttyp.UseWaitCursor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(15, 171);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(112, 24);
            this.label11.TabIndex = 76;
            this.label11.Text = "Driver name";
            this.label11.UseWaitCursor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(15, 127);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(126, 24);
            this.label12.TabIndex = 75;
            this.label12.Text = "Vendor name";
            this.label12.UseWaitCursor = true;
            // 
            // lbltrpid
            // 
            this.lbltrpid.AutoSize = true;
            this.lbltrpid.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltrpid.Location = new System.Drawing.Point(342, 39);
            this.lbltrpid.Name = "lbltrpid";
            this.lbltrpid.Size = new System.Drawing.Size(65, 24);
            this.lbltrpid.TabIndex = 74;
            this.lbltrpid.Text = "Trip ID";
            this.lbltrpid.UseWaitCursor = true;
            // 
            // lblslpid
            // 
            this.lblslpid.AutoSize = true;
            this.lblslpid.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblslpid.Location = new System.Drawing.Point(15, 39);
            this.lblslpid.Name = "lblslpid";
            this.lblslpid.Size = new System.Drawing.Size(63, 24);
            this.lblslpid.TabIndex = 73;
            this.lblslpid.Text = "Slip ID";
            this.lblslpid.UseWaitCursor = true;
            // 
            // gbtareweight
            // 
            this.gbtareweight.Controls.Add(this.txttareuser);
            this.gbtareweight.Controls.Add(this.label4);
            this.gbtareweight.Controls.Add(this.txttareweightime);
            this.gbtareweight.Controls.Add(this.label3);
            this.gbtareweight.Controls.Add(this.txttareweigh);
            this.gbtareweight.Controls.Add(this.label2);
            this.gbtareweight.Location = new System.Drawing.Point(640, 196);
            this.gbtareweight.Name = "gbtareweight";
            this.gbtareweight.Size = new System.Drawing.Size(362, 177);
            this.gbtareweight.TabIndex = 10;
            this.gbtareweight.TabStop = false;
            this.gbtareweight.Text = "Tare weight";
            // 
            // txttareuser
            // 
            this.txttareuser.Location = new System.Drawing.Point(133, 127);
            this.txttareuser.Name = "txttareuser";
            this.txttareuser.Size = new System.Drawing.Size(223, 29);
            this.txttareuser.TabIndex = 27;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(19, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 31);
            this.label4.TabIndex = 23;
            this.label4.Text = "Weight";
            this.label4.UseWaitCursor = true;
            // 
            // txttareweightime
            // 
            this.txttareweightime.Location = new System.Drawing.Point(133, 82);
            this.txttareweightime.Name = "txttareweightime";
            this.txttareweightime.Size = new System.Drawing.Size(163, 29);
            this.txttareweightime.TabIndex = 26;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(19, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 31);
            this.label3.TabIndex = 24;
            this.label3.Text = "Time";
            this.label3.UseWaitCursor = true;
            // 
            // txttareweigh
            // 
            this.txttareweigh.Location = new System.Drawing.Point(133, 39);
            this.txttareweigh.Name = "txttareweigh";
            this.txttareweigh.Size = new System.Drawing.Size(163, 29);
            this.txttareweigh.TabIndex = 22;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 127);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 31);
            this.label2.TabIndex = 25;
            this.label2.Text = "User";
            this.label2.UseWaitCursor = true;
            // 
            // gbgrossweight
            // 
            this.gbgrossweight.Controls.Add(this.txtgrossuser);
            this.gbgrossweight.Controls.Add(this.txtgrossweitim);
            this.gbgrossweight.Controls.Add(this.txtgrossweigh);
            this.gbgrossweight.Controls.Add(this.lbluser);
            this.gbgrossweight.Controls.Add(this.lbltim);
            this.gbgrossweight.Controls.Add(this.lblweigh);
            this.gbgrossweight.Location = new System.Drawing.Point(640, 25);
            this.gbgrossweight.Name = "gbgrossweight";
            this.gbgrossweight.Size = new System.Drawing.Size(362, 165);
            this.gbgrossweight.TabIndex = 9;
            this.gbgrossweight.TabStop = false;
            this.gbgrossweight.Text = "Gross weight";
            // 
            // txtgrossuser
            // 
            this.txtgrossuser.Location = new System.Drawing.Point(133, 115);
            this.txtgrossuser.Name = "txtgrossuser";
            this.txtgrossuser.Size = new System.Drawing.Size(223, 29);
            this.txtgrossuser.TabIndex = 21;
            // 
            // txtgrossweitim
            // 
            this.txtgrossweitim.Location = new System.Drawing.Point(133, 70);
            this.txtgrossweitim.Name = "txtgrossweitim";
            this.txtgrossweitim.Size = new System.Drawing.Size(163, 29);
            this.txtgrossweitim.TabIndex = 20;
            // 
            // txtgrossweigh
            // 
            this.txtgrossweigh.Location = new System.Drawing.Point(133, 27);
            this.txtgrossweigh.Name = "txtgrossweigh";
            this.txtgrossweigh.Size = new System.Drawing.Size(163, 29);
            this.txtgrossweigh.TabIndex = 7;
            // 
            // lbluser
            // 
            this.lbluser.AutoSize = true;
            this.lbluser.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbluser.Location = new System.Drawing.Point(19, 115);
            this.lbluser.Name = "lbluser";
            this.lbluser.Size = new System.Drawing.Size(72, 31);
            this.lbluser.TabIndex = 19;
            this.lbluser.Text = "User";
            this.lbluser.UseWaitCursor = true;
            // 
            // lbltim
            // 
            this.lbltim.AutoSize = true;
            this.lbltim.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltim.Location = new System.Drawing.Point(19, 70);
            this.lbltim.Name = "lbltim";
            this.lbltim.Size = new System.Drawing.Size(74, 31);
            this.lbltim.TabIndex = 18;
            this.lbltim.Text = "Time";
            this.lbltim.UseWaitCursor = true;
            // 
            // lblweigh
            // 
            this.lblweigh.AutoSize = true;
            this.lblweigh.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblweigh.Location = new System.Drawing.Point(19, 25);
            this.lblweigh.Name = "lblweigh";
            this.lblweigh.Size = new System.Drawing.Size(98, 31);
            this.lblweigh.TabIndex = 17;
            this.lblweigh.Text = "Weight";
            this.lblweigh.UseWaitCursor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1047, 318);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(484, 174);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // bgserchresult
            // 
            this.bgserchresult.Controls.Add(this.dataGridView3);
            this.bgserchresult.Location = new System.Drawing.Point(6, 93);
            this.bgserchresult.Name = "bgserchresult";
            this.bgserchresult.Size = new System.Drawing.Size(1525, 208);
            this.bgserchresult.TabIndex = 7;
            this.bgserchresult.TabStop = false;
            this.bgserchresult.Text = "Search result";
            // 
            // dataGridView3
            // 
            this.dataGridView3.AutoGenerateColumns = false;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.vehnumDataGridViewTextBoxColumn,
            this.tripidDataGridViewTextBoxColumn,
            this.vehtypeDataGridViewTextBoxColumn,
            this.typeDataGridViewTextBoxColumn,
            this.zonenameDataGridViewTextBoxColumn,
            this.wardnameDataGridViewTextBoxColumn,
            this.indatetimeDataGridViewTextBoxColumn,
            this.weight1DataGridViewTextBoxColumn});
            this.dataGridView3.DataSource = this.spvehicletripdetailBindingSource;
            this.dataGridView3.Location = new System.Drawing.Point(12, 28);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.ReadOnly = true;
            this.dataGridView3.Size = new System.Drawing.Size(1497, 164);
            this.dataGridView3.TabIndex = 0;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Slip ID";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // vehnumDataGridViewTextBoxColumn
            // 
            this.vehnumDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.vehnumDataGridViewTextBoxColumn.DataPropertyName = "vehnum";
            this.vehnumDataGridViewTextBoxColumn.HeaderText = "Vehicle Number";
            this.vehnumDataGridViewTextBoxColumn.Name = "vehnumDataGridViewTextBoxColumn";
            this.vehnumDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tripidDataGridViewTextBoxColumn
            // 
            this.tripidDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.tripidDataGridViewTextBoxColumn.DataPropertyName = "tripid";
            this.tripidDataGridViewTextBoxColumn.HeaderText = "Trip ID";
            this.tripidDataGridViewTextBoxColumn.Name = "tripidDataGridViewTextBoxColumn";
            this.tripidDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // vehtypeDataGridViewTextBoxColumn
            // 
            this.vehtypeDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.vehtypeDataGridViewTextBoxColumn.DataPropertyName = "vehtype";
            this.vehtypeDataGridViewTextBoxColumn.HeaderText = "Vehicle Type";
            this.vehtypeDataGridViewTextBoxColumn.Name = "vehtypeDataGridViewTextBoxColumn";
            this.vehtypeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // typeDataGridViewTextBoxColumn
            // 
            this.typeDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.typeDataGridViewTextBoxColumn.DataPropertyName = "type";
            this.typeDataGridViewTextBoxColumn.HeaderText = "Waste Type";
            this.typeDataGridViewTextBoxColumn.Name = "typeDataGridViewTextBoxColumn";
            this.typeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // zonenameDataGridViewTextBoxColumn
            // 
            this.zonenameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.zonenameDataGridViewTextBoxColumn.DataPropertyName = "zonename";
            this.zonenameDataGridViewTextBoxColumn.HeaderText = "Zone";
            this.zonenameDataGridViewTextBoxColumn.Name = "zonenameDataGridViewTextBoxColumn";
            this.zonenameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // wardnameDataGridViewTextBoxColumn
            // 
            this.wardnameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.wardnameDataGridViewTextBoxColumn.DataPropertyName = "wardname";
            this.wardnameDataGridViewTextBoxColumn.HeaderText = "Ward";
            this.wardnameDataGridViewTextBoxColumn.Name = "wardnameDataGridViewTextBoxColumn";
            this.wardnameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // indatetimeDataGridViewTextBoxColumn
            // 
            this.indatetimeDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.indatetimeDataGridViewTextBoxColumn.DataPropertyName = "indatetime";
            this.indatetimeDataGridViewTextBoxColumn.HeaderText = "In DateTime";
            this.indatetimeDataGridViewTextBoxColumn.Name = "indatetimeDataGridViewTextBoxColumn";
            this.indatetimeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // weight1DataGridViewTextBoxColumn
            // 
            this.weight1DataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.weight1DataGridViewTextBoxColumn.DataPropertyName = "weight1";
            this.weight1DataGridViewTextBoxColumn.HeaderText = "Gross Weight";
            this.weight1DataGridViewTextBoxColumn.Name = "weight1DataGridViewTextBoxColumn";
            this.weight1DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // gbslipsearch
            // 
            this.gbslipsearch.Controls.Add(this.btnsearch);
            this.gbslipsearch.Controls.Add(this.textBox3);
            this.gbslipsearch.Location = new System.Drawing.Point(989, 15);
            this.gbslipsearch.Name = "gbslipsearch";
            this.gbslipsearch.Size = new System.Drawing.Size(542, 78);
            this.gbslipsearch.TabIndex = 1;
            this.gbslipsearch.TabStop = false;
            this.gbslipsearch.Text = "Slip number search";
            // 
            // btnsearch
            // 
            this.btnsearch.Location = new System.Drawing.Point(357, 31);
            this.btnsearch.Name = "btnsearch";
            this.btnsearch.Size = new System.Drawing.Size(145, 29);
            this.btnsearch.TabIndex = 1;
            this.btnsearch.Text = "Search";
            this.btnsearch.UseVisualStyleBackColor = true;
            this.btnsearch.Click += new System.EventHandler(this.btnsearch_Click);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(17, 31);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(321, 29);
            this.textBox3.TabIndex = 0;
            // 
            // gbdaterange
            // 
            this.gbdaterange.Controls.Add(this.lblto);
            this.gbdaterange.Controls.Add(this.lblfrm);
            this.gbdaterange.Controls.Add(this.dateTimePicker2);
            this.gbdaterange.Controls.Add(this.dateTimePicker1);
            this.gbdaterange.Controls.Add(this.lblvehclnu);
            this.gbdaterange.Controls.Add(this.btngetrec);
            this.gbdaterange.Controls.Add(this.txtvehclnu);
            this.gbdaterange.Location = new System.Drawing.Point(6, 14);
            this.gbdaterange.Name = "gbdaterange";
            this.gbdaterange.Size = new System.Drawing.Size(977, 79);
            this.gbdaterange.TabIndex = 0;
            this.gbdaterange.TabStop = false;
            this.gbdaterange.Text = "Date range selection";
            // 
            // lblto
            // 
            this.lblto.AutoSize = true;
            this.lblto.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblto.Location = new System.Drawing.Point(289, 33);
            this.lblto.Name = "lblto";
            this.lblto.Size = new System.Drawing.Size(33, 24);
            this.lblto.TabIndex = 72;
            this.lblto.Text = "To";
            this.lblto.UseWaitCursor = true;
            // 
            // lblfrm
            // 
            this.lblfrm.AutoSize = true;
            this.lblfrm.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfrm.Location = new System.Drawing.Point(0, 33);
            this.lblfrm.Name = "lblfrm";
            this.lblfrm.Size = new System.Drawing.Size(55, 24);
            this.lblfrm.TabIndex = 71;
            this.lblfrm.Text = "From";
            this.lblfrm.UseWaitCursor = true;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.CustomFormat = "yyyy-MM-dd hh:mm:ss";
            this.dateTimePicker2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker2.Location = new System.Drawing.Point(74, 32);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(196, 26);
            this.dateTimePicker2.TabIndex = 0;
            this.dateTimePicker2.Value = new System.DateTime(2021, 7, 23, 11, 21, 11, 0);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "yyyy-MM-dd hh:mm:ss";
            this.dateTimePicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(341, 32);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(0);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(192, 26);
            this.dateTimePicker1.TabIndex = 1;
            this.dateTimePicker1.Value = new System.DateTime(2021, 7, 23, 11, 21, 32, 0);
            // 
            // lblvehclnu
            // 
            this.lblvehclnu.AutoSize = true;
            this.lblvehclnu.Location = new System.Drawing.Point(552, 33);
            this.lblvehclnu.Name = "lblvehclnu";
            this.lblvehclnu.Size = new System.Drawing.Size(106, 24);
            this.lblvehclnu.TabIndex = 4;
            this.lblvehclnu.Text = "Vehicle no.";
            // 
            // btngetrec
            // 
            this.btngetrec.Location = new System.Drawing.Point(859, 31);
            this.btngetrec.Name = "btngetrec";
            this.btngetrec.Size = new System.Drawing.Size(103, 29);
            this.btngetrec.TabIndex = 3;
            this.btngetrec.Text = "Get records";
            this.btngetrec.UseVisualStyleBackColor = true;
            this.btngetrec.Click += new System.EventHandler(this.btngetrec_Click);
            // 
            // txtvehclnu
            // 
            this.txtvehclnu.Location = new System.Drawing.Point(677, 31);
            this.txtvehclnu.Name = "txtvehclnu";
            this.txtvehclnu.Size = new System.Drawing.Size(163, 29);
            this.txtvehclnu.TabIndex = 2;
            // 
            // tb2PendingTrips
            // 
            this.tb2PendingTrips.Controls.Add(this.gbtarependlist);
            this.tb2PendingTrips.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb2PendingTrips.Location = new System.Drawing.Point(4, 34);
            this.tb2PendingTrips.Name = "tb2PendingTrips";
            this.tb2PendingTrips.Padding = new System.Windows.Forms.Padding(3);
            this.tb2PendingTrips.Size = new System.Drawing.Size(1552, 720);
            this.tb2PendingTrips.TabIndex = 1;
            this.tb2PendingTrips.Text = "Pending Trips";
            this.tb2PendingTrips.UseVisualStyleBackColor = true;
            // 
            // gbtarependlist
            // 
            this.gbtarependlist.Controls.Add(this.dataGridView2);
            this.gbtarependlist.Controls.Add(this.btnignoretrip);
            this.gbtarependlist.Controls.Add(this.btncompltrip);
            this.gbtarependlist.Location = new System.Drawing.Point(6, 6);
            this.gbtarependlist.Name = "gbtarependlist";
            this.gbtarependlist.Size = new System.Drawing.Size(1529, 612);
            this.gbtarependlist.TabIndex = 0;
            this.gbtarependlist.TabStop = false;
            this.gbtarependlist.Text = "Tare pending list";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn1,
            this.zone,
            this.ward,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn4,
            this.weight2});
            this.dataGridView2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.dataGridView2.DataSource = this.spgetpendingtripdetailBindingSource2;
            this.dataGridView2.Location = new System.Drawing.Point(6, 28);
            this.dataGridView2.MultiSelect = false;
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(1516, 516);
            this.dataGridView2.StandardTab = true;
            this.dataGridView2.TabIndex = 3;
            // 
            // btnignoretrip
            // 
            this.btnignoretrip.Location = new System.Drawing.Point(1325, 562);
            this.btnignoretrip.Name = "btnignoretrip";
            this.btnignoretrip.Size = new System.Drawing.Size(197, 39);
            this.btnignoretrip.TabIndex = 1;
            this.btnignoretrip.Text = "Ignore trip";
            this.btnignoretrip.UseVisualStyleBackColor = true;
            this.btnignoretrip.Click += new System.EventHandler(this.btnignoretrip_Click);
            // 
            // btncompltrip
            // 
            this.btncompltrip.Location = new System.Drawing.Point(1107, 562);
            this.btncompltrip.Name = "btncompltrip";
            this.btncompltrip.Size = new System.Drawing.Size(197, 39);
            this.btncompltrip.TabIndex = 0;
            this.btncompltrip.Text = "Complete trip";
            this.btncompltrip.UseVisualStyleBackColor = true;
            this.btncompltrip.Click += new System.EventHandler(this.btncompltrip_Click);
            // 
            // tb1Vehicleweighment
            // 
            this.tb1Vehicleweighment.Controls.Add(this.gbrecorddetail);
            this.tb1Vehicleweighment.Controls.Add(this.btncapture);
            this.tb1Vehicleweighment.Controls.Add(this.txtweighweight);
            this.tb1Vehicleweighment.Controls.Add(this.lblweighweight);
            this.tb1Vehicleweighment.Controls.Add(this.lblnw);
            this.tb1Vehicleweighment.Controls.Add(this.lblcamera);
            this.tb1Vehicleweighment.Controls.Add(this.lblfp);
            this.tb1Vehicleweighment.Controls.Add(this.grpbx2livevideo);
            this.tb1Vehicleweighment.Controls.Add(this.grpbx3summary);
            this.tb1Vehicleweighment.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb1Vehicleweighment.Location = new System.Drawing.Point(4, 34);
            this.tb1Vehicleweighment.Name = "tb1Vehicleweighment";
            this.tb1Vehicleweighment.Padding = new System.Windows.Forms.Padding(3);
            this.tb1Vehicleweighment.Size = new System.Drawing.Size(1552, 720);
            this.tb1Vehicleweighment.TabIndex = 0;
            this.tb1Vehicleweighment.Text = "Vehicle weighment";
            this.tb1Vehicleweighment.UseVisualStyleBackColor = true;
            // 
            // gbrecorddetail
            // 
            this.gbrecorddetail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gbrecorddetail.Controls.Add(this.cbdrvrname);
            this.gbrecorddetail.Controls.Add(this.txttyp);
            this.gbrecorddetail.Controls.Add(this.btnnewrecord);
            this.gbrecorddetail.Controls.Add(this.btnsaveprint);
            this.gbrecorddetail.Controls.Add(this.lblnetweight);
            this.gbrecorddetail.Controls.Add(this.txtnetweight);
            this.gbrecorddetail.Controls.Add(this.txttaretime);
            this.gbrecorddetail.Controls.Add(this.lbltaretime);
            this.gbrecorddetail.Controls.Add(this.lbltareweight);
            this.gbrecorddetail.Controls.Add(this.txttareweight);
            this.gbrecorddetail.Controls.Add(this.txtgrosstime);
            this.gbrecorddetail.Controls.Add(this.lblgrosstime);
            this.gbrecorddetail.Controls.Add(this.lblgrossweigh);
            this.gbrecorddetail.Controls.Add(this.grosstxt);
            this.gbrecorddetail.Controls.Add(this.cbwardname);
            this.gbrecorddetail.Controls.Add(this.cbzonename);
            this.gbrecorddetail.Controls.Add(this.cbwastetype);
            this.gbrecorddetail.Controls.Add(this.txtslipno);
            this.gbrecorddetail.Controls.Add(this.cbvendername);
            this.gbrecorddetail.Controls.Add(this.lblwardname);
            this.gbrecorddetail.Controls.Add(this.lblzonename);
            this.gbrecorddetail.Controls.Add(this.lblwastetype);
            this.gbrecorddetail.Controls.Add(this.lblslipno);
            this.gbrecorddetail.Controls.Add(this.lbldrivername);
            this.gbrecorddetail.Controls.Add(this.lblvendorname);
            this.gbrecorddetail.Controls.Add(this.cbvehicle);
            this.gbrecorddetail.Controls.Add(this.textBox1);
            this.gbrecorddetail.Controls.Add(this.lbltripid);
            this.gbrecorddetail.Controls.Add(this.lbltype);
            this.gbrecorddetail.Controls.Add(this.txtvehicleno);
            this.gbrecorddetail.Controls.Add(this.lblvehicleno);
            this.gbrecorddetail.Location = new System.Drawing.Point(28, 113);
            this.gbrecorddetail.Name = "gbrecorddetail";
            this.gbrecorddetail.Size = new System.Drawing.Size(696, 587);
            this.gbrecorddetail.TabIndex = 23;
            this.gbrecorddetail.TabStop = false;
            this.gbrecorddetail.Text = "Record detail";
            // 
            // cbdrvrname
            // 
            this.cbdrvrname.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbdrvrname.FormattingEnabled = true;
            this.cbdrvrname.Location = new System.Drawing.Point(147, 177);
            this.cbdrvrname.Name = "cbdrvrname";
            this.cbdrvrname.Size = new System.Drawing.Size(530, 32);
            this.cbdrvrname.TabIndex = 4;
            // 
            // txttyp
            // 
            this.txttyp.Location = new System.Drawing.Point(384, 27);
            this.txttyp.MaxLength = 100;
            this.txttyp.Name = "txttyp";
            this.txttyp.Size = new System.Drawing.Size(293, 29);
            this.txttyp.TabIndex = 1;
            // 
            // btnnewrecord
            // 
            this.btnnewrecord.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnnewrecord.Location = new System.Drawing.Point(521, 531);
            this.btnnewrecord.Name = "btnnewrecord";
            this.btnnewrecord.Size = new System.Drawing.Size(157, 30);
            this.btnnewrecord.TabIndex = 2;
            this.btnnewrecord.Text = "New record";
            this.btnnewrecord.UseVisualStyleBackColor = true;
            this.btnnewrecord.Click += new System.EventHandler(this.btnnewrecord_Click_1);
            // 
            // btnsaveprint
            // 
            this.btnsaveprint.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsaveprint.Location = new System.Drawing.Point(346, 531);
            this.btnsaveprint.Name = "btnsaveprint";
            this.btnsaveprint.Size = new System.Drawing.Size(157, 30);
            this.btnsaveprint.TabIndex = 1;
            this.btnsaveprint.Text = "Save";
            this.btnsaveprint.UseVisualStyleBackColor = true;
            this.btnsaveprint.Click += new System.EventHandler(this.btnsaveprint_Click);
            // 
            // lblnetweight
            // 
            this.lblnetweight.AutoSize = true;
            this.lblnetweight.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblnetweight.Location = new System.Drawing.Point(12, 530);
            this.lblnetweight.Name = "lblnetweight";
            this.lblnetweight.Size = new System.Drawing.Size(99, 24);
            this.lblnetweight.TabIndex = 53;
            this.lblnetweight.Text = "Net weight";
            this.lblnetweight.UseWaitCursor = true;
            // 
            // txtnetweight
            // 
            this.txtnetweight.Location = new System.Drawing.Point(149, 532);
            this.txtnetweight.Name = "txtnetweight";
            this.txtnetweight.Size = new System.Drawing.Size(170, 29);
            this.txtnetweight.TabIndex = 16;
            // 
            // txttaretime
            // 
            this.txttaretime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttaretime.Location = new System.Drawing.Point(396, 486);
            this.txttaretime.Name = "txttaretime";
            this.txttaretime.Size = new System.Drawing.Size(281, 26);
            this.txttaretime.TabIndex = 14;
            // 
            // lbltaretime
            // 
            this.lbltaretime.AutoSize = true;
            this.lbltaretime.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltaretime.Location = new System.Drawing.Point(342, 486);
            this.lbltaretime.Name = "lbltaretime";
            this.lbltaretime.Size = new System.Drawing.Size(53, 24);
            this.lbltaretime.TabIndex = 48;
            this.lbltaretime.Text = "Time";
            this.lbltaretime.UseWaitCursor = true;
            // 
            // lbltareweight
            // 
            this.lbltareweight.AutoSize = true;
            this.lbltareweight.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltareweight.Location = new System.Drawing.Point(14, 480);
            this.lbltareweight.Name = "lbltareweight";
            this.lbltareweight.Size = new System.Drawing.Size(109, 24);
            this.lbltareweight.TabIndex = 47;
            this.lbltareweight.Text = "Tare weight";
            this.lbltareweight.UseWaitCursor = true;
            // 
            // txttareweight
            // 
            this.txttareweight.Location = new System.Drawing.Point(149, 483);
            this.txttareweight.Name = "txttareweight";
            this.txttareweight.Size = new System.Drawing.Size(170, 29);
            this.txttareweight.TabIndex = 13;
            // 
            // txtgrosstime
            // 
            this.txtgrosstime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgrosstime.Location = new System.Drawing.Point(396, 437);
            this.txtgrosstime.Name = "txtgrosstime";
            this.txtgrosstime.Size = new System.Drawing.Size(281, 26);
            this.txtgrosstime.TabIndex = 11;
            // 
            // lblgrosstime
            // 
            this.lblgrosstime.AutoSize = true;
            this.lblgrosstime.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblgrosstime.Location = new System.Drawing.Point(338, 431);
            this.lblgrosstime.Name = "lblgrosstime";
            this.lblgrosstime.Size = new System.Drawing.Size(53, 24);
            this.lblgrosstime.TabIndex = 42;
            this.lblgrosstime.Text = "Time";
            this.lblgrosstime.UseWaitCursor = true;
            // 
            // lblgrossweigh
            // 
            this.lblgrossweigh.AutoSize = true;
            this.lblgrossweigh.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblgrossweigh.Location = new System.Drawing.Point(15, 430);
            this.lblgrossweigh.Name = "lblgrossweigh";
            this.lblgrossweigh.Size = new System.Drawing.Size(119, 24);
            this.lblgrossweigh.TabIndex = 41;
            this.lblgrossweigh.Text = "Gross weight";
            this.lblgrossweigh.UseWaitCursor = true;
            // 
            // grosstxt
            // 
            this.grosstxt.Location = new System.Drawing.Point(149, 434);
            this.grosstxt.Name = "grosstxt";
            this.grosstxt.Size = new System.Drawing.Size(174, 29);
            this.grosstxt.TabIndex = 10;
            // 
            // cbwardname
            // 
            this.cbwardname.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbwardname.FormattingEnabled = true;
            this.cbwardname.Location = new System.Drawing.Point(148, 382);
            this.cbwardname.Name = "cbwardname";
            this.cbwardname.Size = new System.Drawing.Size(530, 32);
            this.cbwardname.TabIndex = 9;
            // 
            // cbzonename
            // 
            this.cbzonename.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbzonename.FormattingEnabled = true;
            this.cbzonename.Location = new System.Drawing.Point(149, 330);
            this.cbzonename.Name = "cbzonename";
            this.cbzonename.Size = new System.Drawing.Size(530, 32);
            this.cbzonename.TabIndex = 8;
            this.cbzonename.SelectedIndexChanged += new System.EventHandler(this.cbzonename_SelectedIndexChanged);
            // 
            // cbwastetype
            // 
            this.cbwastetype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbwastetype.FormattingEnabled = true;
            this.cbwastetype.Location = new System.Drawing.Point(148, 278);
            this.cbwastetype.Name = "cbwastetype";
            this.cbwastetype.Size = new System.Drawing.Size(530, 32);
            this.cbwastetype.TabIndex = 7;
            // 
            // txtslipno
            // 
            this.txtslipno.Location = new System.Drawing.Point(147, 229);
            this.txtslipno.Name = "txtslipno";
            this.txtslipno.Size = new System.Drawing.Size(231, 29);
            this.txtslipno.TabIndex = 6;
            // 
            // cbvendername
            // 
            this.cbvendername.Cursor = System.Windows.Forms.Cursors.VSplit;
            this.cbvendername.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbvendername.FormattingEnabled = true;
            this.cbvendername.Location = new System.Drawing.Point(147, 125);
            this.cbvendername.Name = "cbvendername";
            this.cbvendername.Size = new System.Drawing.Size(530, 32);
            this.cbvendername.TabIndex = 3;
            // 
            // lblwardname
            // 
            this.lblwardname.AutoSize = true;
            this.lblwardname.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblwardname.Location = new System.Drawing.Point(14, 380);
            this.lblwardname.Name = "lblwardname";
            this.lblwardname.Size = new System.Drawing.Size(111, 24);
            this.lblwardname.TabIndex = 31;
            this.lblwardname.Text = "Ward Name";
            this.lblwardname.UseWaitCursor = true;
            // 
            // lblzonename
            // 
            this.lblzonename.AutoSize = true;
            this.lblzonename.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblzonename.Location = new System.Drawing.Point(15, 330);
            this.lblzonename.Name = "lblzonename";
            this.lblzonename.Size = new System.Drawing.Size(111, 24);
            this.lblzonename.TabIndex = 30;
            this.lblzonename.Text = "Zone Name";
            this.lblzonename.UseWaitCursor = true;
            // 
            // lblwastetype
            // 
            this.lblwastetype.AutoSize = true;
            this.lblwastetype.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblwastetype.Location = new System.Drawing.Point(12, 280);
            this.lblwastetype.Name = "lblwastetype";
            this.lblwastetype.Size = new System.Drawing.Size(102, 24);
            this.lblwastetype.TabIndex = 29;
            this.lblwastetype.Text = "Waste type";
            this.lblwastetype.UseWaitCursor = true;
            // 
            // lblslipno
            // 
            this.lblslipno.AutoSize = true;
            this.lblslipno.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblslipno.Location = new System.Drawing.Point(14, 230);
            this.lblslipno.Name = "lblslipno";
            this.lblslipno.Size = new System.Drawing.Size(73, 24);
            this.lblslipno.TabIndex = 28;
            this.lblslipno.Text = "Slip no.";
            this.lblslipno.UseWaitCursor = true;
            // 
            // lbldrivername
            // 
            this.lbldrivername.AutoSize = true;
            this.lbldrivername.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldrivername.Location = new System.Drawing.Point(12, 180);
            this.lbldrivername.Name = "lbldrivername";
            this.lbldrivername.Size = new System.Drawing.Size(112, 24);
            this.lbldrivername.TabIndex = 26;
            this.lbldrivername.Text = "Driver name";
            this.lbldrivername.UseWaitCursor = true;
            // 
            // lblvendorname
            // 
            this.lblvendorname.AutoSize = true;
            this.lblvendorname.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblvendorname.Location = new System.Drawing.Point(12, 130);
            this.lblvendorname.Name = "lblvendorname";
            this.lblvendorname.Size = new System.Drawing.Size(126, 24);
            this.lblvendorname.TabIndex = 25;
            this.lblvendorname.Text = "Vendor name";
            this.lblvendorname.UseWaitCursor = true;
            // 
            // cbvehicle
            // 
            this.cbvehicle.AutoSize = true;
            this.cbvehicle.Location = new System.Drawing.Point(396, 70);
            this.cbvehicle.Name = "cbvehicle";
            this.cbvehicle.Size = new System.Drawing.Size(233, 28);
            this.cbvehicle.TabIndex = 24;
            this.cbvehicle.Text = "Is Commercial vehicle??";
            this.cbvehicle.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(148, 76);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(230, 29);
            this.textBox1.TabIndex = 2;
            // 
            // lbltripid
            // 
            this.lbltripid.AutoSize = true;
            this.lbltripid.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltripid.Location = new System.Drawing.Point(14, 80);
            this.lbltripid.Name = "lbltripid";
            this.lbltripid.Size = new System.Drawing.Size(65, 24);
            this.lbltripid.TabIndex = 22;
            this.lbltripid.Text = "Trip ID";
            this.lbltripid.UseWaitCursor = true;
            // 
            // lbltype
            // 
            this.lbltype.AutoSize = true;
            this.lbltype.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltype.Location = new System.Drawing.Point(331, 30);
            this.lbltype.Name = "lbltype";
            this.lbltype.Size = new System.Drawing.Size(53, 24);
            this.lbltype.TabIndex = 20;
            this.lbltype.Text = "Type";
            this.lbltype.UseWaitCursor = true;
            // 
            // txtvehicleno
            // 
            this.txtvehicleno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtvehicleno.Location = new System.Drawing.Point(148, 27);
            this.txtvehicleno.MaxLength = 10;
            this.txtvehicleno.Name = "txtvehicleno";
            this.txtvehicleno.Size = new System.Drawing.Size(175, 29);
            this.txtvehicleno.TabIndex = 0;
            this.txtvehicleno.TextChanged += new System.EventHandler(this.txtvehicleno_TextChanged_1);
            // 
            // lblvehicleno
            // 
            this.lblvehicleno.AutoSize = true;
            this.lblvehicleno.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblvehicleno.Location = new System.Drawing.Point(12, 30);
            this.lblvehicleno.Name = "lblvehicleno";
            this.lblvehicleno.Size = new System.Drawing.Size(106, 24);
            this.lblvehicleno.TabIndex = 12;
            this.lblvehicleno.Text = "Vehicle no.";
            this.lblvehicleno.UseWaitCursor = true;
            // 
            // btncapture
            // 
            this.btncapture.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncapture.Location = new System.Drawing.Point(326, 59);
            this.btncapture.Name = "btncapture";
            this.btncapture.Size = new System.Drawing.Size(215, 35);
            this.btncapture.TabIndex = 22;
            this.btncapture.Text = "Capture weight and image\r\n";
            this.btncapture.UseVisualStyleBackColor = true;
            this.btncapture.Click += new System.EventHandler(this.btncapture_Click_1);
            // 
            // txtweighweight
            // 
            this.txtweighweight.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtweighweight.ForeColor = System.Drawing.SystemColors.Highlight;
            this.txtweighweight.Location = new System.Drawing.Point(42, 56);
            this.txtweighweight.Name = "txtweighweight";
            this.txtweighweight.Size = new System.Drawing.Size(256, 40);
            this.txtweighweight.TabIndex = 0;
            this.txtweighweight.TextChanged += new System.EventHandler(this.txtweighweight_TextChanged_1);
            // 
            // lblweighweight
            // 
            this.lblweighweight.AutoSize = true;
            this.lblweighweight.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblweighweight.Location = new System.Drawing.Point(29, 14);
            this.lblweighweight.Name = "lblweighweight";
            this.lblweighweight.Size = new System.Drawing.Size(269, 31);
            this.lblweighweight.TabIndex = 21;
            this.lblweighweight.Text = "Weighbridge weight";
            this.lblweighweight.UseWaitCursor = true;
            // 
            // lblnw
            // 
            this.lblnw.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblnw.BackColor = System.Drawing.Color.Green;
            this.lblnw.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblnw.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblnw.Location = new System.Drawing.Point(1276, 22);
            this.lblnw.Name = "lblnw";
            this.lblnw.Size = new System.Drawing.Size(242, 36);
            this.lblnw.TabIndex = 5;
            this.lblnw.Text = "NETWORK";
            this.lblnw.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblcamera
            // 
            this.lblcamera.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblcamera.BackColor = System.Drawing.Color.Green;
            this.lblcamera.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcamera.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblcamera.Location = new System.Drawing.Point(1011, 22);
            this.lblcamera.Name = "lblcamera";
            this.lblcamera.Size = new System.Drawing.Size(242, 36);
            this.lblcamera.TabIndex = 4;
            this.lblcamera.Text = "CAMERA";
            this.lblcamera.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblfp
            // 
            this.lblfp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblfp.BackColor = System.Drawing.Color.Red;
            this.lblfp.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfp.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblfp.Location = new System.Drawing.Point(746, 22);
            this.lblfp.Name = "lblfp";
            this.lblfp.Size = new System.Drawing.Size(242, 36);
            this.lblfp.TabIndex = 3;
            this.lblfp.Text = "FINGER PRINT";
            this.lblfp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // grpbx2livevideo
            // 
            this.grpbx2livevideo.Controls.Add(this.pictureBox3);
            this.grpbx2livevideo.Location = new System.Drawing.Point(735, 85);
            this.grpbx2livevideo.Name = "grpbx2livevideo";
            this.grpbx2livevideo.Size = new System.Drawing.Size(783, 243);
            this.grpbx2livevideo.TabIndex = 1;
            this.grpbx2livevideo.TabStop = false;
            this.grpbx2livevideo.Text = "Live Video feed";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(24, 28);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(737, 209);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            // 
            // grpbx3summary
            // 
            this.grpbx3summary.Controls.Add(this.dataGridView1);
            this.grpbx3summary.Location = new System.Drawing.Point(741, 346);
            this.grpbx3summary.Name = "grpbx3summary";
            this.grpbx3summary.Size = new System.Drawing.Size(777, 355);
            this.grpbx3summary.TabIndex = 2;
            this.grpbx3summary.TabStop = false;
            this.grpbx3summary.Text = "Summary for the day";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.vendorDataGridViewTextBoxColumn,
            this.vehiclesDataGridViewTextBoxColumn,
            this.tripDataGridViewTextBoxColumn,
            this.weightDataGridViewTextBoxColumn});
            this.dataGridView1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.dataGridView1.DataSource = this.daysummaryBindingSource1;
            this.dataGridView1.Enabled = false;
            this.dataGridView1.Location = new System.Drawing.Point(18, 25);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(737, 312);
            this.dataGridView1.TabIndex = 0;
            // 
            // vendorDataGridViewTextBoxColumn
            // 
            this.vendorDataGridViewTextBoxColumn.DataPropertyName = "vendor";
            this.vendorDataGridViewTextBoxColumn.HeaderText = "Vendor Name";
            this.vendorDataGridViewTextBoxColumn.Name = "vendorDataGridViewTextBoxColumn";
            // 
            // vehiclesDataGridViewTextBoxColumn
            // 
            this.vehiclesDataGridViewTextBoxColumn.DataPropertyName = "vehicles";
            this.vehiclesDataGridViewTextBoxColumn.HeaderText = "Vehicles";
            this.vehiclesDataGridViewTextBoxColumn.Name = "vehiclesDataGridViewTextBoxColumn";
            // 
            // tripDataGridViewTextBoxColumn
            // 
            this.tripDataGridViewTextBoxColumn.DataPropertyName = "trip";
            this.tripDataGridViewTextBoxColumn.HeaderText = "Trip";
            this.tripDataGridViewTextBoxColumn.Name = "tripDataGridViewTextBoxColumn";
            // 
            // weightDataGridViewTextBoxColumn
            // 
            this.weightDataGridViewTextBoxColumn.DataPropertyName = "weight";
            this.weightDataGridViewTextBoxColumn.HeaderText = "Weight";
            this.weightDataGridViewTextBoxColumn.Name = "weightDataGridViewTextBoxColumn";
            // 
            // tb1vehicleweigh
            // 
            this.tb1vehicleweigh.Controls.Add(this.tb1Vehicleweighment);
            this.tb1vehicleweigh.Controls.Add(this.tb2PendingTrips);
            this.tb1vehicleweigh.Controls.Add(this.tb4Weighmenthistory);
            this.tb1vehicleweigh.Controls.Add(this.tb7ReportsMore);
            this.tb1vehicleweigh.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb1vehicleweigh.ItemSize = new System.Drawing.Size(201, 30);
            this.tb1vehicleweigh.Location = new System.Drawing.Point(12, 65);
            this.tb1vehicleweigh.Name = "tb1vehicleweigh";
            this.tb1vehicleweigh.SelectedIndex = 0;
            this.tb1vehicleweigh.Size = new System.Drawing.Size(1560, 758);
            this.tb1vehicleweigh.TabIndex = 12;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "id";
            this.Column1.FillWeight = 29.62963F;
            this.Column1.HeaderText = "Slip No";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "num";
            this.dataGridViewTextBoxColumn3.FillWeight = 110.0529F;
            this.dataGridViewTextBoxColumn3.HeaderText = "Vehicle No";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "type";
            this.dataGridViewTextBoxColumn2.FillWeight = 110.0529F;
            this.dataGridViewTextBoxColumn2.HeaderText = "Weste Type";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "id";
            this.dataGridViewTextBoxColumn1.HeaderText = "Trip Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 88;
            // 
            // zone
            // 
            this.zone.DataPropertyName = "zone1";
            this.zone.FillWeight = 110.0529F;
            this.zone.HeaderText = "Zone";
            this.zone.Name = "zone";
            this.zone.ReadOnly = true;
            // 
            // ward
            // 
            this.ward.DataPropertyName = "ward1";
            this.ward.FillWeight = 110.0529F;
            this.ward.HeaderText = "Ward";
            this.ward.Name = "ward";
            this.ward.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "weight1";
            this.dataGridViewTextBoxColumn5.FillWeight = 110.0529F;
            this.dataGridViewTextBoxColumn5.HeaderText = "Gross Weight";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "indatetime";
            this.dataGridViewTextBoxColumn4.FillWeight = 110.0529F;
            this.dataGridViewTextBoxColumn4.HeaderText = "In DateTime";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // weight2
            // 
            this.weight2.DataPropertyName = "weight2";
            this.weight2.FillWeight = 110.0529F;
            this.weight2.HeaderText = "Tare Weight";
            this.weight2.Name = "weight2";
            this.weight2.ReadOnly = true;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1584, 861);
            this.Controls.Add(this.tb1vehicleweigh);
            this.Controls.Add(this.btnconfig);
            this.Controls.Add(this.lblusername);
            this.Controls.Add(this.btnlogout);
            this.Controls.Add(this.btnchangepassword);
            this.Controls.Add(this.btnverifyfinger);
            this.Controls.Add(this.lblcurrentuser);
            this.Name = "Form2";
            this.Text = "Weighbridge Management System";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form2_FormClosing);
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.spgetpendingtripdetailBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spgetpendingtripdetailBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spvehicletripdetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.daysummaryBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spgetpendingtripdetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblzoneBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spgettripdetailBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.daysummaryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmsDataSet28)).EndInit();
            this.tb7ReportsMore.ResumeLayout(false);
            this.tb7ReportsMore.PerformLayout();
            this.tb4Weighmenthistory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.gbweighdet.ResumeLayout(false);
            this.gbweighdet.PerformLayout();
            this.gbtareweight.ResumeLayout(false);
            this.gbtareweight.PerformLayout();
            this.gbgrossweight.ResumeLayout(false);
            this.gbgrossweight.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.bgserchresult.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.gbslipsearch.ResumeLayout(false);
            this.gbslipsearch.PerformLayout();
            this.gbdaterange.ResumeLayout(false);
            this.gbdaterange.PerformLayout();
            this.tb2PendingTrips.ResumeLayout(false);
            this.gbtarependlist.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tb1Vehicleweighment.ResumeLayout(false);
            this.tb1Vehicleweighment.PerformLayout();
            this.gbrecorddetail.ResumeLayout(false);
            this.gbrecorddetail.PerformLayout();
            this.grpbx2livevideo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.grpbx3summary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tb1vehicleweigh.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblusername;
        private System.Windows.Forms.Button btnlogout;
        private System.Windows.Forms.Button btnchangepassword;
        private System.Windows.Forms.Button btnverifyfinger;
        private System.Windows.Forms.Label lblcurrentuser;
        private wmsDataSet wmsDataSet;
        private System.Windows.Forms.BindingSource spgettripdetailBindingSource;
        private wmsDataSetTableAdapters.sp_gettripdetailTableAdapter sp_gettripdetailTableAdapter;
        private wmsDataSet1 wmsDataSet1;
        private System.Windows.Forms.Timer timer1;
        private wmsDataSet2 wmsDataSet2;
        private wmsDataSet3 wmsDataSet3;
        private System.Windows.Forms.BindingSource spgetpendingtripdetailBindingSource1;
        private wmsDataSet3TableAdapters.sp_getpendingtripdetailTableAdapter sp_getpendingtripdetailTableAdapter1;
        private System.Windows.Forms.Button btnconfig;
        private wmsDataSet5 wmsDataSet5;
        private System.Windows.Forms.BindingSource tblzoneBindingSource;
        private wmsDataSet5TableAdapters.tbl_zoneTableAdapter tbl_zoneTableAdapter;
        private wmsDataSet6 wmsDataSet6;
        private wmsDataSet2TableAdapters.sp_getpendingtripdetailTableAdapter sp_getpendingtripdetailTableAdapter;
        private wmsDataSet7 wmsDataSet7;
        private System.Windows.Forms.BindingSource spgetpendingtripdetailBindingSource;
        private wmsDataSet7TableAdapters.sp_getpendingtripdetailTableAdapter sp_getpendingtripdetailTableAdapter2;
        private wmsDataSet8 wmsDataSet8;
        private System.Windows.Forms.BindingSource spgetpendingtripdetailBindingSource2;
        private wmsDataSet8TableAdapters.sp_getpendingtripdetailTableAdapter sp_getpendingtripdetailTableAdapter3;
        private wmsDataSet9 wmsDataSet9;
        private System.Windows.Forms.BindingSource daysummaryBindingSource;
        private wmsDataSet9TableAdapters.daysummaryTableAdapter daysummaryTableAdapter;
        private wmsDataSet10 wmsDataSet10;
        private System.Windows.Forms.BindingSource daysummaryBindingSource1;
        private wmsDataSet10TableAdapters.daysummaryTableAdapter daysummaryTableAdapter1;
        private wmsDataSet19 wmsDataSet19;
        private wmsDataSet20 wmsDataSet20;
        private wmsDataSet21 wmsDataSet21;
        private wmsDataSet22 wmsDataSet22;
        private wmsDataSet23 wmsDataSet23;
        private wmsDataSet24 wmsDataSet24;
        private wmsDataSet25 wmsDataSet25;
        private wmsDataSet26 wmsDataSet26;
        private wmsDataSet27 wmsDataSet27;
        private wmsDataSet28 wmsDataSet28;
        private System.Windows.Forms.BindingSource spvehicletripdetailBindingSource;
        private wmsDataSet29 wmsDataSet29;
        private wmsDataSet29TableAdapters.sp_vehicletripdetailTableAdapter sp_vehicletripdetailTableAdapter;
        private System.Windows.Forms.TabPage tb7ReportsMore;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label5;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.Button btnviewreprt;
        private System.Windows.Forms.ComboBox cbrepttyp;
        private System.Windows.Forms.Label lblreprttyp;
        private System.Windows.Forms.TabPage tb4Weighmenthistory;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.GroupBox gbweighdet;
        private System.Windows.Forms.ComboBox cbvehcltyp;
        private System.Windows.Forms.Button btnupdttrp;
        private System.Windows.Forms.Button btnignrtrp;
        private System.Windows.Forms.Button btnprntslp;
        private System.Windows.Forms.TextBox txtnetweigh;
        private System.Windows.Forms.ComboBox cbwrdname;
        private System.Windows.Forms.ComboBox cbznname;
        private System.Windows.Forms.ComboBox cbwsttyp;
        private System.Windows.Forms.Label lblvehcltyp;
        private System.Windows.Forms.ComboBox cbdrivername;
        private System.Windows.Forms.ComboBox cbvndrname;
        private System.Windows.Forms.TextBox txttripid;
        private System.Windows.Forms.TextBox txtslipid;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblwsttyp;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lbltrpid;
        private System.Windows.Forms.Label lblslpid;
        private System.Windows.Forms.GroupBox gbtareweight;
        private System.Windows.Forms.TextBox txttareuser;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txttareweightime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txttareweigh;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox gbgrossweight;
        private System.Windows.Forms.TextBox txtgrossuser;
        private System.Windows.Forms.TextBox txtgrossweitim;
        private System.Windows.Forms.TextBox txtgrossweigh;
        private System.Windows.Forms.Label lbluser;
        private System.Windows.Forms.Label lbltim;
        private System.Windows.Forms.Label lblweigh;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox bgserchresult;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.GroupBox gbslipsearch;
        private System.Windows.Forms.Button btnsearch;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.GroupBox gbdaterange;
        private System.Windows.Forms.Label lblto;
        private System.Windows.Forms.Label lblfrm;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label lblvehclnu;
        private System.Windows.Forms.Button btngetrec;
        private System.Windows.Forms.TextBox txtvehclnu;
        private System.Windows.Forms.TabPage tb2PendingTrips;
        private System.Windows.Forms.GroupBox gbtarependlist;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button btnignoretrip;
        private System.Windows.Forms.Button btncompltrip;
        private System.Windows.Forms.TabPage tb1Vehicleweighment;
        private System.Windows.Forms.GroupBox gbrecorddetail;
        private System.Windows.Forms.ComboBox cbdrvrname;
        private System.Windows.Forms.TextBox txttyp;
        private System.Windows.Forms.Button btnnewrecord;
        private System.Windows.Forms.Button btnsaveprint;
        private System.Windows.Forms.Label lblnetweight;
        private System.Windows.Forms.TextBox txtnetweight;
        private System.Windows.Forms.TextBox txttaretime;
        private System.Windows.Forms.Label lbltaretime;
        private System.Windows.Forms.Label lbltareweight;
        private System.Windows.Forms.TextBox txttareweight;
        private System.Windows.Forms.TextBox txtgrosstime;
        private System.Windows.Forms.Label lblgrosstime;
        private System.Windows.Forms.Label lblgrossweigh;
        private System.Windows.Forms.TextBox grosstxt;
        private System.Windows.Forms.ComboBox cbwardname;
        private System.Windows.Forms.ComboBox cbzonename;
        private System.Windows.Forms.ComboBox cbwastetype;
        private System.Windows.Forms.TextBox txtslipno;
        private System.Windows.Forms.ComboBox cbvendername;
        private System.Windows.Forms.Label lblwardname;
        private System.Windows.Forms.Label lblzonename;
        private System.Windows.Forms.Label lblwastetype;
        private System.Windows.Forms.Label lblslipno;
        private System.Windows.Forms.Label lbldrivername;
        private System.Windows.Forms.Label lblvendorname;
        private System.Windows.Forms.CheckBox cbvehicle;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lbltripid;
        private System.Windows.Forms.Label lbltype;
        private System.Windows.Forms.TextBox txtvehicleno;
        private System.Windows.Forms.Label lblvehicleno;
        private System.Windows.Forms.Button btncapture;
        private System.Windows.Forms.TextBox txtweighweight;
        private System.Windows.Forms.Label lblweighweight;
        private System.Windows.Forms.Label lblnw;
        private System.Windows.Forms.Label lblcamera;
        private System.Windows.Forms.Label lblfp;
        private System.Windows.Forms.GroupBox grpbx2livevideo;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.GroupBox grpbx3summary;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn vendorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vehiclesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tripDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn weightDataGridViewTextBoxColumn;
        private System.Windows.Forms.TabControl tb1vehicleweigh;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vehnumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tripidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vehtypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn zonenameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn wardnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn indatetimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn weight1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn zone;
        private System.Windows.Forms.DataGridViewTextBoxColumn ward;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn weight2;
    }
}