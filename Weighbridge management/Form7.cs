﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Weighbridge_management
{
    public partial class Form7 : Form
    {
        dbconnection con = new dbconnection();
        public Form7()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            con.Open();
            string query = "SELECT * FROM wms.tbl_trip  where id = " + Form2.id + "";
            MySqlDataReader row;
            row = con.ExecuteReader(query);
            if (row.HasRows)
            {
                con.Close();
                con.Open();
                string query1 = "update `wms`.`tbl_trip` set outdatetime=now(),isignore=1  where id = '" + Form2.id + "'";
                MySqlDataReader row1;
                row1 = con.ExecuteReader(query1);
                if (row1.RecordsAffected == 1)
                {
                    con.Close();
                    this.Close();

                    MessageBox.Show("Trip ignore...");
                }
               
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
            MessageBox.Show("Trip ignore cancle...");

        }
    }
}
