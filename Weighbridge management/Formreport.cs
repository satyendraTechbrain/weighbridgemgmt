﻿using Microsoft.Reporting.WinForms;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Weighbridge_management
{
    public partial class Formreport : Form
    {
        MySql.Data.MySqlClient.MySqlConnection conn;
        static string host = "localhost";
        static string database = "wms";
        static string userDB = "root";
        static string password = "Techbrain@123";
        public static string strProvider = "server=" + host + ";Database=" + database + ";User ID=" + userDB + ";Password=" + password;
        public Formreport()
        {
            InitializeComponent();
        }

        private void Formreport_Load(object sender, EventArgs e)
        {
            this.reportViewer1.LocalReport.ReportPath = "Report1.rdlc";
            ReportDataSource rds = new ReportDataSource("DataSet1", getdetail());
            this.reportViewer1.LocalReport.DataSources.Add(rds);
            this.reportViewer1.RefreshReport();
        }

      
        private List<report> getdetail()
        {
            string type = Form2.type;
            string time1 = Form2.time1;
          

             strProvider = "server=" + host + ";Database=" + database + ";User ID=" + userDB + ";Password=" + password;
                conn = new MySqlConnection(strProvider);
                conn.Open();
                string q1 = "sp_report";
                MySqlCommand sd = new MySqlCommand(q1, conn);
                sd.CommandType = CommandType.StoredProcedure;
                sd.Parameters.Add("@typ", MySqlDbType.VarChar, 225).Value = type;
                sd.Parameters.Add("@fromdate", MySqlDbType.VarChar, 225).Value = time1;
                MySqlDataReader read = sd.ExecuteReader();
                List<report> TestList = new List<report>();
                report test = null;
                while (read.Read())
                {
                    test = new report();
                    test.slipno = int.Parse(read["id"].ToString());
                    test.trip = int.Parse(read["tripid"].ToString());
                    test.vehicle= read["number"].ToString();
                    test.type= read["type"].ToString();
                    test.ward= read["name"].ToString();
                    test.indatetime= DateTime.Parse(read["grossdate"].ToString());
                    test.intime= DateTime.Parse( read["intime"].ToString());                    
                    test.outtime= DateTime.Parse( read["outtime"].ToString());
                    test.outtime.ToString("hh:mm tt");
                    test.weight1= read["weight1"].ToString();
                    test.weight3= read["weight3"].ToString();
                    test.weight4= read["weight4"].ToString();
                    TestList.Add(test);
                }


            return TestList;
        }

      
    }
}
